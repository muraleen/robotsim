\documentclass[twocoloumn,letterpaper]{IEEEAerospaceCLS}  % only supports 
\usepackage[pdftex]{graphicx}
\graphicspath{{pdf/}}
\DeclareGraphicsExtensions{.pdf}
\usepackage[caption=false, font=footnotesize]{subfig}

\usepackage{cite}
\usepackage[cmex10]{amsmath}
\DeclareMathOperator{\condif}{if}
\interdisplaylinepenalty=2500
\usepackage{amsfonts}
\usepackage{bm}
\usepackage{algorithm}
\usepackage{mathtools}
\usepackage{multirow}
\usepackage{hhline}
\usepackage[hyphens]{url}

%\hyphenation{}

\begin{document}

\title{Omnidirectional Locomotion Control of a \\Pendulum Driven Spherical Robot}

\author{Narendran Muraleedharan\\
Embry-Riddle Aeronautical University\\
Prescott, AZ 86301\\
muraleen@my.erau.edu,
\and
Daniel Cohen\\
Embry-Riddle Aeronautical University\\
Prescott, AZ 86301\\
cohend1@my.erau.edu
\thanks{\footnotesize 978-1-4673-7676-1/16/$\$31.00$ \copyright2016 IEEE}
}

\maketitle

\thispagestyle{plain}
\pagestyle{plain}

\begin{abstract}
This paper proposes a method for omnidirectional motion control of a pendulum driven spherical robot. Two continuous rotation actuators control the orientation of a pendulum within the spherical shell. The control system generates desired actuators angles to move the pendulum in the direction of desired motion in reference to an assumed inertial frame. Additionally, partial feedback linearizion of the under-actuated nonlinear system is used to aid in driving the actuator angles to the desired angles. The dynamics of the robot were derived using the Euler-Lagrange formulation and simulated to verify the functionality of the controller. The simulations show that the proposed control method is capable of performing directional and set-point control of the robot.
\end{abstract}

\tableofcontents

\section{Introduction}

Spherical mobile robots have an external spherical shell which use various internal mechanisms as opposed to wheels for locomotion. This allows the robots to be completely sealed from the surroundings and hence endure harsh environments \cite{AlvesJ2003}. Spherical mobile robots also have the ability to perform omnidirectional locomotion - which in this paper is assumed to be the ability to move in any direction from any position and orientation. Spherical robots can perform omnidirectional locomotion by rolling in the desired direction. \cite{XuW2011}

A significant amount of research has been conducted over the past decade on design of spherical robot actuation mechanisms and control systems. The most commonly used internal actuation mechanisms for spherical robots are the barycenter offset system and an internally actuated pendulum. The barycenter offset system, also known as the hamster ball design uses a wheeled robot inside a spherical shell. By moving the wheeled robot, the center of gravity of the system shifts and induces a torque on the outer shell. This method however inhibits the robot from being able to accelerate in any desired direction while moving. Previous research on such robots show the viability of the mechanism for linear motion and driving in paths with small curvature. However, such systems are shown to exhibit trouble driving in paths with large curvatures \cite{AlvesJ2003, XueleiN2014}. A pendulum driven robot uses an internal ballast which can be moved around inside the shell. This also induces a torque on the outer shell and allows for locomotion. Other spherical robot actuation mechanisms include an internal drive unit, universal wheel, double pendulum, control moment gyroscopes (CMG), shell transformation systems etc \cite{ChaseR2012}.

This paper considers the use of a pendulum driven spherical robot with two orthogonally placed actuators to move the ballast inside the shell. Pendulum driven spherical robots have been studied extensively and various control systems have been developed. A common approach to controlling pendulum driven robots is by controlling the drive speed of the robot with one of the actuators and the steering with the other \cite{CaiY2011, HalmeA1996, LiuD2008, NagaiM2008, OthmanK2012}. A drive and steer based control system inhibits the robot's ability to perform omnidirectional locomotion. Usually, such robots can be directly teleoperated and do not need any automatic control \cite{HalmeA1996}. However, controllers can be developed to stabilize the drive speed and lean angle for steering \cite{LiuD2008}.

Some interesting alternative controllers used for pendulum driven spherical robots include the use of various neural network structures \cite{CaiY2011} and exponential reaching law (ERL) controllers \cite{YuT2014}. The study on the neural network controller by Cai et. al. provides promising results for linear motion, however does not address two dimensional (2D) control of the robot. Yu and Sun's study on the sliding mode approach for an ERL controller attains omnidirectional locomotion control of a pendulum driven spherical robot (named BYQ-VIII) on an inclined plane. The method has been tested in simulation and experimentally and proven to be functional \cite{YuT2014}. The method proposed in this paper approaches the problem from a simpler perspective using some reasonable assumptions for low speed omnidirectional locomotion.

The proposed controller is comprised of two stages. The first stage does not require any knowledge of the system dynamics and can be tuned independently. The second stage uses partial feedback linearization to control the actuator angles but can be replaced with a controller that does not require knowledge of the dynamic parameters of the robot. The use of well controlled servo actuators can also eliminate the need for the dynamic parameters.

\section{Dynamic Modeling}

\clearpage

\section{Control System Design}

The control system is divided into two stages - the first to transform desired commands from task space to joint space and the second to control the actuator angles to the desired angles calculated by the first stage with the use of partial feedback linearization.

\subsection{Task-space to Joint-space transformation}

The proposed control system is based on the assumption that the the robot accelerates in the direction the pendulum is lifted. Some fundamental assumptions that there is no slip between the sphere and the ground, the robot can be simplified into a two-body system comprised of the sphere and the pendulum, and that the robot will be moving slow enough for the vector of Coriolis and centripetal forces $\bm{d}$ to be negligible allow such an assumption to be made.

The magnitude of acceleration can be controller by the angle between the pendulum and the vertical, while the direction of acceleration can be controller by the rotation of the pendulum about the inertial z axis. 

\textbf{FIGURE: cuts of the sphere showing beta1 and beta2}

By applying force and moment equilibrium equations about the center of the sphere for a one dimensional (1D) case as shown in equations (\ref{Mx}) and (\ref{Fy}) and the kinematic constraint, the required angle between the pendulum and the vertical $\beta_1$ can be computed.

\begin{equation} \label{Mx}
	\Sigma M_x = F_f r - m_p g d \textrm{sin}(\beta_1) = J \ddot{\varphi}
\end{equation}

\begin{equation} \label{Fy}
	\Sigma F_y = F_f = (m_p + m_s) \ddot{y}
\end{equation}

Where $F_f$ is the force of friction at the point of contact between the spherical shell and the ground, $r$ is the radius of the sphere, $m_p$ is the mass of the pendulum ballast, $m_s$ is the mass of the sphere, $d$ is the distance between the center of the sphere and the center of mass of the pendulum, $\varphi$ is the angle of rotation in the 1D system and $y$ is the linear translation in the 1D system.

The kinematic Pfaffian constraint discussed earlier can be written in the 1D system as:

\begin{equation} \label{constraint}
	\dot{y} = r \dot{\varphi} \quad \implies \quad \ddot{y} = r \ddot{\varphi}
\end{equation}

By substituting $F_f$ from equation (\ref{Fy}) and $\ddot{\varphi}$ from equation (\ref{constraint}) into equation (\ref{Mx}),

\begin{equation} \label{beta1}
	\beta_1 = \textrm{sin}^{-1} \bigg( \bigg( \bigg( 1 + \frac{m_s}{m_p} \bigg) r - \frac{J}{r m_p} \bigg) \frac{\ddot{y}}{g d} \bigg)
\end{equation}

Where $J$ is the rotational inertia of the system about the rolling direction. To simplify calculations, the desired $\beta_1$ angle is instead computed using a PID controller with the error defined by $|\bm{v}_{des} - \bm{v}|$ where $|\bm{v}_{des}|$ is the magnitude of desired velocity and $|\bm{v}|$ is the magnitude of the actual velocity vector of the robot.

\textbf{FIX this - use ades instead of vdes - v}

\begin{align} \label{pid}
	\beta_1 &= K_{p,1} (\bm{v}_{des} - \bm{v}) + K_{d,1} \frac{d}{dt} (\bm{v}_{des} - \bm{v}) \nonumber \\
	&+ K_{i,1} \int (\bm{v}_{des} - \bm{v}) dt
\end{align}

Where $K_{p,1}$, $K_{d,1}$ and $K_{i,1}$ are the proportional, derivative and integral gains of the PID controller.

The direction of the desired acceleration vector can be computed using equation \ref{beta2}.

\begin{equation} \label{beta2}
	\beta_2 = \textrm{atan2}(v_{des}, u_{des})
\end{equation}

Where $u_{des}$ and $v_{des}$ are the x and y components of the desired acceleration in the inertial frame.

Once the desired $\beta_1$ and $\beta_2$ angles are computed, the required actuator angles $\alpha_1$ and $\alpha_2$ need to be computed to move the pendulum to the desired position.

The transformation matrix from the inertial frame $I$ to the pendulum frame $p$ can be described both by equation (\ref{ITP}) and the product of ${}^I \bm{T}_S$ and ${}^S \bm{T}_P$ from equations (\ref{ITS}) and (\ref{STP}) as shown in equation (\ref{R_eq1}).

\begin{equation} \label{ITP}
	^I \bm{T}_P (\beta_1, \beta_2) = \bm{R}_{z, \beta_2} \bm{R}_{x, \beta_1}
\end{equation}

\begin{equation} \label{ITS}
	{}^I \bm{T}_S (\varphi, \vartheta, \psi) = \bm{R}_{x,\varphi} \bm{R}_{y,\vartheta} \bm{R}_{z,\psi}
\end{equation}

\begin{equation} \label{STP}
	{}^S \bm{T}_P (\alpha_1, \alpha_2) = \bm{R}_{x,\alpha_1} \bm{R}_{y,\alpha_2}
\end{equation}

\begin{equation} \label{R_eq1}
	{}^I \bm{T}_P (\beta_1, \beta_2) = {}^I \bm{T}_S (\varphi, \vartheta, \psi) {}^S \bm{T}_P (\alpha_1, \alpha_2)
\end{equation}

This allows the transformation matrix composed of the actuator angles ${}^S \bm{T}_P (\alpha_1, \alpha_2)$ to be calculated.

\begin{equation} \label{R_eq2}
	{}^S \bm{T}_P (\alpha_1, \alpha_2) = ({}^I \bm{T}_S (\varphi, \vartheta, \psi))^{-1} {}^I \bm{T}_P (\beta_1, \beta_2)
\end{equation}

As the transformation matrices are orthogonal, the transpose of the matrix could be used instead of inverse, as shown in equation (\ref{R_eq3}).

\begin{equation} \label{R_eq3}
{}^S \bm{T}_P (\alpha_1, \alpha_2) = ({}^I \bm{T}_S (\varphi, \vartheta, \psi))^T {}^I \bm{T}_P (\beta_1, \beta_2)
\end{equation}

The left hand side of equation (\ref{R_eq3}) is a two axis rotation, whereas the right hand side can be any orthogonal matrix. This requires a virtual rotation matrix to be multiplied to the left hand side to allow for complete three rotation. A rotation matrix about the z-axis by a virtual angle $a_v$ is used as shown in equation (\ref{R}).

\begin{equation} \label{R}
	\bm{R} = {}^S \bm{T}_P (\alpha_1, \alpha_2) = \bm{R}_{x,\alpha_1} \bm{R}_{y,\alpha_2} R_{z,\alpha_v}
\end{equation}

The required actuator angles $\alpha_1$ and $\alpha_2$ can be extracted from the rotation matrix $\bm{R}$ using equations (\ref{alpha1}) and (\ref{alpha2}). The equations are selected so that the virtual angle $\alpha_v$ does not affect the actuator angles.

\begin{equation} \label{alpha1}
	\alpha_1 = \textrm{atan2}(-\bm{R}(1.2), \bm{R}(2,2))
\end{equation}

\begin{equation} \label{alpha2}
	\alpha_2 = \textrm{atan2}(\bm{R}(0,2), \sqrt{\bm{R}(0,0)^2 + \bm{R}(0,1)^2})
\end{equation}

\subsection{Partial Feedback Linearization}

The system equations of motion can be separated into the unactuated and actuated equations as shown in equation (\ref{sepdyn}).

\begin{equation} \label{sepdyn}
	\begin{bmatrix}
	\bm{0} \\ \bm{f}
	\end{bmatrix} = \begin{bmatrix}
	\bm{H}_{11} && \bm{H}_{12} \\
	\bm{H}_{21} && \bm{H}_{22}
	\end{bmatrix} \begin{bmatrix}
	\bm{\ddot{\gamma}}_1 \\
	\bm{\ddot{\gamma}}_2
	\end{bmatrix} + \begin{bmatrix}
	\bm{d}_1 \\
	\bm{d}_2
	\end{bmatrix} + \begin{bmatrix}
	\bm{g}_1 \\
	\bm{g}_2
	\end{bmatrix}
\end{equation}

Where the vector of forces $\bm{f} = [F_1, F_2]^T$, the unactuated state vector accelerations $\bm{\ddot{\gamma}}_1 = [ \ddot{x}, \ddot{y}, \ddot{\varphi}, \ddot{\vartheta}, \ddot{\psi}]^T$, the actuated state vector accelerations $\bm{\ddot{\gamma}}_2 = [\ddot{\alpha_1}, \ddot{\alpha_2}]^T$, $\bm{H}_{11}$, $\bm{H}_{12}$, $\bm{H}_{21}$ and $\bm{H}_{22}$ are the components of the system mass matrix $\bm{H}$, $\bm{d}_1$ and $\bm{d}_2$ are the unactuated and actuated components of the vector of Coriolis, centripetal and frictional forces $\bm{d}$ and $\bm{g}_1$ and $\bm{g}_2$ are the unactuated and actuated components of the gravitational force vector $\bm{g}$.

\begin{equation} \label{unactuated}
	\bm{0} = \bm{H}_{11} \bm{\ddot{\gamma}}_1 + \bm{H}_{12} \bm{\ddot{\gamma}}_2 + \bm{d}_1 + \bm{g}_1
\end{equation}

\begin{equation} \label{actuated}
	\bm{f} = \bm{H}_{21} \bm{\ddot{\gamma}}_1 + \bm{H}_{22} \bm{\ddot{\gamma}}_2 + \bm{d}_2 + \bm{g}_2
\end{equation}

Equation (\ref{unactuated}) can be solved for $\bm{\ddot{\gamma}}_1$ and substituted into equation (\ref{actuated}) to get equation (\ref{solved}).

\begin{align} \label{solved}
	\bm{f} &= (\bm{H}_{22} - \bm{H}_{21} \bm{H}_{11}^{-1} \bm{H}_{12}) \bm{\ddot{\gamma}}_2 + \bm{d}_2 + \bm{g}_2 \nonumber \\ &- \bm{H}_{21} \bm{H}_{11}^{-1} (\bm{d}_1 + \bm{g}_1)
\end{align}

The actuated state vector accelerations can be replaced with an input vector $\bm{u}$ which can be computed using a PD controller where the gains $K_{p,2}$ and $K_{d,2}$ can be set for a desired natural frequency and damping ratio.

\begin{equation} \label{input}
	\bm{u} = K_{p,2} \begin{bmatrix}
	\alpha_{1,d} - \alpha_1 \\
	\alpha_{2,d} - \alpha_2
	\end{bmatrix} + K_{d,2} \frac{d}{dt} \begin{bmatrix}
	\alpha_{1,d} - \alpha_1 \\
	\alpha_{2,d} - \alpha_2
	\end{bmatrix}
\end{equation}

The complete block diagram of the control system is illustrated in Figure \ref{fig:ctlr}.

\textbf{FIGURE: full page wide diagram of the controller block diagram.}

\section{Simulation and Results}

The equations of motion derived in the dynamic modeling section were integrated using a fourth order Runga-Kutta integrator to simulate the robot and controller. The direction control functionality was tested by commanding a certain desired velocity vector within the designed operational range and plotting the achieved velocity vector components. The control system was also tested with a desired trajectory input and comparing against the simulated robot trajectory.

\subsection{Direction Control}

stuff here (1 plot and discussion)

\subsection{Trajectory Tracking Control}

stuff here (1 plot and discussion)

\section{Conclusion}

stuff here (say the controller works, pretty much).

\subsection{Future Development}

stuff here (talk about robot being built for experimental validation and proving stability of the controller)

\acknowledgments
This research is part of a senior capstone project at Embry-Riddle Aeronautical University - Prescott campus. The complete design team, incurve includes Gabe Bentz, John Cybulski and Micaela Stewart. The authors would like to thank the rest of the team, our professor Dr. Douglas Isenberg and the Embry-Riddle development fund for their help and support.

\bibliographystyle{IEEEtran}
\bibliography{references}

\end{document}
