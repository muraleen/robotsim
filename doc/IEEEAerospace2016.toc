\contentsline {subsubsection}{\bfseries Abstract}{1}
\contentsline {section}{\numberline {1.}Introduction}{1}
\contentsline {section}{\numberline {2.}Methodology}{2}
\contentsline {subsection}{System Dynamics}{2}
\contentsline {subsubsection}{Space Robot Model}{2}
\contentsline {subsubsection}{Force-Torque Sensor Model}{4}
\contentsline {subsubsection}{Robotic Platform Model}{4}
\contentsline {subsection}{Control Strategies}{4}
\contentsline {subsubsection}{HIL Simulation Tracking Controller}{4}
\contentsline {subsubsection}{Force-Feedback Control}{5}
\contentsline {section}{\numberline {3.}Simulation and Results}{5}
\contentsline {subsection}{Simulation Method}{5}
\contentsline {subsection}{HIL Simulation Verification}{6}
\contentsline {subsection}{Trajectory Errors}{7}
\contentsline {subsection}{Conservation of Momentum}{7}
\contentsline {subsection}{Results Across Multiple System Excitation Input Sets}{7}
\contentsline {section}{\numberline {4.}Conclusion}{8}
\contentsline {section}{Acknowledgments}{9}
\contentsline {section}{References}{9}
