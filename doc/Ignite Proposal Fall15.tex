\documentclass[]{article}

\usepackage[letterpaper, portrait, margin=1in]{geometry}
\usepackage{graphicx}

\setlength{\parindent}{2em}
\setlength{\parskip}{1em}

\begin{document}
\begin{titlepage}
	\centering
	\LARGE Autonomous Redundant Space Robot Control:\\
	Robotic Platform Construction and Testing\par
	\large
	\vspace{20mm}
	\textbf{Proposed by:}\\
	\vspace{5mm}
	\textbf{Narendran Muraleedharan (Team Lead)}\\
	Aerospace Engineering, 2016\\
	muraleen@my.erau.edu\\
	\vspace{5mm}
	\textbf{Ricardo Fernandez Garcia}\\
	Aerospace Engineering, 2016\\
	fernar10@my.erau.edu\\
	\vspace{20mm}
	\textbf{Faculty Mentors:}\\
	\vspace{5mm}
	\textbf{Dr. Douglas Isenberg}\\
	Aerospace and Mechanical Engineering\\
	isenberd@erau.edu\\
	\vspace{5mm}
	\textbf{Dr. Iacopo Gentilini}\\
	Aerospace and Mechanical Engineering\\
	gentilii@erau.edu\\
	\vspace{20mm}
	\textbf{Funding Request:}\\
	\vspace{5mm}
	\$4,608.00\\
	\vspace{20mm}
	\textbf{Funding Period:}\\
	\vspace{5mm}
	October 5, 2015 - December 14, 2015	
\end{titlepage}

\section{Project Summary}
An Autonomous Redundant Space Robot (ARSR) is a satellite with one or more robotic manipulators, designed to either rendezvous with target satellites to perform on-orbit servicing (OOS) or perform orbital debris removal. Autonomous unmanned systems are preferred for such tasks to eliminate the human risk factor and minimize development and launch costs \cite{BekeyG2008}. A hardware platform is required to sufficiently test such systems on Earth prior to launch, as the number of in-orbit failures have exceeded the number of rocket launch failures in recent years \cite{FloresAbadA2014, ElleryA2008}. Common hardware simulations methods used today include Hardware-in-the-Loop (HIL) simulation, air-bearing tables and parabolic flight\cite{XuW2011}.

The project is a continuation of the Summer 2015 URI Research Project titled \textit{Autonomous Redundant Space Robot Control}. During the summer, the research project focused on developing a method to re-create a planar free-floating environment on Earth. The method uses a Force-Torque sensor mounted on a 2-axis robotic platform (RP) controller with force-feedback from the sensor. The research team developed a software simulation program and tested the use of force-feedback control against a commonly used method - HIL simulation. A wide array of control sets were used and it was proven in simulation that the method developed by the team performs more reliably in simulating a free-floating environment when compared to HIL simulation with a 10\% uncertainty in knowledge of system dynamics.

The research team would now like to focus on constructing the RP and force-feedback controller. The Fall 2015 semester is intended to be spent on acquiring required components, assembling the RP, fabricating and programming the controller and performing experimental validation of the simulations conducted during the summer.

\clearpage

\section{Project Description}
\subsection{Background}

The field of Space Robotics has seen significant advancements over the last few decades. The risk factor involved in launching human space-flight missions is still relatively high .\cite{JerryH2013, CongressOTA1989} The use of unmanned autonomous systems will not only eliminate the human risk factor but also eliminate the need for life support systems and cut mission costs \cite{BekeyG2008}. Space Robots can be used to perform a wide variety of missions in orbit, such as spacecraft maintenance and debris removal. The number of failures in orbit have exceeded the number of rocket launch failures in recent years \cite{FloresAbadA2014, ElleryA2008}. The limited ability for direct human intervention during these missions can inhibit problem solving capabilities for such failures. Therefore, space robots need to be tested thoroughly prior to launch.

For mission tasks such as debris removal and orbital maintenance, the robotic manipulator plays a key role and hence can be of a significant size and weight compared to the spacecraft itself. This poses an interesting challenge to testing on Earth due to the complex dynamic coupling between the spacecraft and the robotic manipulator. The robotic manipulator can no longer be tested with the base fixed at a location, as motion of the manipulator joints can cause a significant displacement and rotation of the base at the mount point \cite{XuY1993}. This creates a need for a realistic and reliable hardware testing mechanism.

A variety of ground experiment systems are currently used for on-Earth spacecraft testing such as Hardware-in-the-Loop (HIL) simulations, air-bearing tables and parabolic flight \cite{XuW2011} by various facilities around the world.

HIL Simulations (also known as Hybrid Experiment Systems due to the combination of a mechanical and mathematical model) are used when the primary experimental objective is to test subsystems on the spacecraft instead not pertaining to motion. The system dynamics are computed using the mathematical model and a target trajectory is generated. The trajectory is tracked in real-time by a robotic manipulator. This method provides a powerful tool to test hardware and control methods on-board that are very difficult to emulate with a software simulation such as computer-vision-based docking systems (similar to the HIL simulation testing of Neptec's laser camera system by the Canadian Space Agency \cite{AghiliF2008}). To attain a realistic scenario from HIL simulations, a precise dynamic model of the system is necessary \cite{XuW2011}. Uncertainties and inaccuracies in the knowledge of dynamic parameters are however, imminent in real-world experimental setups. HIL Simulations with a good contact-force model can be used to test docking hardware and control systems, as done by the European Proximity Operations Simulator (EPOS) at the German Aerospace Center (DLR) \cite{BogeT2011, MaO2012}.

An air-bearing table is a flat surface, usually made of Granite \cite{SchwartzJ2003} where a model spacecraft is placed and uses pressurized gas flowing through a number of air bearings to create a 10 micros gas film between the spacecraft and the surface. This method is used at facilities including the Stanford Aerospace Robotics Lab and the National Technical University of Athens to test docking and collision avoidance systems \cite{PapadopoulosE2008, PapadopoulosE2011}. An air-bearing table eliminates friction between the surface and the spacecraft, however it limits the spacecraft to planar motion (2D).\par
\vspace{5mm}
A true spatial (3D) micro-gravity simulation method used to test space robotic systems is parabolic flight. This has been used jointly by the Tokyo Institute of Technology and the National Space Development Agency of Japan to test a 4-DOF space robotic arm. The method is capable of achieving gravitational accelerations as low as 0.02g, however the experimentation is limited to a very small time frame (approximately 20s) \cite{SawadaH2004}. This prevents the method from being used to test time-consuming missions such as docking and on-orbit servicing.

We developed a method which uses force-feedback control to drive the physical simulation. Notable work have been done previously on the use of force-feedback control for orbital simulations, but they have been used in combination with HIL simulation. The EPOS facility employs a 6-DOF force-moment sensor at the docking interface and uses an "admittance model" \cite{DubowskyS1988} (similar to that used for assistance-as-needed robots \cite{CarmichaelM2013}) to generate the appropriate motions. Tests conducted on the EPOS robot at DLR the viability of the method for docking simulations by verifying the EPOS HIL Simulation against an independent zero-gravity computerized simulation \cite{MaO2012}. The method still requires knowledge of the system dynamics to operate. Placing the force-moment sensor at the base of the robot rather than the end effector allow simulating the robot's behavior with respect to forces acting not only on the end-effector but also the structure \cite{OttC2009}. This has been implemented for space robotics testing on the Vehicle Emulation System Model II (VES II) test bed at MIT. The VES II feeds the measurements from the force-torque sensor at the mount between the simulation platform and the manipulator along with a gravity wrench estimate into the vehicle's admittance model and computerized dynamic simulations to generate position control signals for the platform \cite{DubowskyS1994}. Our method emulates the dynamics of the entire system without the use of computational dynamics integration. The method focuses on recreating a planar free-floating environment as a proof-of-concept but a similar method can be integrated into 3D hardware simulation with the use of a physical (eg. Suspension System \cite{XuY1994}) or a computational (eg. Gravity Wrench Estimate \cite{DubowskyS1994}) gravity compensation system.

\begin{figure}
    \centering
    \includegraphics[width=5in]{pdf/sim_scr}
    \caption{Screenshot of Simulation showing the ARSR mounted on the RP}
    \label{pdf/sim_scr}
\end{figure}

A simulation software was developed in C++ (See Figure \ref{pdf/sim_scr}) and was used to test the developed method against HIL simulation with a 10\% uncertainty in the knowledge of system dynamics. The force-feedback control method proved to be more reliable in conserving linear and angular momentum and minimizing trajectory errors.

\subsection{Project Thesis}

A planar free-floating environment can be emulated on Earth with the use of a force-torque sensor transducer mounted on a 2-axis RP and a controller designed to drive the reaction forces measured on the sensor to zero.

\subsection{Objectives}

The following are short term objectives for the project to be completed by the end of the Fall 2015 semester.
\begin{enumerate}
	\item Construct a 2-axis (translation along x and y axes, rotation about z axis) RP.
	\item Program the force-feedback controller to read measurements from a force-torque sensor transducer and send outputs to the RP to drive the readings to zero.
	\item Construct a planar ARSR with a 4-DOF robotic manipulator, a reaction wheel and 8 reaction control system (RCS) thrusters.
	\item Verify software simulations by performing hardware experiments with the ARSR and RP.
	\item Publish the experimental results in the form of a research paper to complement the paper describing the simulation results.
\end{enumerate}
Long term project goals include designing and constructing a spatial (3D - translation along x, y and z axes, rotation about x, y and z axes) free-floating environment recreation RP with the use of a similar technique combined with a gravity compensation system. Once a reliable testing RP has been developed, the project will focus on using supervised machine-learning techniques to develop a robust control system for the ARSR.

\subsection{Approach}

The College of Engineering has already acquired a 6-axis force-torque sensor transducer and two RPs which the research team is currently setting up for the project. One the RPs (YAMAHA FXYx) use a position controller (DRCX) to send user inputs to the actuators while a controller was not provided with the other RP (Hirata Cartesian Robot). We would like to replace the AC servo motors on the Hirata RP with affordable brushed DC motors and drivers and use a micro-controller to minimize the control system latency and program the custom force-feedback controller.

The ARSR structure will be fabricated using scrap parts from the AXFAB and will use DC servo actuators for the manipulator arm.

The Electrical Engineering department's PCB mill will be used to fabricate any custom circuits to be used in the project.

On completion of the RP construction, various control sets will be programmed into the ARSR and the RP will be used for both HIL simulation tests and force-feedback control tests. The reaction forces at the interface will be logged to compare the performance of the two methods during the set of experiments.

\subsection{Expected Outcomes}

The experimental results are expected to back the simulation results of the force-feedback control method. The results will be published and a detailed analysis will be made. The experiments will give insight to the effects of real-world noise and data discretization on the control system.

Regardless of whether the developed method produces the desired outputs, the project will result in the University's Robotics Laboratory possessing a working 2-axis robotic platform which can be used for educational use or other research projects.

The research team would like to continue using the platform with either the force-feedback control method or HIL simulation method, whichever performs better as a test-bed to develop a robust control system for ARSRs.

\subsection{Safety Considerations}

As the project primarily consists of construction and fabrication, a lot of the work will be conducted in the AXFAB machine shop. Both student researchers have gone through the required safety briefings and have been checked out to use the Machine Shop.

The RP uses high voltage AC controllers and actuators and the required safety measures (proper insulation etc.) will be taken when working with the devices.

\subsection{Planned Outreach Events}

To outreach the community, the team has decided to have a poster and present our research project to the community on the Open House organized by Embry Riddle on October 17, 2015. Our second activity will be cooperating with the Spot Museum during their Open House and improve their robotics section in collaboration with Judy L. Paris, M.Ed. and Dr. Gentilini. Since the team members have both, hands on and in class experience with robotics, we think we will be a valuable asset for the Spot Museum.

\clearpage

\section{Researchers Background and Responsibility}

The student researchers are Aerospace Engineering students with Robotics minors (completed Robotics sequence) and have worked on multiple research projects funded through the Ignite and E-prize grants including the \textit{Autonomous Redundant Space Robot Control} project over the summer which serves as the basis for this proposal. Both researchers have experience with designing and fabricating custom PCBs for other prior projects.

Narendran will be primarily focusing on designing the control system, setting up and performing the series of experiments and porting code developed for the simulation software into the hardware controller. Ricardo will be tasked with fabricating custom components and assembling the RP and ARSR.

\clearpage

\section{Project Timeline}

The majority of the project will be completed by the end of the Fall 2015 semester. The research team will attempt to finish any remaining tasks during the month of December 2015. The planned project timeline is as follows:

\vspace{5mm}
\begin{tabular}{ll}
\textbf{Oct 5 - Oct 11, 2015} & Complete selection of RP components and place order\\
& Preliminary design of ARSR
\vspace{5mm}\\
\textbf{Oct 12 - Oct 18, 2015} & Construct insulation enclosures for high voltage circuits
\vspace{5mm}\\
\textbf{Oct 19 - Oct 15, 2015} & Detail design of ARSR
\vspace{5mm}\\
\textbf{Oct 26 - Nov 1, 2015} & Complete selection of ARSR components and place order
\vspace{5mm}\\
\textbf{Nov 2, 2015 - Nov 8, 2015} & Replace motors on RP with purchased components
\vspace{5mm}\\
\textbf{Nov 9, 2015 - Nov 15, 2015} & Wiring and assembly of RP and ARSR
\vspace{5mm}\\
\textbf{Nov 16, 2015 - Nov 22, 2015} & Controller design for RP and ARSR
\vspace{5mm}\\
\textbf{Nov 23, 2015 - Nov 20, 2015} & Assembly of RP and ARSR system\\
& Set up experiments for validation
\vspace{5mm}\\
\textbf{Nov 30, 2015 - Dec 6, 2015} & Perform series of validation experiments
\vspace{5mm}\\
\textbf{Dec 7, 2015 - Dec 13, 2015} & Prepare IEEE conference paper and submit to IROS 2016
\end{tabular}

\clearpage

\section{Project Budget}
The project budget is divided into hardware components required for the successful completion of the objectives (\$2608.00) and student hours dedicated to the research (\$2000.00).
\subsection{Hardware Components}
\begin{tabular}{llrrr}
\textbf{S.no} & \textbf{Item Description} & \textbf{Unit Cost} & \textbf{Quantity} & \textbf{Total Cost}\\
\hline
\hline
1 & Beaglebone Black Microcontroller & \$85.00 & 2 & \$170.00\\
2 & HS-7985MG High Torque Servos & \$85.00 & 4 & \$340.00\\
3 & Banebots FIRST CIM 12v DC Motor & \$28.00 & 1 & \$28.00\\
4 & Applied Motion V0250 Motor & \$600.00 & 3 & \$1800.00\\
5 & DC Brushed Motor Driver & \$70.00 & 3 & \$210.00\\
6 & Quadrature Motor Encoder & \$20.00 & 3 & \$60.00\\
\hline
&&&& \textbf{\$2,608.00}
\end{tabular}


\subsection{Research Hours}
\begin{tabular}{lrrrr}
\textbf{Researcher} & \textbf{Hours/Week} & \textbf{Weeks} & \textbf{Pay Rate} & \textbf{Total Pay}\\
\hline
\hline
Narendran Muraleedharan & 10 & 10 & \$10.00 & \$1,000.00\\
Ricardo Fernandez Garcia & 10 & 10 & \$10.00 & \$1,000.00\\
\hline
&&&& \textbf{\$2,000.00}
\end{tabular}

\clearpage

\bibliographystyle{unsrt}
\bibliography{IEEEAerospace2016}

\end{document}          
