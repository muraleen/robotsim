\contentsline {subsubsection}{\bfseries Abstract}{1}
\contentsline {section}{\numberline {1.}Introduction}{1}
\contentsline {section}{\numberline {2.}Dynamic Modeling}{1}
\contentsline {section}{\numberline {3.}Control System Design}{2}
\contentsline {subsection}{Task-space to Joint-space transformation}{2}
\contentsline {subsection}{Partial Feedback Linearization}{3}
\contentsline {section}{\numberline {4.}Simulation and Results}{3}
\contentsline {subsection}{Direction Control}{3}
\contentsline {subsection}{Trajectory Tracking Control}{3}
\contentsline {section}{\numberline {5.}Conclusion}{3}
\contentsline {subsection}{Future Development}{3}
\contentsline {section}{Acknowledgments}{3}
\contentsline {section}{References}{3}
