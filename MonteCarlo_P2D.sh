#!/bin/sh

dt=0.001	# Simulation delta-time (s)
Ts=0.01		# Controller update interval (s)
Tl=0.01		# Logging update interval (s)
Tf=20		# Simulation run time (s)

#for i in `seq 0 54`
#do
#	echo -n "Running simulation (force) with control set $i ... "
#	./robotsim --model=P2D --dt=$dt --Tl=$Tl --Ts=$Ts --tsim=$Tf --no-gfx --log=log/forceASR/set_${i}.csv --prop:/controller/MCS_ctlSet=$i
#	echo "DONE!"
#done

#for i in `seq 0 54`
#do
#	echo -n "Running simulation (trackAT) with control set $i ... "
#	./robotsim --model=P2D --dt=$dt --Tl=$Tl --Ts=$Ts --tsim=$Tf --no-gfx --log=log/trackAT/set_${i}.csv --prop:/controller/MCS_ctlSet=$i --prop:/controller/MCS_logFile=log/AT/set_${i}.csv --ctlr=cxx
#	echo "DONE!"
#done

for i in `seq 0 54`
do
	echo -n "Running simulation (trackHT) with control set $i ... "
	./robotsim --model=P2D --dt=$dt --Tl=$Tl --Ts=$Ts --tsim=$Tf --no-gfx --log=log/trackHT/set_${i}.csv --prop:/controller/MCS_ctlSet=$i --prop:/controller/MCS_logFile=log/HT/set_${i}.csv --ctlr=cxx
	echo "DONE!"
done
