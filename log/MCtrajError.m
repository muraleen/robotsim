ref = 'AT';
refIndex = [8 9 2 7 3 4 5 6 16 17 10 15 11 12 13 14];

fname = ['RMS/trajectory/' MCSet '.csv'];

pRMS = [];
oRMS = [];
jRMS = [];

for jj = 0:54

	logData_raw = csvread([MCSet '/set_' num2str(jj) '.csv'], 1, 0);
	refData_raw = csvread([ref '/set_' num2str(jj) '.csv'], 1, 0);

	logData = logData_raw(:, MCIndex);
	refData = refData_raw(:, refIndex);

	xRMS = rms(logData(:,1) - refData(:,1));
	yRMS = rms(logData(:,2) - refData(:,2));
	
	pRMS = [pRMS sqrt(xRMS.^2 + yRMS.^2)];
	
	oRMS = [oRMS rms(logData(:,3) - refData(:,3))];
	
	jRMS = [jRMS rms([rms(logData(:,4)-refData(:,4)) rms(logData(:,5)-refData(:,5)) rms(logData(:,6)-refData(:,6)) rms(logData(:,7)-refData(:,7)) rms(logData(:,8)-refData(:,8))])];

end

csvwrite(fname, [pRMS.' oRMS.' jRMS.']);

sqrt(rms(xRMS)^2 + rms(yRMS)^2)
rms(jRMS)
