data1 = 'AT/set_0.csv';
index1 = [8 9 2 7 3 4 5 6 16 17 10 15 11 12 13 14];

data2 = 'trackHT/set_0.csv';
index2 = [11 12 5 10 6 7 8 9 22 23 16 21 17 18 19 20];

log1_raw = csvread(data1, 1, 0);
log2_raw = csvread(data2, 1, 0);

log1_ordered = log1_raw(:, index1);
log2_ordered = log2_raw(:, index2);

t = log1_raw(:,1);

figure(1);

for ii = 1:8

	subplot(4,2,ii); hold on;
	
	plot(t.', log1_ordered(:,ii), 'b'); hold on;
	plot(t.', log2_ordered(:,ii), 'r'); grid on;

end
