% Time plot of Monte Carlo Simulations

joints = [1:3 5:8];

figure(1);

linkName = {'X-position', 'Y-position', 'Attitude', 'Momentum Wheel', 'Link 1', 'Link 2', 'Link 3', 'Link 4'};
yName = {'Error (m)', 'Error (m)', 'Error (rad)', 'Error (rad)', 'Error (rad)', 'Error (rad)', 'Error (rad)', 'Error (rad)'};

for joints = 5:8

	subplot(2,2,joints-4);  hold on;

	RMSvect = [];

	% for ii = [0:242]
	
		ii = 0;

		control_log = csvread(["HT/set_" num2str(ii) ".csv"]);
		subject_log = csvread(["AT/set_" num2str(ii) ".csv"]);
		%subject_log = csvread(["MC_P2D/set_" num2str(ii) ".csv"]);

		t = control_log(:,1);

		% Compute RMS Error	
		control_data = control_log(:, [8 9 2 7 3 4 5 6]);
		subject_data = subject_log(:, [8 9 2 7 3 4 5 6]); % <-- ARSR
		%subject_data = subject_log(:, [11 12 5 10 6 7 8 9]); % <-- P2D
	
		MSerror = zeros(size(control_data),1);
	
		for jj = joints
			MSerror = MSerror + (angle(exp(j * (control_data(:,jj) - subject_data(:,jj))))).^2;
		end

		RMSerror = sqrt(MSerror);
		RMSvect = [RMSvect RMSerror];
	
		%/sqrt(max(control_log(:,2))^2 + max(control_log(:,3))^2);
		%/max(abs(control_log(:,4)));

	%Vmw_freq = Ufreq[(math.floor(ctlSet / 625) % 5) + 1]
	%V1_freq = Ufreq[(math.floor(ctlSet / 125) % 5) + 1]
	%V2_freq = Ufreq[(math.floor(ctlSet / 25) % 5) + 1]
	%V3_freq = Ufreq[(math.floor(ctlSet / 5) % 5) + 1]
	%V4_freq = Ufreq[(ctlSet % 5) + 1]

	%	f(1) = mod(floor(ii/625),5) + 1;
	%	f(2) = mod(floor(ii/125),5) + 1;
	%	f(3) = mod(floor(ii/25),5) + 1;
	%	f(4) = mod(floor(ii/5),5) + 1;
	%	f(5) = mod(floor(ii),5) + 1;

	%	if sum(f) == 25
	%		plot3(control_log(:,2).', control_log(:,3).', t.', 'b'); hold on;
	%		plot3(subject_log(:,2).' + subject_log(:,5).', subject_log(:,3).' + subject_log(:,6).', t.', 'r'); hold on;
	%		view(45,45);
	%	elseif sum(f) == 25
	%		plot(control_log(:,2).', control_log(:,3).', 'b'); hold on;
	%		plot(subject_log(:,2).' + subject_log(:,5).', subject_log(:,3).' + subject_log(:,6).', 'r'); hold on;
	%	end
	
		% Plot RMS error vs. time
		%plot(t.', RMSerror.', 'Color', cmap(ii+1,:)); hold on;
		%plot(t.', RMSerror.', 'b'); hold on;
	% end

	plot(t.', mean(RMSvect.'), 'r'); hold on;
	plot(t.', max(RMSvect.'), 'r--');
	plot(t.', min(RMSvect.'), 'r--');

	grid on;
	%axis([0 20 0 0.025]);

	RMSvect = [];

	for ii = [0:242]

		control_log = csvread(["MC_ARSR/set_" num2str(ii) ".csv"]);
		%subject_log = csvread(["MC_ARSR_10off/set_" num2str(ii) ".csv"]);
		subject_log = csvread(["MC_P2D/set_" num2str(ii) ".csv"]);

		t = control_log(:,1);

		% Compute RMS Error	
		control_data = control_log(:, [8 9 2 7 3 4 5 6]);
		%subject_data = subject_log(:, [8 9 2 7 3 4 5 6]); % <-- ARSR
		subject_data = subject_log(:, [11 12 5 10 6 7 8 9]); % <-- P2D
	
		MSerror = zeros(size(control_data),1);
	
		for jj = joints
			MSerror = MSerror + (angle(exp(j * (control_data(:,jj) - subject_data(:,jj))))).^2;
		end

		RMSerror = sqrt(MSerror);
		RMSvect = [RMSvect RMSerror];
	
		%/sqrt(max(control_log(:,2))^2 + max(control_log(:,3))^2);
		%/max(abs(control_log(:,4)));

	%Vmw_freq = Ufreq[(math.floor(ctlSet / 625) % 5) + 1]
	%V1_freq = Ufreq[(math.floor(ctlSet / 125) % 5) + 1]
	%V2_freq = Ufreq[(math.floor(ctlSet / 25) % 5) + 1]
	%V3_freq = Ufreq[(math.floor(ctlSet / 5) % 5) + 1]
	%V4_freq = Ufreq[(ctlSet % 5) + 1]

	%	f(1) = mod(floor(ii/625),5) + 1;
	%	f(2) = mod(floor(ii/125),5) + 1;
	%	f(3) = mod(floor(ii/25),5) + 1;
	%	f(4) = mod(floor(ii/5),5) + 1;
	%	f(5) = mod(floor(ii),5) + 1;

	%	if sum(f) == 25
	%		plot3(control_log(:,2).', control_log(:,3).', t.', 'b'); hold on;
	%		plot3(subject_log(:,2).' + subject_log(:,5).', subject_log(:,3).' + subject_log(:,6).', t.', 'r'); hold on;
	%		view(45,45);
	%	elseif sum(f) == 25
	%		plot(control_log(:,2).', control_log(:,3).', 'b'); hold on;
	%		plot(subject_log(:,2).' + subject_log(:,5).', subject_log(:,3).' + subject_log(:,6).', 'r'); hold on;
	%	end
	
		% Plot RMS error vs. time
		%plot(t.', RMSerror.', 'Color', cmap(ii+1,:)); hold on;
		%plot(t.', RMSerror.', 'b'); hold on;
	end

	plot(t.', mean(RMSvect.'), 'b');
	plot(t.', max(RMSvect.'), 'b--');
	plot(t.', min(RMSvect.'), 'b--');
	
	title(linkName(joints));
	xlabel('t (s)');
	ylabel(yName(joints));
	
	pause(0.001);
	
end

legend('10% deviation:MEAN', '10% deviation:MAX', '10% deviation:MIN', 'Platform Ctld:MEAN', 'Platform Ctld:MAX', 'Platform Ctld:MIN')
