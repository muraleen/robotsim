function [out] = wrap(in, low, high)

	out = in;

	while out > high
		out = out + (low - high);
	end
	
	while out < low
		out = out + (high - low);
	end

end
