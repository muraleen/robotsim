ARSR_index = [8 9 2 7 3 4 5 6];
P2D_index = [11 12 5 10 6 7 8 9];

trackASR.log = 'trackASR/set_12.csv';
trackASR.index = P2D_index;

forceASR.log = 'forceASR/set_12.csv';
forceASR.index = P2D_index;

% Reference: ACTUAL TRAJECTORY
AT.log = 'AT/set_12.csv';
AT.index = ARSR_index;

% Read log files
trackASR.rawdata = csvread(trackASR.log, 1, 0);
trackASR.data = trackASR.rawdata(:, trackASR.index);

forceASR.rawdata = csvread(forceASR.log, 1, 0);
forceASR.data = forceASR.rawdata(:, forceASR.index);

AT.rawdata = csvread(AT.log, 1, 0);
AT.data = AT.rawdata(:, AT.index);

t = AT.rawdata(:,1);

trackASR.errors = trackASR.data - AT.data;
forceASR.errors = forceASR.data - AT.data;

trackASR.RMS = [rms(trackASR.errors(:,1)); rms(trackASR.errors(:,2)); rms(trackASR.errors(:,3))];
forceASR.RMS = [rms(forceASR.errors(:,1)); rms(forceASR.errors(:,2)); rms(forceASR.errors(:,3))];

trackASR.RMS
forceASR.RMS
