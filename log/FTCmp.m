indexP2D = [40 41 42];

for cs = 17

	trackHT.file = ['trackHT/set_' num2str(cs) '.csv'];
	trackHT.index = indexP2D;
	trackHT.err = 0.1; % ASR
	trackHT.pdata = {trackHT.file, trackHT.index, trackHT.err, 'ASR tracking HT', 'r--'};

	trackAT.file = ['trackAT/set_' num2str(cs) '.csv'];
	trackAT.index = indexP2D;
	trackAT.err = 0.1; % ASR
	trackAT.pdata = {trackAT.file, trackAT.index, trackAT.err, 'ASR tracking AT', 'k:'};

	forceASR.file = ['forceASR/set_' num2str(cs) '.csv'];
	forceASR.index = indexP2D;
	forceASR.err = 0.1; % ASR
	forceASR.pdata = {forceASR.file, forceASR.index, forceASR.err, 'Force-feedback', 'b-'};

	plotData = [trackHT.pdata; forceASR.pdata];

	for ii = 1:size(plotData,1)

		logFile = plotData{ii,1};
		logIndex = plotData{ii,2};
		err = plotData{ii,3};
		label = plotData{ii,4};
		linespec = plotData{ii,5};

		logData = csvread(logFile, 1, 0);

		t = logData(:,1);
		F = logData(:, logIndex);

		figure(1+cs);

		subplot(3,1,1); hold on;
		label
		plot(t(20:end).', F(20:end,1).', linespec); hold on;
		rms(F(200:end,1))
		grid on;
		xlabel('t (s)');
		ylabel('F_x (N)');
		title(['Forces and Torque measured at the Interface - Set ' num2str(cs+1)]);

		subplot(3,1,2); hold on;
		plot(t(20:end).', F(20:end,2).', linespec); hold on;
		rms(F(200:end,2))
		grid on;
		xlabel('t (s)');
		ylabel('F_y (N)');

		subplot(3,1,3); hold on;
		plot(t(20:end).', F(20:end,3).', linespec); hold on;
		rms(F(200:end,3))
		grid on;
		xlabel('t (s)');
		ylabel('\tau_z (Nm)');
	
	end

end
