function [rmsVal] = rms(x)

	rmsVal = 0;

	for ii = 1:length(x)
		rmsVal = rmsVal + x(ii)^2;
	end
	
	rmsVal = sqrt(rmsVal/length(x));

end
