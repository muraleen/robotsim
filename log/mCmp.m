indexARSR = [8 9 2 7 3 4 5 6 16 17 10 15 11 12 13 14];
indexP2D = [11 12 5 10 6 7 8 9 22 23 16 21 17 18 19 20];

for cs = 0

	trackHT.file = ['trackHT/set_' num2str(cs) '.csv'];
	trackHT.index = indexP2D;
	trackHT.err = 0.1; % ASR
	trackHT.pdata = {trackHT.file, trackHT.index, trackHT.err, 'ASR tracking HT', 'r--'};

	trackAT.file = ['trackAT/set_' num2str(cs) '.csv'];
	trackAT.index = indexP2D;
	trackAT.err = 0.1; % ASR
	trackAT.pdata = {trackAT.file, trackAT.index, trackAT.err, 'ASR tracking AT', 'k:'};

	forceASR.file = ['forceASR/set_' num2str(cs) '.csv'];
	forceASR.index = indexP2D;
	forceASR.err = 0.1; % ASR
	forceASR.pdata = {forceASR.file, forceASR.index, forceASR.err, 'Force-feedback', 'b-'};

	plotData = [trackHT.pdata; trackAT.pdata; forceASR.pdata];

	for ii = 1:size(plotData,1)

		logFile = plotData{ii,1};
		logIndex = plotData{ii,2};
		err = plotData{ii,3};
		label = plotData{ii,4};
		linespec = plotData{ii,5};

		logData = csvread(logFile, 1, 0);

		% CONSTANTS
		mB = 6.968.*(1+err);
		BBGamma = [0.1324; 0; -0.0488].*(1+err);
		BBJ = [0.169 0 0; ...
			   0 0.192 0; ...
			   0 0 0.300].*(1+err);

		mMW = 3.52.*(1+err);
		MWMWGamma = [0; 0; -0.176].*(1+err);
		MWMWJ = [0.01736 0 0; ...
				 0 0.01736 0; ...
				 0 0 0.01126].*(1+err);

		mL1 = 0.782.*(1+err);
		L1L1Gamma = [0.147; 0.012; 0.0049].*(1+err);
		L1L1J = [3.2583e-4 0 0; ...
				 0 0.0162 0; ...
				 0 0 0.0162].*(1+err);

		mL2 = 0.782.*(1+err);
		L2L2Gamma = [0.147; 0.012; 0.0049].*(1+err);
		L2L2J = [3.2583e-4 0 0; ...
				 0 0.0162 0; ...
				 0 0 0.0162].*(1+err);

		mL3 = 0.782.*(1+err);
		L3L3Gamma = [0.147; 0.012; 0.0049].*(1+err);
		L3L3J = [3.2583e-4 0 0; ...
				 0 0.0162 0; ...
				 0 0 0.0162].*(1+err);

		mL4 = 0.25.*(1+err);
		L4L4Gamma = [0.0183; 0; 0].*(1+err);
		L4L4J = [3.258e-4 0 0; ...
				 0 2.769e-3 0; ...
				 0 0 2.769e-3].*(1+err);

		BBrL1 = [0.2386; -0.0237; 0];
		L1L1rL2 = [0.248; 0; 0];
		L2L2rL3 = [0.248; 0; 0];
		L3L3rL4 = [0.248; 0; 0];

		t = logData(:,1);
		Xcm = zeros(3, size(logData,1));
		p = zeros(3, size(logData,1));
		h = zeros(3, size(logData,1));

		hB = zeros(3, size(logData,1));
		hMW = zeros(3, size(logData,1));
		hL1 = zeros(3, size(logData,1));
		hL2 = zeros(3, size(logData,1));
		hL3 = zeros(3, size(logData,1));
		hL4 = zeros(3, size(logData,1));

		for ii = 1:size(logData,1)
	
			thisData = logData(ii, logIndex);
	
			% VARIABLES
			IIRB = [thisData(1); thisData(2); 0];
			ITB = rotz(thisData(3));
			BTMW = rotz(thisData(4));
			BTL1 = rotz(thisData(5));
			L1TL2 = rotz(thisData(6));
			L2TL3 =  rotz(thisData(7));
			L3TL4 =  rotz(thisData(8));

			IIRMW = IIRB;
			ITMW = ITB*BTMW;
	
			IIRL1 = IIRB + ITB*BBrL1;
			ITL1 = ITB*BTL1;
	
			IIRL2 = IIRL1 + ITL1*L1L1rL2;
			ITL2 = ITL1*L1TL2;
	
			IIRL3 = IIRL2 + ITL2*L2L2rL3;
			ITL3 = ITL2*L2TL3;
	
			IIRL4 = IIRL3 + ITL3*L3L3rL4;
			ITL4 = ITL3*L3TL4;

			IIRBdot = [thisData(9); thisData(10); 0];	
			ITBdot = rotzdot(thisData(3), thisData(11));
	
			IIRMWdot = IIRBdot;
			ITMWdot = ITBdot*BTMW + ITB*rotzdot(thisData(4), thisData(12));
	
			IIRL1dot = IIRBdot + ITBdot*BBrL1;
			ITL1dot = ITBdot*BTL1 + ITB*rotzdot(thisData(5), thisData(13));
	
			IIRL2dot = IIRL1dot + ITL1dot*L1L1rL2;
			ITL2dot = ITL1dot*L1TL2 + ITL1*rotzdot(thisData(6), thisData(14));
	
			IIRL3dot = IIRL2dot + ITL2dot*L2L2rL3;
			ITL3dot = ITL2dot*L2TL3 + ITL2*rotzdot(thisData(7), thisData(15));

			IIRL4dot = IIRL3dot + ITL3dot*L3L3rL4;
			ITL4dot = ITL3dot*L3TL4 + ITL3*rotzdot(thisData(8), thisData(16));

			IBGamma = IIRB*mB + ITB*BBGamma;
			IMWGamma = IIRMW*mMW + ITMW*MWMWGamma;
			IL1Gamma = IIRL1*mL1 + ITL1*L1L1Gamma;
			IL2Gamma = IIRL2*mL2 + ITL2*L2L2Gamma;
			IL3Gamma = IIRL3*mL3 + ITL3*L3L3Gamma;
			IL4Gamma = IIRL4*mL4 + ITL4*L4L4Gamma;
	
			Xcm(:,ii) = (IBGamma + IMWGamma + IL1Gamma + IL2Gamma + IL3Gamma + IL4Gamma) / (mB + mMW + mL1 + mL2 + mL3 + mL4);

			pB = IIRBdot*mB + ITBdot*BBGamma;
			pMW = IIRMWdot*mMW + ITMWdot*MWMWGamma;
			pL1 = IIRL1dot*mL1 + ITL1dot*L1L1Gamma;
			pL2 = IIRL2dot*mL2 + ITL2dot*L2L2Gamma;
			pL3 = IIRL3dot*mL3 + ITL3dot*L3L3Gamma;
			pL4 = IIRL4dot*mL4 + ITL4dot*L4L4Gamma;

			p(:,ii) = pB + pMW + pL1 + pL2 + pL3 + pL4;
		
			hB(:,ii) = cross(IIRB, pB) - cross(IIRBdot, ITB*BBGamma) + ITB*BBJ*unskew(ITB\ITBdot);
			hMW(:,ii) = cross(IIRMW, pMW) - cross(IIRMWdot, ITMW*MWMWGamma) + ITMW*MWMWJ*unskew(ITMW\ITMWdot);
			hL1(:,ii) = cross(IIRL1, pL1) - cross(IIRL1dot, ITL1*L1L1Gamma) + ITL1*L1L1J*unskew(ITL1\ITL1dot);
			hL2(:,ii) = cross(IIRL2, pL2) - cross(IIRL2dot, ITL2*L2L2Gamma) + ITL2*L2L2J*unskew(ITL2\ITL2dot);
			hL3(:,ii) = cross(IIRL3, pL3) - cross(IIRL3dot, ITL3*L3L3Gamma) + ITL3*L3L3J*unskew(ITL3\ITL3dot);
			hL4(:,ii) = cross(IIRL4, pL4) - cross(IIRL4dot, ITL4*L4L4Gamma) + ITL4*L4L4J*unskew(ITL4\ITL4dot);
		
			h(:,ii) = hB(:,ii) + hMW(:,ii) + hL1(:,ii) + hL2(:,ii) + hL3(:,ii) + hL4(:,ii);
		end

		figure(1+cs);

		subplot(3,1,1); hold on;
		label
		plot(t(50:end).', p(1,50:end).', linespec); hold on;
		rms(p(1,200:end))
		mean(p(1,200:end))
		grid on;
		xlabel('t (s)');
		ylabel('p_x (Ns)');
		title(['Momentum Errors for HIL Simulation - Set ' num2str(cs+1)]);

		subplot(3,1,2); hold on;
		plot(t(50:end).', p(2,50:end).', linespec); hold on;
		rms(p(2,200:end))
		mean(p(2,200:end))
		grid on;
		xlabel('t (s)');
		ylabel('p_y (Ns)');

		subplot(3,1,3); hold on;
		plot(t(50:end).', h(3,50:end).', linespec); hold on;
		rms(h(3,200:end))
		mean(h(3,200:end))
		grid on;
		xlabel('t (s)');
		ylabel('h_z (Nms)');
	
	end

end

