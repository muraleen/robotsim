indexARSR = [8 9 2 7 3 4 5 6];
indexP2D = [11 12 5 10 6 7 8 9];

for cs = 12

	trackHT.file = ['trackHT/set_' num2str(cs) '.csv'];
	trackHT.index = indexP2D;
	trackHT.pdata = {trackHT.file, trackHT.index, 0, 'ASR tracking HT', 'r--'};

	trackAT.file = ['trackAT/set_' num2str(cs) '.csv'];
	trackAT.index = indexP2D;
	trackAT.pdata = {trackAT.file, trackAT.index, 0, 'ASR tracking AT', 'k-'};

	forceASR.file = ['forceASR/set_' num2str(cs) '.csv'];
	forceASR.index = indexP2D;
	forceASR.pdata = {forceASR.file, forceASR.index, 0, 'Force-feedback', 'b-'};

	plotData = [trackHT.pdata; forceASR.pdata];

	ref.file = ['AT/set_' num2str(cs) '.csv'];
	ref.index = indexARSR;

	refData = csvread(ref.file, 1, 0);

	qref = refData(:, ref.index);

	for ii = 1:size(plotData,1)

		logFile = plotData{ii,1};
		logIndex = plotData{ii,2};
		label = plotData{ii,4};
		linespec = plotData{ii,5};

		logData = csvread(logFile, 1, 0);

		t = logData(:,1);
		q = logData(:, logIndex);
	
		dq = q - qref;

		figure(cs+45);

		subplot(2,2,1); hold on;
		label
		plot(t(20:end).', dq(20:end,1).', linespec); hold on;
		rms(dq(200:end,1))
		grid on;
		xlabel('t (s)');
		ylabel('\deltax (m)');
		%title(['ARSR Trajectory Errors - Set ' num2str(cs+1)]);

		subplot(2,2,2); hold on;
		plot(t(20:end).', dq(20:end,2).', linespec); hold on;
		rms(dq(200:end,2))
		grid on;
		xlabel('t (s)');
		ylabel('\deltay (m)');
	
		subplot(2,2,3); hold on;
		plot(t(20:end).', dq(20:end,3).', linespec); hold on;
		rms(dq(200:end,3))
		grid on;
		xlabel('t (s)');
		ylabel('\delta\psi (rad)');
	
		subplot(2,2,4); hold on;
		plot(t(20:end).', dq(20:end,4).', linespec); hold on;
		rms(dq(200:end,4))
		grid on;
		xlabel('t (s)');
		ylabel('\delta\theta_{MW} (rad)');
	
	end

	subplot(2,2,2);
	legend('HIL-ASR-MT','FF-ASR');

end
