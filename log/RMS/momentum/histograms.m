clc; clear; close all;

forceASR = csvread('forceASR.csv');
trackHT = csvread('trackHT.csv');

bins = linspace(0,0.002,40);
sets = 55;

trackHT_bins = zeros(length(bins),3);
forceASR_bins = zeros(length(bins),3);

for ii = 1:length(bins)
  
  if ii < length(bins)
  
    bin = [bins(ii) bins(ii+1)];
    
    for cs = 1:55
      for dat = 1:3
        if trackHT(cs,dat) >= bin(1) && trackHT(cs,dat) < bin(2)
          trackHT_bins(ii,dat) = trackHT_bins(ii,dat) + 1;
        end
        
        if forceASR(cs,dat) >= bin(1) && forceASR(cs,dat) < bin(2)
          forceASR_bins(ii,dat) = forceASR_bins(ii,dat) + 1;
        end
      end
    end
    
  else
  
    for cs = 1:55
      for dat = 1:3
        if trackHT(cs,dat) >= bins(ii)
          trackHT_bins(ii,dat) = trackHT_bins(ii,dat) + 1;
        end
        
        if forceASR(cs,dat) >= bins(ii)
          forceASR_bins(ii,dat) = forceASR_bins(ii,dat) + 1;
        end
      end
    end
    
  end
  
end

thickness = 0.8*(bins(2)-bins(1));

figure;
for dat = 1:3
  subplot(3,1,dat); hold on; grid on;
  
  for ii = 1:length(bins)

    if trackHT_bins(ii,dat) > forceASR_bins(ii,dat)
      rectangle('Position', [bins(ii) 0 thickness trackHT_bins(ii,dat)], 'FaceColor', [0 0 1]);
      rectangle('Position', [bins(ii) 0 thickness forceASR_bins(ii,dat)], 'FaceColor', [1 0 0]);
    else
      rectangle('Position', [bins(ii) 0 thickness forceASR_bins(ii,dat)], 'FaceColor', [1 0 0]);
      rectangle('Position', [bins(ii) 0 thickness trackHT_bins(ii,dat)], 'FaceColor', [0 0 1]);
    end
  end
  
  axis([0 bins(ii)+(bins(2)-bins(1)) 0 25]);
end