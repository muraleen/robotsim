indexARSR = [8 9 2 7 3 4 5 6];
indexP2D = [11 12 5 10 6 7 8 9];

cs = 0;

figure(45+cs);
clf;

for cs = 0

	trackHT.file = ['trackHT/set_' num2str(cs) '.csv'];
	trackHT.index = indexP2D;
	trackHT.pdata = {trackHT.file, trackHT.index, 0, 'ASR tracking HT', 'r--'};

	trackAT.file = ['trackAT/set_' num2str(cs) '.csv'];
	trackAT.index = indexP2D;
	trackAT.pdata = {trackAT.file, trackAT.index, 0, 'ASR tracking AT', 'k-'};

	forceASR.file = ['forceASR/set_' num2str(cs) '.csv'];
	forceASR.index = indexP2D;
	forceASR.pdata = {forceASR.file, forceASR.index, 0, 'Force-feedback', 'b-'};

	plotData = [trackHT.pdata; forceASR.pdata];

	ref.file = ['AT/set_' num2str(cs) '.csv'];
	ref.index = indexARSR;

	refData = csvread(ref.file, 1, 0);

	qref = refData(:, ref.index);

	for ii = 1:size(plotData,1)

		logFile = plotData{ii,1};
		logIndex = plotData{ii,2};
		label = plotData{ii,4};
		linespec = plotData{ii,5};

		logData = csvread(logFile, 1, 0);

		t = logData(:,1);
		q = logData(:, logIndex);
	
		dq = q - qref;

		figure(45+cs);
	
		subplot(2,2,1); hold on;
		plot(t(20:end).', dq(20:end,5).', linespec); hold on;
		rms(dq(200:end,5))
		grid on;
		%title(['Manipulator Joint Angle Errors - Set ' num2str(cs+1)]);
		xlabel('t (s)');
		ylabel('\delta\theta_1 (rad)');
	
		subplot(2,2,2); hold on;
		plot(t(20:end).', dq(20:end,6).', linespec); hold on;
		rms(dq(200:end,6))
		grid on;
		xlabel('t (s)');
		ylabel('\delta\theta_2 (rad)');
	
		subplot(2,2,3); hold on;
		plot(t(20:end).', dq(20:end,7).', linespec); hold on;
		rms(dq(200:end,7))
		grid on;
		xlabel('t (s)');
		ylabel('\delta\theta_3 (rad)');
	
		subplot(2,2,4); hold on;
		plot(t(20:end).', dq(20:end,8).', linespec); hold on;
		rms(dq(200:end,8))
		grid on;
		xlabel('t (s)');
		ylabel('\delta\theta_4 (rad)');
	
	end

end
