function [w] = unskew(W)
	w = [W(3,2); W(1,3); W(2,1)];
end
