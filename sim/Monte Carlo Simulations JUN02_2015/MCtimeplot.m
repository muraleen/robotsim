% Time plot of Monte Carlo Simulations
cmap = ['r', 'g', 'b', 'k', 'm'];

figure(2); hold on;

joint = 5;

for ii = [0 3124]

	freefloat_log = csvread(["MC_ARSR/set_" num2str(ii) ".csv"]);
	platform_log = csvread(["MC_P2D/set_" num2str(ii) ".csv"]);

	t = freefloat_log(:,1);

	% Compute RMS Error
%	MSerror = zeros(size(freefloat_log),1);
	
%	for jj = 2:3
%		MSerror = MSerror + (freefloat_log(:, jj) - (platform_log(:, jj) + platform_log(:, jj+3))).^2;
%	end

%	for jj = joint
%		MSerror = MSerror + (freefloat_log(:, jj) - platform_log(:, jj+3)).^2;
%	end

%	RMSerror = sqrt(MSerror)/max(abs(freefloat_log(:,joint)));
	
	%/sqrt(max(freefloat_log(:,2))^2 + max(freefloat_log(:,3))^2);
	%/max(abs(freefloat_log(:,4)));

%Vmw_freq = Ufreq[(math.floor(ctlSet / 625) % 5) + 1]
%V1_freq = Ufreq[(math.floor(ctlSet / 125) % 5) + 1]
%V2_freq = Ufreq[(math.floor(ctlSet / 25) % 5) + 1]
%V3_freq = Ufreq[(math.floor(ctlSet / 5) % 5) + 1]
%V4_freq = Ufreq[(ctlSet % 5) + 1]

	f(1) = mod(floor(ii/625),5) + 1;
	f(2) = mod(floor(ii/125),5) + 1;
	f(3) = mod(floor(ii/25),5) + 1;
	f(4) = mod(floor(ii/5),5) + 1;
	f(5) = mod(floor(ii),5) + 1;

	if sum(f) == 25
		plot3(freefloat_log(:,2).', freefloat_log(:,3).', t.', 'b'); hold on;
		plot3(platform_log(:,2).' + platform_log(:,5).', platform_log(:,3).' + platform_log(:,6).', t.', 'r'); hold on;
		view(45,45);
%	elseif sum(f) == 25
%		plot(freefloat_log(:,2).', freefloat_log(:,3).', 'b'); hold on;
%		plot(platform_log(:,2).' + platform_log(:,5).', platform_log(:,3).' + platform_log(:,6).', 'r'); hold on;
	end
	
	% Plot RMS error vs. time
%	if sum(f) == 5
%		plot(t.', RMSerror.', 'b'); hold on;
%	elseif sum(f) == 25
%		plot(t.', RMSerror.', 'r');
%	end

end

grid on;

