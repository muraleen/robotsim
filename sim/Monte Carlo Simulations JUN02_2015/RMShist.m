% Histogram of Monte Carlo Simulation end RMS error
RMSerror = zeros(1, 3125);
RMSmean = 0;

for ii = 0:3124
	% Load Log files
	freefloat_log = csvread(["MC_ARSR/set_" num2str(ii) ".csv"]);
	platform_log = csvread(["MC_P2D/set_" num2str(ii) ".csv"]);
	
	% Compute final error (after simulation)
	ff_n = size(freefloat_log, 1);
	p_n = size(platform_log, 1);
	
	MSerror = 0;
	
	for jj = 2:4
	%for jj = 2:3
		MSerror = MSerror + (freefloat_log(ff_n, jj) - (platform_log(p_n, jj) + platform_log(p_n, jj+3)))^2;
	end
	
	for jj = 5:9
		MSerror = MSerror + (freefloat_log(ff_n, jj) - platform_log(p_n, jj+3))^2;
	end
	
	RMSerror(1, ii+1) = sqrt(MSerror);
	RMSmean = RMSmean + sqrt(MSerror);
end

RMSmean = RMSmean / 3125;

hist(RMSerror, 50);
grid on;
