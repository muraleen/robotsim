#!/bin/sh

dt=0.001	# Simulation delta-time (s)
Ts=0.01	# Controller update interval (s)
Tf=20		# Simulation run time (s)
Tl=0.01	# Logging update interval (s)

for i in `seq 0 54`
do
	echo -n "Running simulation with control set $i ... "
	./robotsim --model=ARSR --dt=$dt --Ts=$Ts --Tl=$Tl --tsim=$Tf --no-gfx --log=log/HT/set_${i}.csv --prop:/controller/MCS_ctlSet=$i
	echo "DONE!"
done
