format short g

P2D_links = dlmread('data/pose_P2D.dat', '\t');
ARSR_links = dlmread('data/pose_ARSR.dat', '\t');

labelY = {'\deltax (m)'; '\deltay (m)'; '\delta\psi (rad)'};

t = P2D_links(:, 1);
linkErrors = angle(exp(j*(P2D_links(:, 2:4) - ARSR_links(:, 2:4))));

figure(3);

for link = 1:3

	subplot(3,1,link); hold on;
	plot(t.', linkErrors(:,link).');
	grid on;
	ylabel(labelY(link));
	xlabel('t (s)');

end
