set term wxt title "Error/Deviation in Pose Tracking with FTsensor Data"

set multiplot layout 3, 1 title "Error/Deviation in Pose Tracking"

set ylabel "dx (m)"
set y2label "Fx (N)"


plot "data/P2D_poseErr.dat" using 1:2 title "Error" with lines axes x1y1, \
	 "data/P2D_FTsensor.dat" using 1:2 title "Force" with lines axes x1y2

set ylabel "dy (m)"
set y2label "Fy (N)"


plot "data/P2D_poseErr.dat" using 1:3 title "Error" with lines axes x1y1, \
	 "data/P2D_FTsensor.dat" using 1:3 title "Force" with lines axes x1y2

set ylabel "dpsi (rad)"
set y2label "Tz (Nm)"


plot "data/P2D_poseErr.dat" using 1:4 title "Error" with lines axes x1y1, \
	 "data/P2D_FTsensor.dat" using 1:4 title "Force" with lines axes x1y2

set style line 81 lt 0 lc rgb "#404040" lw 1

set ytics nomirror
set y2tics
#set grid xtics ytics y2tics mxtics mytics my2tics
set grid back ls 81

set zeroaxis

unset multiplot

pause 0.1
reread
