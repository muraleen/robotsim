set term wxt title "Joint Angle comparison of ARSR and P2D"

set multiplot layout 4, 1 title "Joint Angle comparison of ARSR and P2D"

set ylabel "Link 1 (rad)"

plot "data/links_ARSR.dat" using 1:2 title "Free-floating" with lines, \
	 "data/links_P2D.dat" using 1:2 title "Tracking Ctlr" with lines

set ylabel "Link 2 (rad)"

plot "data/links_ARSR.dat" using 1:3 title "Free-floating" with lines, \
	 "data/links_P2D.dat" using 1:3 title "Tracking Ctlr" with lines

set ylabel "Link 3 (rad)"

plot "data/links_ARSR.dat" using 1:4 title "Free-floating" with lines, \
	 "data/links_P2D.dat" using 1:4 title "Tracking Ctlr" with lines

set ylabel "Link 4 (rad)"

plot "data/links_ARSR.dat" using 1:5 title "Free-floating" with lines, \
	 "data/links_P2D.dat" using 1:5 title "Tracking Ctlr" with lines

set style line 81 lt 0 lc rgb "#404040" lw 1

set grid xtics ytics mxtics mytics
set grid back ls 81

unset multiplot

pause 0.1
reread
