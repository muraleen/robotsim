set title "Incurve Robot Position Plot"
set xlabel "x (m)"
set ylabel "y (m)"

plot "data/incurve_pos.dat" using 2:3 with lines

set style line 81 lt 0 lc rgb "#808080" lw 0.5

set style line 81 lt 0 lc rgb "#404040" lw 1

set grid xtics ytics mxtics mytics
set grid back ls 81

pause 0.1
reread
