set termopt enhanced

set term wxt title "Live Plot: Angular Momentum Vector"

set title "Angular Momentum Vector"
set xlabel "t (s)"
set ylabel "h (N-m-s)"

plot "data/plot_h.dat" using 1:2 title "h_x(t)" with lines, \
	 "data/plot_h.dat" using 1:3 title "h_y(t)" with lines, \
	 "data/plot_h.dat" using 1:4 title "h_z(t)" with lines

set style line 81 lt 0 lc rgb "#808080" lw 0.5

set grid xtics ytics mxtics mytics
set grid back ls 81

pause 0.1
reread
