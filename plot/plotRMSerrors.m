format short g

P2D_pose = dlmread('data/pose_ARSR_3off.dat', '\t');
ARSR_pose = dlmread('data/pose_ARSR.dat', '\t');
P2D_links = dlmread('data/links_ARSR_3off.dat', '\t');
ARSR_links = dlmread('data/links_ARSR.dat', '\t');

t = P2D_pose(:, 1);
poseErrors = P2D_pose(:, 2:4) - ARSR_pose(:, 2:4);
linkErrors = angle(exp(j*(P2D_links(:, 2:5) - ARSR_links(:, 2:5))));

sysErrors = [poseErrors linkErrors];

RMSerrors = [];
RMSerrorSquare = 0;

for ii = 1:size(t,1)
	for jj = 1:7
		RMSerrorSquare = RMSerrorSquare + (sysErrors(ii,jj))^2;
	end
	RMSerrors = [RMSerrors sqrt(RMSerrorSquare)];
end

figure(1);

plot(t.', RMSerrors.');
grid on;

RMSerrors(end)
