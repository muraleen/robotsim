format short g

P2D_links = dlmread('data/links_ARSR_5off.dat', '\t');
ARSR_links = dlmread('data/links_ARSR.dat', '\t');

t = P2D_links(:, 1);
linkErrors = angle(exp(j*(P2D_links(:, 2:5) - ARSR_links(:, 2:5))));

figure(2);

for link = 1:4

	subplot(4,1,link); hold on;
	plot(t.', linkErrors(:,link).');
	grid on;
	ylabel(['Link ' link ' Error (rad)']);
	xlabel('t (s)');

end
