set term wxt title "Force/Torque Sensor Outputs"

set multiplot layout 6, 1 title "Force/Torque Sensor Outputs"

set ylabel "Fx (N)"

plot "data/ft_p3d.dat" using 1:2 title "Force in x-direction" with lines

set ylabel "Fy (N)"

plot "data/ft_p3d.dat" using 1:3 title "Force in y-direction" with lines

set ylabel "Fz (N)"

plot "data/ft_p3d.dat" using 1:4 title "Force in z-direction" with lines

set ylabel "tau_x (Nm)"

plot "data/ft_p3d.dat" using 1:5 title "Torque in x-direction" with lines

set ylabel "tau_y (Nm)"

plot "data/ft_p3d.dat" using 1:6 title "Torque in y-direction" with lines

set ylabel "tau_z (Nm)"

plot "data/ft_p3d.dat" using 1:7 title "Torque in z-direction" with lines

set style line 81 lt 0 lc rgb "#404040" lw 1

set grid xtics ytics mxtics mytics
set grid back ls 81

unset multiplot

pause 0.2
reread
