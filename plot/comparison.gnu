set term wxt title "Pose comparison plots"

set multiplot layout 3, 1 title "Pose comparison plots"

set ylabel "x (m)"

plot "data/pose_ARSR.dat" using 1:2 title "Correct Dynamics" with lines, \
	 "data/pose_ARSR_10.dat" using 1:2 title "25% Deviation" with lines, \
	 "data/pose_P2D.dat" using 1:2 title "Platform Controlled" with lines

set ylabel "y (m)"

plot "data/pose_ARSR.dat" using 1:3 title "Correct Dynamics" with lines, \
	 "data/pose_ARSR_10.dat" using 1:3 title "25% Deviation" with lines, \
	 "data/pose_P2D.dat" using 1:3 title "Platform Controlled" with lines

set ylabel "psi (rad)"

plot "data/pose_ARSR.dat" using 1:4 title "Correct Dynamics" with lines, \
	 "data/pose_ARSR_10.dat" using 1:4 title "25% Deviation" with lines, \
	 "data/pose_P2D.dat" using 1:4 title "Platform Controlled" with lines

set style line 81 lt 0 lc rgb "#404040" lw 1

set grid xtics ytics mxtics mytics
set grid back ls 81

unset multiplot

pause 0.1
reread
