set term wxt title "Pose comparison of ARSR and tracking controller"

set multiplot layout 3, 1 title "Pose comparison of ARSR and tracking controller"

set ylabel "x (m)"

plot "data/pose_ARSR.dat" using 1:2 title "Free-floating" with lines, \
	 "data/pose_P2D.dat" using 1:2 title "Tracking Ctlr" with lines, \
	 "data/pose_P2D.dat" using 1:5 title "Desired" with lines

set ylabel "y (m)"

plot "data/pose_ARSR.dat" using 1:3 title "Free-floating" with lines, \
	 "data/pose_P2D.dat" using 1:3 title "Tracking Ctlr" with lines, \
	 "data/pose_P2D.dat" using 1:6 title "Desired" with lines

set ylabel "psi (rad)"

plot "data/pose_ARSR.dat" using 1:4 title "Free-floating" with lines, \
	 "data/pose_P2D.dat" using 1:4 title "Tracking Ctlr" with lines, \
	 "data/pose_P2D.dat" using 1:7 title "Desired" with lines

set style line 81 lt 0 lc rgb "#404040" lw 1

set grid xtics ytics mxtics mytics
set grid back ls 81

unset multiplot

pause 0.1
reread
