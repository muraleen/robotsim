set term wxt title "Force/Torque Sensor Outputs"

set multiplot layout 3, 1 title "Force/Torque Sensor Outputs"

set ylabel "Fx (N)"

plot "data/P2D_FTsensor.dat" using 1:2 title "Force in x-direction" with lines

set ylabel "Fy (N)"

plot "data/P2D_FTsensor.dat" using 1:3 title "Force in y-direction" with lines

set ylabel "tau_z (Nm)"

plot "data/P2D_FTsensor.dat" using 1:4 title "Torque in z-direction" with lines

set style line 81 lt 0 lc rgb "#404040" lw 1

set grid xtics ytics mxtics mytics
set grid back ls 81

unset multiplot

pause 0.1
reread
