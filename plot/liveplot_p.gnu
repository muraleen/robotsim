set termopt enhanced

set term wxt title "Live Plot: Linear Momentum Vector"

set title "Linear Momentum Vector"
set xlabel "t (s)"
set ylabel "p (N-s)"

plot "data/plot_p.dat" using 1:2 title "p_x(t)" with lines, \
	 "data/plot_p.dat" using 1:3 title "p_y(t)" with lines, \
	 "data/plot_p.dat" using 1:4 title "p_z(t)" with lines

set style line 81 lt 0 lc rgb "#808080" lw 0.5

set grid xtics ytics mxtics mytics
set grid back ls 81

pause 0.1
reread
