# README #

This program is a robotic simulation system for various kinds (manipulators, mobile robots, space robots etc.) of robotic platforms to test dynamics, perform simulations and analysis and to aid in the design of good control systems. The original intention for the project was to develop an 'Autonomous Redundant Space Robot' simulation to understand the viability of the use of Force/Torque nullifying controllers on a multi-axis (2/3) CNC platform to simulate free-floating environments on Earth. The project also aims at developing an optimal controller for the platform.

* Authors: Narendran Muraleedharan, Dr. Iacopo Gentilini, Dr. Douglas Isenberg
* Version: 0.1.0
* [ERAU Prescott Space Robotics Laboratory](http://robotics.pr.erau.edu/research.html)

### Installation ###

The following dependencies are required to be installed (along with *build-essential* tools) to compile the simulator.

* armadillo-5.100.2
* lua-5.3.0
* openscenegraph-3.2.2-rc2
* symbolicc++3-3.35

Source codes of the above dependencies (may not be the latest version!) are available in the **lib/** directory of the project.

```
#!sh

./configure
```
Will build the above dependencies from source, please run as superuser.


```
#!sh

make
```
Will build the program, use the -jX flag to build with X number of threads (faster).

### Usage ###

To start the simulation, run the following command:


```
#!sh

./robotsim --model=<model> [other options]
```

Available command line options are:

* --mode=<mode>	        Simulation mode [rt: real-time (default), pr: pre-run]
* --ctlr=<ctlr>	        System Controller Language [lua (default), cxx]
* --dt=<dt>	        Simulation delta-time [DEFAULT: 0.001 sec]
* --freq=<freq>	        Simulation frequency [DEFAULT: 1000 Hz]
* --Ts=<Ts>	        Controller sampling interval [DEFAULT: 0.01 sec]
* --Fs=<Fs>	        Controller sampling frequency [DEFAULT: 100 Hz]
* --Tl=<Ts>	        Log interval [DEFAULT: 0.1 sec]
* --Fl=<Fs>	        Log frequency [DEFAULT: 10 Hz]
* --tsim=<tsim>	        Simulation run time [DEFAULT: 10 sec]
* --log=<logfile>	Log File Path [DEFAULT: log.csv]
* --prop:<path>=<value>	Set Property Value on initialization
* --no-gfx		Run with graphics disabled

### Help and Contact ###

I have not yet made documentation on creating robot models at the moment, but if you need to use this for a project, please feel free to contact me and I can assist you through the development of your model and dynamics to be run in the simulator.

* Narendran Muraleedharan <[muraleen@my.erau.edu](mailto:muraleen@my.erau.edu)>
* [ERAU Space Robotics Laboratory Contact](http://robotics.pr.erau.edu/contact.html)