#ifndef CONTROLLER_HXX
#define CONTROLLER_HXX

#include <armadillo>
#include <iostream>
#include <cmath>
#include <string>
#include <vector>
#include <map>
#include <fstream>

#include "../interpreter/lua.hxx"
#include "../parser/parser.hxx"
#include "../propertytree/propertytree.hxx"
#include "../parser/parser.hxx"
#include "../math/math.hxx"

using namespace std;
using namespace arma;

map<string, double> controller_cxx(double Ts, vec X, map<string, double> U, map<string, double> params, string model);
map<string, double> controller_lua(LuaScript *ctlr_script, double Ts, map<string, double> gamma, map<string, double> inputs);
void controller_lua_init(LuaScript *ctlr_script, string file);

class CtlHelper
{
	public:
		static double saturate(double val, double min, double max);
		static double wrap(double val, double min, double max);
		static vector<map<string, double>> applySatWrap(map<string, double> gamma, map<string, double> inputs, VarsList vList);
};

class PIDController
{
	public:
		PIDController(double _Kp, double _Ki, double _Kd);
		void setIntegralLimit(double _ilim);
		double run(double dt, double error);
	private:
		double integral;
		double prev_error;
		double Kp;
		double Ki;
		double Kd;
		double ilim;
};

#endif
