#include "controller.hxx"

map<string, double> controller_lua(LuaScript *ctlr_script, double Ts, map<string, double> gamma, map<string, double> inputs)
{
	map<string, double> _inputs;

	// Pre-load generalized co-ordinates (X) and input variables (U) into stack
	ctlr_script->set("dt", Ts);
	
	for (auto it : gamma) {
		ctlr_script->set("X_" + it.first, gamma[it.first]);
	}
	for (auto it : inputs) {
		ctlr_script->set("U_" + it.first, inputs[it.first]);
	}
	
	ctlr_script->run();
	
	for (auto it : inputs) {
		_inputs[it.first] = ctlr_script->get("U_" + it.first);
	}
	
	return _inputs;
}

void controller_lua_init(LuaScript *ctlr_script, string file)
{
	ctlr_script->load(file);
}

PIDController::PIDController(double _Kp, double _Ki, double _Kd)
{
	this->Kp = _Kp;
	this->Ki = _Ki;
	this->Kd = _Kd;
	this->integral = 0;
	this->prev_error = 0;
	this->ilim = 99999;
}

void PIDController::setIntegralLimit(double _ilim)
{
	this->ilim = _ilim;
}

double PIDController::run(double dt, double error)
{
	this->integral += error*dt;
	this->integral = CtlHelper::saturate(this->integral, -ilim, ilim);
	
	double output;
	
	output = this->Kp*error;
	output += this->Ki*this->integral;
	output += this->Kd*((error - this->prev_error)/dt);
	
	this->prev_error = error;
	
	return output;
}

double CtlHelper::saturate(double val, double min, double max)
{
	if (val > max) {
		return max;
	} else if (val < min) {
		return min;
	} else {
		return val;
	}
}

double CtlHelper::wrap(double val, double min, double max)
{
	while (val > max) {
		val += (min - max);
	}
	while (val < min) {
		val += (max - min);
	}
	
	return val;
}

vector<map<string, double>> CtlHelper::applySatWrap(map<string, double> gamma, map<string, double> inputs, VarsList vList)
{
	/* for (auto it : vList.wrap) {
		if (gamma.find(it.first) != gamma.end()) {
			if (gamma[it.first] > vList.wrap[it.first].at(1)) {
				gamma[it.first] += vList.wrap[it.first].at(0) - vList.wrap[it.first].at(1);
			} else if (gamma[it.first] < vList.wrap[it.first].at(0)) {
				gamma[it.first] += vList.wrap[it.first].at(1) - vList.wrap[it.first].at(0);
			}
			// gamma[it.first] = atan2(sin(gamma[it.first]), cos(gamma[it.first]));
		}
		if (inputs.find(it.first) != inputs.end()) {
			if (inputs[it.first] > vList.wrap[it.first].at(1)) {
				inputs[it.first] += vList.wrap[it.first].at(1) - vList.wrap[it.first].at(0);
			} else if (inputs[it.first] < vList.wrap[it.first].at(0)) {
				inputs[it.first] += vList.wrap[it.first].at(0) - vList.wrap[it.first].at(1);
			}
			// inputs[it.first] = atan2(sin(inputs[it.first]), cos(inputs[it.first]));
		}
	}
	for (auto it : vList.sat) {
		if (gamma.find(it.first) != gamma.end()) {
			if (gamma[it.first] > vList.sat[it.first].at(1)) {
				gamma[it.first] = vList.sat[it.first].at(1);
			} else if (gamma[it.first] < vList.sat[it.first].at(0)) {
				gamma[it.first] = vList.sat[it.first].at(0);
			}
		}
		if (inputs.find(it.first) != inputs.end()) {
			if (inputs[it.first] > vList.sat[it.first].at(1)) {
				inputs[it.first] = vList.sat[it.first].at(1);
			} else if (inputs[it.first] < vList.sat[it.first].at(0)) {
				inputs[it.first] = vList.sat[it.first].at(0);
			}
		}
	} */
	
	vector<map<string, double>> retVect;
	retVect.push_back(gamma);
	retVect.push_back(inputs);
	
	return retVect;
}
