#ifndef PROPERTYTREE_HXX
#define PROPERTYTREE_HXX

#include <string>
#include <vector>
#include <map>
#include "../parser/parser.hxx"
#include <iomanip>

using namespace std;

struct pNode
{
	string name;
	string value;
	string type; // int, double, string, dir
	map<string, pNode> nodes;
};

class PropertyTree
{
	public:
		PropertyTree();
		void set(string _prop, int val);
		void set(string _prop, double val);
		void set(string _prop, string val);
		int getInt(string _prop);
		double getDouble(string _prop);
		string getString(string _prop);
		string getType(string _prop);
		static PropertyTree *props();
	private:
		pNode tree;
		static PropertyTree *_props;
};

#endif
