#include "propertytree.hxx"

PropertyTree *PropertyTree::_props = NULL;

PropertyTree *PropertyTree::props()
{
	if (!_props)
		_props = new PropertyTree();
		
	return _props;
}

PropertyTree::PropertyTree()
{
	tree.name = "root";
	tree.type = "dir";
}

void PropertyTree::set(string _prop, int val)
{
	pNode *currNode = &tree;
	
	if (_prop.at(0) == '/') {
		_prop.erase(0,1);
	}
	vector<string> path = Parser::splitStr(_prop, "/");
	for (int i=0; i<path.size()-1; i++) {
		if (currNode->nodes.find(path.at(i)) != currNode->nodes.end()) {
			currNode = &currNode->nodes[path.at(i)];
		} else {
			pNode newNode;
			newNode.name = path.at(i);
			newNode.type = "dir";
			currNode->nodes[path.at(i)] = newNode;
			currNode = &currNode->nodes[path.at(i)];
		}
	}
	
	pNode prop;
	prop.name = path.at(path.size()-1);
	prop.value = to_string(val);
	prop.type = "int";
	
	currNode->nodes[prop.name] = prop;
}

void PropertyTree::set(string _prop, double val)
{
	pNode *currNode = &tree;
	
	if (_prop.at(0) == '/') {
		_prop.erase(0,1);
	}
	vector<string> path = Parser::splitStr(_prop, "/");
	for (int i=0; i<path.size()-1; i++) {
		if (currNode->nodes.find(path.at(i)) != currNode->nodes.end()) {
			currNode = &currNode->nodes[path.at(i)];
		} else {
			pNode newNode;
			newNode.name = path.at(i);
			newNode.type = "dir";
			currNode->nodes[path.at(i)] = newNode;
			currNode = &currNode->nodes[path.at(i)];
		}
	}
	
	pNode prop;
	prop.name = path.at(path.size()-1);
	
	stringstream ss;
	ss << setprecision(12) << val;
	
	prop.value = ss.str();
	
	prop.type = "double";
	
	currNode->nodes[prop.name] = prop;
}

void PropertyTree::set(string _prop, string val)
{
	pNode *currNode = &tree;
	
	if (_prop.at(0) == '/') {
		_prop.erase(0,1);
	}
	vector<string> path = Parser::splitStr(_prop, "/");
	for (int i=0; i<path.size()-1; i++) {
		if (currNode->nodes.find(path.at(i)) != currNode->nodes.end()) {
			currNode = &currNode->nodes[path.at(i)];
		} else {
			pNode newNode;
			newNode.name = path.at(i);
			newNode.type = "dir";
			currNode->nodes[path.at(i)] = newNode;
			currNode = &currNode->nodes[path.at(i)];
		}
	}
	
	pNode prop;
	prop.name = path.at(path.size()-1);
	prop.value = val;
	
	prop.type = "string";
	
	currNode->nodes[prop.name] = prop;
}

int PropertyTree::getInt(string _prop)
{
	pNode *currNode = &tree;
	
	if (_prop.at(0) == '/') {
		_prop.erase(0,1);
	}
	vector<string> path = Parser::splitStr(_prop, "/");
	for (int i=0; i<path.size()-1; i++) {
		if (currNode->nodes.find(path.at(i)) != currNode->nodes.end()) {
			currNode = &currNode->nodes[path.at(i)];
		} else {
			return 0;
		}
	}
	if (currNode->nodes.find(path.at(path.size()-1)) != currNode->nodes.end()) {
		return stoi(currNode->nodes[path.at(path.size()-1)].value);
	} else {
		return 0;
	}
}

double PropertyTree::getDouble(string _prop)
{
	pNode *currNode = &tree;
	
	if (_prop.at(0) == '/') {
		_prop.erase(0,1);
	}
	vector<string> path = Parser::splitStr(_prop, "/");
	
	for (int i=0; i<path.size()-1; i++) {
		if (currNode->nodes.find(path.at(i)) != currNode->nodes.end()) {
			currNode = &currNode->nodes[path.at(i)];
		} else {
			return 0;
		}
	}
	if (currNode->nodes.find(path.at(path.size()-1)) != currNode->nodes.end()) {
		return stod(currNode->nodes[path.at(path.size()-1)].value);
	} else {
		return 0;
	}
}

string PropertyTree::getString(string _prop)
{
	pNode *currNode = &tree;
	
	if (_prop.at(0) == '/') {
		_prop.erase(0,1);
	}
	vector<string> path = Parser::splitStr(_prop, "/");
	for (int i=0; i<path.size()-1; i++) {
		if (currNode->nodes.find(path.at(i)) != currNode->nodes.end()) {
			currNode = &currNode->nodes[path.at(i)];
		} else {
			return "";
		}
	}
	if (currNode->nodes.find(path.at(path.size()-1)) != currNode->nodes.end()) {
		return currNode->nodes[path.at(path.size()-1)].value;
	} else {
		return "";
	}
}

string PropertyTree::getType(string _prop)
{
	pNode *currNode = &tree;
	
	if (_prop.at(0) == '/') {
		_prop.erase(0,1);
	}
	vector<string> path = Parser::splitStr(_prop, "/");
	for (int i=0; i<path.size()-1; i++) {
		if (currNode->nodes.find(path.at(i)) != currNode->nodes.end()) {
			currNode = &currNode->nodes[path.at(i)];
		} else {
			return "undefined";
		}
	}
	if (currNode->nodes.find(path.at(path.size()-1)) != currNode->nodes.end()) {
		return currNode->nodes[path.at(path.size()-1)].type;
	} else {
		return "undefined";
	}
}
