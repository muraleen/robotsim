#include "includes.hxx"

/*
auto time_prev = chrono::high_resolution_clock::now();

double delta_time()
{
	auto time_now = chrono::high_resolution_clock::now();
	double t = (chrono::duration_cast<chrono::microseconds>(time_now - time_prev).count())/1000000.0;
	time_prev = time_now;
	return t;
} */

int main(int argc, char** argv)
{	
	string model;
	map<string, string> path;
	
	PropertyTree *props = PropertyTree::props();
	LuaScript *ctlr_script;
	vector<LuaScript*> scripts;
	
	props->set("/sim/time/script-interval", 0.05);

	double dt = 0.001; // Simulation delta-time
	double Ts = 0.01; // Controller update interval
	double Tl = 0.1; // Log interval
	double tSim = 10; // Simulation run time
	double sim_rate = 1.0;
	bool sim_rt = true;
	bool ctl_lua = true;
	bool noGfx = false;
	string logFile = "log.csv";

	vec _cm(3); _cm.zeros();

	// Handle input arguments
	if (argc < 2) {
		cerr << "Usage: " << argv[0] << " --model=<model> [options]" << endl
			 << "OPTIONS:" << endl
			 << "\t--mode=<mode>\tSimulation mode [rt: real-time (default), pr: pre-run]" << endl
			 << "\t--ctlr=<ctlr>\tSystem Controller Language [lua (default), cxx]" << endl
			 << "\t--dt=<dt>\tSimulation delta-time [DEFAULT: 0.001 sec]" << endl
			 << "\t--freq=<freq>\tSimulation frequency [DEFAULT: 1000 Hz]" << endl
			 << "\t--Ts=<Ts>\tController sampling interval [DEFAULT: 0.01 sec]" << endl
			 << "\t--Fs=<Fs>\tController sampling frequency [DEFAULT: 100 Hz]" << endl
			 << "\t--Tl=<Ts>\tLog interval [DEFAULT: 0.1 sec]" << endl
			 << "\t--Fl=<Fs>\tLog frequency [DEFAULT: 10 Hz]" << endl
			 << "\t--tsim=<tsim>\tSimulation run time [DEFAULT: 10 sec]" << endl
			 << "\t--rate=<rate>\tSimulation rate [DEFAULT: 1 X]" << endl
			 << "\t--log=<logfile>\tLog File Path [DEFAULT: log.csv]" << endl
			 << "\t--prop:<path>=<value>\tSet Property Value on initialization" << endl
			 << "\t--no-gfx\t\tRun with graphics disabled" << endl;
		exit(0);
	}
	
	for (int i=1; i<argc; i++) {
		string argStr(argv[i]);
		vector<string> argset = Parser::splitStr(argStr, "=");
		if (argset.at(0).compare("--model") == 0) {
			model = argset.at(1);
			path = Parser::parseModelFile(model);
		} else if (argset.at(0).compare("--dt") == 0) {
			dt = stod(argset.at(1));
		} else if (argset.at(0).compare("--freq") == 0) {
			dt = 1/stod(argset.at(1));
		} else if (argset.at(0).compare("--Ts") == 0) {
			Ts = stod(argset.at(1));
			props->set("/sim/time/script-interval", Ts);
		} else if (argset.at(0).compare("--Fs") == 0) {
			Ts = 1/stod(argset.at(1));
			props->set("/sim/time/script-interval", Ts);
		} else if (argset.at(0).compare("--Tl") == 0) {
			Tl = stod(argset.at(1));
		} else if (argset.at(0).compare("--Fl") == 0) {
			Tl = 1/stod(argset.at(1));
		} else if (argset.at(0).compare("--tsim") == 0) {
			tSim = stod(argset.at(1));
		} else if (argset.at(0).compare("--rate") == 0) {
			sim_rate = stod(argset.at(1));
		} else if (argset.at(0).compare("--mode") == 0) {
			if (argset.at(1).compare("pr") == 0) {
				sim_rt = false;
			}
		} else if (argset.at(0).compare("--ctlr") == 0) {
			if (argset.at(1).compare("cxx") == 0) {
				ctl_lua = false;
			}
		} else if (argset.at(0).compare("--log") == 0) {
			logFile = argset.at(1);
		} else if (argset.at(0).compare("--no-gfx") == 0) {
			noGfx = true;
			sim_rt = false;
		} else {
			vector<string> argSubSet = Parser::splitStr(argset.at(0), ":");
			if (argSubSet.at(0).compare("--prop") == 0) {
				if (Parser::isNumber(argset.at(1))) {
					props->set(argSubSet.at(1), stod(argset.at(1)));
				} else {
					props->set(argSubSet.at(1), argset.at(1));
				}
			}
		}
	}
	
	props->set("/sim/time/dynamics/dt", dt);
	props->set("/sim/time/dynamics/freq", 1/dt);
	props->set("/sim/time/controller/dt", Ts);
	props->set("/sim/time/controller/freq", 1/Ts);
	props->set("/sim/time/log/dt", Tl);
	props->set("/sim/time/log/freq", 1/Tl);
	props->set("/sim/time/sim-time", tSim);
	if (sim_rt) {
		props->set("/options/sim-mode", "real-time");
	} else {
		props->set("/options/sim-mode", "pre-run");
	}
	if (ctl_lua) {
		props->set("/options/controller", "lua");
	} else {
		props->set("/options/controller", "c++");
	}
	props->set("/options/log-file", logFile);
	
	VarsList vList = Parser::parseVarsFile(path["VARS_PATH"]);
	map<string, double> gamma = vList.varsMap.at(0);
	map<string, double> inputs = vList.varsMap.at(1);
	map<string, double> others = vList.varsMap.at(2);
	map<string, double> gammad = vList.varsMap.at(3);
	map<string, vector<double>> wrap_vars = vList.wrap;
	map<string, vector<double>> sat_vars = vList.sat;
	map<string, double> params = Parser::parseParamsFile(path["PARAMS_PATH"]);
	map<string, itable> tables = Parser::parseParamsFileTables(path["PARAMS_PATH"]);
	
	vector<string> logData = Parser::parseLogCfg(gamma, inputs);
	
	/*
	for (auto it : params) {
		cout << "double " << it.first << " = params[\"" << it.first << "\"];" << endl;
	}
	
	exit(0);
	*/
	
	Graphics *gfx = NULL;
	
	if (!noGfx) {
		gfx = new Graphics();
		Gfx::setGfxInstance(gfx);
	}
	
	bool FKDeriv_cxx = false;
	if (path["CFG:FK_DERIV"].compare("cxx") == 0) {
		FKDeriv_cxx = true;
	}
	
	ForwardKinematics *fk = new ForwardKinematics(Parser::parseFKFile(path["FK_PATH"]), params, gamma, dt, FKDeriv_cxx, model);
	
	for(auto it: params) {
		if(props->getDouble("/dynamics/params/" + it.first) == 0) { // Let cmd-line arg override params file value
			props->set("/dynamics/params/" + it.first, params[it.first]);
		}
	}
	
	if (ctl_lua) {
		ctlr_script = new LuaScript(path["CTLR_PATH"]);
		if (path["CTLR_INIT"].length() > 0) {
			controller_lua_init(ctlr_script, path["CTLR_INIT"]);
		}
	}
	
	// Load lua scripts
	vector<string> scriptPaths = Parser::splitStr(path["SCRIPTS"], ",");
	for (int i = 0; i < scriptPaths.size(); i++) {
		scripts.push_back(new LuaScript("model/" + model + "/" + scriptPaths.at(i)));
		scripts.at(i)->run();
	}
	
	// Run simulation
	double T_last = -Ts;
	double T_log = -Tl;
	int frame = 0;
	int cIndex = 0;
	
	double T_script = -props->getDouble("/sim/time/script-interval");
	
	vec X(vList.vars.at(0).size()*2 + vList.vars.at(2).size());
	
	X.zeros();
	for (int i = 0; i < vList.vars.at(0).size(); i++) {
		X(i) = gamma[vList.vars.at(0).at(i)];
	}
	
	for (int i = 0; i < vList.vars.at(3).size(); i++) {
		X(vList.vars.at(0).size() + i) = gammad[vList.vars.at(3).at(i)];
	}
	
	for (int i = 0; i < vList.vars.at(2).size(); i++) {
		X(vList.vars.at(0).size()*2 + i) = others[vList.vars.at(2).at(i)];
	}
	
	vector<vec> X_sim;
	X_sim.push_back(X);
	
	vector<map<string, double>> U_sim;
	U_sim.push_back(inputs);
	
	ofstream log_file;
	log_file.open(logFile);
	
	vec cm(3), p(3), h(3);
	vector<vec> momentum;
	
	mat R(3,3);
	
	// Log file header
	log_file << "t";
	/* for (int i = 0; i < vList.vars.at(0).size(); i++) {
		log_file << ", " << vList.vars.at(0).at(i);
	}
	for (int i = 0; i < vList.vars.at(1).size(); i++) {
		log_file << ", " << vList.vars.at(1).at(i);
	} */
	for (int i=0; i<logData.size(); i++) {
		log_file << ", " << logData.at(i);
	}
	log_file << endl;
	
	if (!sim_rt) { // Pre-run simulation mode
		for (double t = 0; t <= tSim; t += dt) {
			if (t - T_last >= Ts) {
				// Run Controller
				
				if (ctl_lua) {
					inputs = controller_lua(ctlr_script, Ts, gamma, inputs);
				} else {
					inputs = controller_cxx(Ts, X, inputs, params, model);
				}
				
				for (int i = 0; i < vList.vars.at(0).size(); i++) {
					X(i) = gamma[vList.vars.at(0).at(i)];
				}
				
				U_sim.push_back(inputs);
				T_last = t;
				cIndex++;				
			}
			
			if (t - T_script >= props->getDouble("/sim/time/script-interval")) {
				if (path["CFG:CALC_MMTM"].compare("enable") == 0) {
					cm = fk->getCenterOfMass(gamma);
					props->set("/dynamics/center-of-mass/x", cm(0));
					props->set("/dynamics/center-of-mass/y", cm(1));
					props->set("/dynamics/center-of-mass/z", cm(2));
	
					momentum = fk->calcMomentum(X, gamma);
					p = momentum.at(0);
					h = momentum.at(1);
	
					props->set("/dynamics/linear-momentum/x", p(0));
					props->set("/dynamics/linear-momentum/y", p(1));
					props->set("/dynamics/linear-momentum/z", p(2));
					props->set("/dynamics/angular-momentum/x", h(0));
					props->set("/dynamics/angular-momentum/y", h(1));
					props->set("/dynamics/angular-momentum/z", h(2));
				}
				
				for (int i = 0; i < scripts.size(); i++) {
					scripts.at(i)->run();
				}
				T_script = t;
			}
			
			if (t - T_log >= Tl) {
				log_file << t;
				/* for (int i = 0; i < vList.vars.at(0).size(); i++) {
					log_file << ", " << X(i);
				}
				for (int i = 0; i < vList.vars.at(1).size(); i++) {
					log_file << ", " << inputs[vList.vars.at(1).at(i)];
				} */
				for (int i=0; i<logData.size(); i++) {
					log_file << ", " << setprecision(12) << props->getDouble(logData.at(i));
				}
				log_file << endl;
				
				T_log = t;
			}
			
			vector<map<string, double>> filteredVect = CtlHelper::applySatWrap(gamma, inputs, vList);
			gamma = filteredVect.at(0);
			inputs = filteredVect.at(1);
			
			for (int i = 0; i < vList.vars.at(0).size(); i++) {
				X(i) = gamma[vList.vars.at(0).at(i)];
				props->set("/dynamics/gamma/" + vList.vars.at(0).at(i), X(i));
				props->set("/dynamics/gamma-dot/" + vList.vars.at(0).at(i), X(i + vList.vars.at(0).size()));
			}
			for (int i = 0; i < vList.vars.at(1).size(); i++) {
				props->set("/dynamics/inputs/" + vList.vars.at(1).at(i), inputs[vList.vars.at(1).at(i)]);
			}
			
			// Integrate dynamics
			X = RK4::integrate(dt, X, inputs, params, tables, model, path);
			for (int i = 0; i < vList.vars.at(0).size(); i++) {
				gamma[vList.vars.at(0).at(i)] = X(i);
			}
		
			props->set("/sim/time/elapsed", t);
			
			X_sim.push_back(X);
			frame++;
		}
		log_file.close();
	}
	
	if (noGfx) {
		exit(0);
	}
    
    gfx->setAnimations(gfx->loadModel(path["MODEL_PATH"]), Parser::parseAnimationsFile(path["ANIM_PATH"]));
	
	T_last = -Ts;
	frame = 0; cIndex = 0;
	vec X_vect;
	
	auto start = chrono::high_resolution_clock::now();
	auto curr = chrono::high_resolution_clock::now();
	auto dt_chrono = curr - start;
	long long dt_us;
	double dt_real;
	
	double t = 0, t1 = 0;
	
	gfx->visualize();
	
	auto begin = chrono::high_resolution_clock::now();
	
	while ((t < tSim) || sim_rt) {
		
		// cout << "[336] dt = " << delta_time() << "s" << endl;
		
		// Calculate graphics delta time
		curr = chrono::high_resolution_clock::now();
		dt_chrono = curr - start;
		dt_us = chrono::duration_cast<chrono::microseconds>(dt_chrono).count();
		dt_real = sim_rate*((double) dt_us)/1000000;
		
		// cout << (int) (1/dt_real) << " fps" << endl;
		
		props->set("/time/delta-sec", dt_real);
		start = curr;
		
		t1 += dt_real;
		
		if (t1 > 0) {
		
		if (sim_rt) {
			for (double _t = t; _t <= t+dt_real; _t += dt) {
			
				/* Compute psi (only for incurve)
				R = rotx(X(2))*roty(X(3))*rotz(X(4));
				props->set("/sensors/gps/psi", atan2(R(1,0),R(0,0))); */
				
				// cout << "[360] dt = " << delta_time() << "s" << endl;
				
				if (_t - T_last >= Ts) {
					// Run Controller
					if (ctl_lua) {
						inputs = controller_lua(ctlr_script, Ts, gamma, inputs);
					} else {
						inputs = controller_cxx(Ts, X, inputs, params, model);
					}
			
					U_sim.push_back(inputs);
					T_last = _t;			
				}
				
				// cout << "[374] dt = " << delta_time() << "s" << endl;
				
				if (_t - T_script >= props->getDouble("/sim/time/script-interval")) {
					if (path["CFG:CALC_MMTM"].compare("enable") == 0) {
						
						cm = fk->getCenterOfMass(gamma);
						props->set("/dynamics/center-of-mass/x", cm(0));
						props->set("/dynamics/center-of-mass/y", cm(1));
						props->set("/dynamics/center-of-mass/z", cm(2));
	
						momentum = fk->calcMomentum(X, gamma);
						
						p = momentum.at(0);
						h = momentum.at(1);
						
						props->set("/dynamics/linear-momentum/x", p(0));
						props->set("/dynamics/linear-momentum/y", p(1));
						props->set("/dynamics/linear-momentum/z", p(2));
						props->set("/dynamics/angular-momentum/x", h(0));
						props->set("/dynamics/angular-momentum/y", h(1));
						props->set("/dynamics/angular-momentum/z", h(2));
					}
					
					// cout << "[397] dt = " << delta_time() << "s" << endl;
					
					for (int i = 0; i < scripts.size(); i++) {
						scripts.at(i)->run();
					}
					
					T_script = _t;
					
					// cout << "[405] dt = " << delta_time() << "s" << endl;
				}
				
				// cout << "[408] dt = " << delta_time() << "s" << endl;
				
				if (t - T_log >= Tl) {
					log_file << t;
					/* for (int i = 0; i < vList.vars.at(0).size(); i++) {
						log_file << ", " << X(i);
					}
					for (int i = 0; i < vList.vars.at(1).size(); i++) {
						log_file << ", " << inputs[vList.vars.at(1).at(i)];
					} */
					for (int i=0; i<logData.size(); i++) {
						log_file << ", " << setprecision(12) << props->getDouble(logData.at(i));
					}
					log_file << endl;
				
					T_log = t;
				}
				
				// cout << "[425] dt = " << delta_time() << "s" << endl;
				
				vector<map<string, double>> filteredVect = CtlHelper::applySatWrap(gamma, inputs, vList);
					
				gamma = filteredVect.at(0);
				inputs = filteredVect.at(1);
				
				for (int i = 0; i < vList.vars.at(0).size(); i++) {
					X(i) = gamma[vList.vars.at(0).at(i)];
					props->set("/dynamics/gamma/" + vList.vars.at(0).at(i), X(i));
					props->set("/dynamics/gamma-dot/" + vList.vars.at(0).at(i), X(i + vList.vars.at(0).size()));
				}
				for (int i = 0; i < vList.vars.at(1).size(); i++) {
					props->set("/dynamics/inputs/" + vList.vars.at(1).at(i), inputs[vList.vars.at(1).at(i)]);
				}
				for (int i = 0; i < vList.vars.at(2).size(); i++) {
					props->set("/dynamics/others/" + vList.vars.at(2).at(i), others[vList.vars.at(2).at(i)]);
				}
				
				// cout << "[445] dt = " << delta_time() << "s" << endl;
				
				// Integrate dynamics
				X = RK4::integrate(dt, X, inputs, params, tables, model, path);
			
				// cout << "[450] dt = " << delta_time() << "s" << endl;
			
				props->set("/sim/time/elapsed", _t);
				
				X_sim.push_back(X);
				for (int i = 0; i < vList.vars.at(0).size(); i++) {
					gamma[vList.vars.at(0).at(i)] = X(i);
				}
				
				// cout << "[459] dt = " << delta_time() << "s" << endl;
				
			}
			X_vect = X;
		}
		
		t += dt_real;
		
		if (!sim_rt) {
			frame = (int) (roundto(t, dt)/dt);
			cIndex = (int) (roundto(t, Ts)/Ts);
		
			if (cIndex < U_sim.size()) inputs = U_sim.at(cIndex);
		
			if (frame < X_sim.size()) {
				X_vect = X_sim.at(frame);
			}
		}
		
		for (int i = 0; i < vList.vars.at(0).size(); i++) {
			gamma[vList.vars.at(0).at(i)] = X_vect(i);
		}
		
		}
		
		// cout << "[484] dt = " << delta_time() << "s" << endl;
		
		if (gfx->isRunning()) {
			gfx->update(gamma, inputs);
		} else {
			exit(0);
		}
		
		// cout << "[492] dt = " << delta_time() << "s" << endl << endl;
	}
	
	// Keep graphics up until closed
	while (gfx->isRunning() && !sim_rt) {
		gfx->update(gamma, inputs);
	}
	
	if (sim_rt) log_file.close();

	return 1;
}
