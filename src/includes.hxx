#ifndef INCLUDES_HXX
#define INCLUDES_HXX

#include "controller/controller.hxx"
#include "fk/derivatives.hxx"
#include "fk/fk.hxx"
#include "graphics/graphics.hxx"
#include "integrator/dynamics.hxx"
// #include "inputs/joystick.hxx"
#include "integrator/RK4.hxx"
#include "interpreter/lua.hxx"
#include "math/math.hxx"
#include "parser/parser.hxx"
#include "propertytree/propertytree.hxx"

#include <chrono>
#include <iomanip>

#endif
