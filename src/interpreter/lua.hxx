#ifndef LUA_HXX
#define LUA_HXX

#define LUA_MINSTACK 40

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>

using namespace std;

extern "C" {
	#include "lua.h"
	#include "lualib.h"
	#include "lauxlib.h"
}

class LuaScript
{
	public:
		LuaScript(string _file);
		~LuaScript();
		void run();
		void set(string name, double value);
		double get(string name);
		void load(string file);
	private:
		const char *err; // Not used at the moment
		string fileContents;
		lua_State *L;
		int status;
		
};

#endif
