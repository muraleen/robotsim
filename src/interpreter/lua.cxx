#include "lua.hxx"

#include "interface/interface.cxx"

LuaScript::LuaScript(string _file)
{
	L = luaL_newstate();
	
	// Load lua libraries
	luaL_openlibs(L);
	
	// Register lua_interface functions
	lua_register(L, "pid_init", pid_init);
	lua_register(L, "pid_run", pid_run);
	lua_register(L, "saturate", saturate);
	lua_register(L, "setprop", setprop);
	lua_register(L, "getprop", getprop);
	lua_register(L, "gfx_load3d", gfx_load3d);
	lua_register(L, "gfx_setRot", gfx_setRot);
	lua_register(L, "gfx_setTrans", gfx_setTrans);
	lua_register(L, "gfx_setScale", gfx_setScale);
	lua_register(L, "wrap", wrap);
	lua_register(L, "keypress", keypress);
	lua_register(L, "pid_setilim", pid_setilim);
	
	ifstream luaFile(_file);
	luaFile.seekg(0, ios::end);
	fileContents.reserve(luaFile.tellg());
	luaFile.seekg(0, ios::beg);
	
	lua_pushboolean(L, 1);
	lua_setglobal(L, string("init").c_str());
	
	fileContents.assign((istreambuf_iterator<char>(luaFile)), istreambuf_iterator<char>());
	luaL_checkstack(L, 1000, err);
}

LuaScript::~LuaScript()
{
	lua_close(L);
}

void LuaScript::set(string name, double value)
{
	luaL_checkstack(L, 1000, err);
	lua_pushnumber(L, value);
	lua_setglobal(L, name.c_str());
}

double LuaScript::get(string name)
{
	lua_getglobal(L, name.c_str());
	return lua_tonumber(L, -1);
}

void LuaScript::run()
{
	status = luaL_loadstring(L, fileContents.c_str());
	if (status == 0) {
		status = lua_pcall(L, 0, LUA_MULTRET, 0);
	}
	lua_pushboolean(L, 0);
	lua_setglobal(L, string("init").c_str());
	luaL_checkstack(L, 1000, err);
}

void LuaScript::load(string file)
{
	status = luaL_dofile(L, file.c_str());
}
