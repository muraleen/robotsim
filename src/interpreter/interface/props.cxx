#include "../../propertytree/propertytree.hxx"

static int getprop(lua_State *L)
{
	// Get number of arguments
	int nArgs = lua_gettop(L);
	
	if (nArgs < 1) {
		cout << "[lua] error: getprop requires 1 argument! ['path/to/property']";
		return 0;
	}
	
	string _prop = lua_tostring(L, 1);
	string type = PropertyTree::props()->getType(_prop);
	
	// Return property value
	if (type.compare("int") == 0) {
		lua_pushnumber(L, PropertyTree::props()->getInt(_prop));
	} else if (type.compare("double") == 0) {
		lua_pushnumber(L, PropertyTree::props()->getDouble(_prop));
	} else if (type.compare("string") == 0) {
		lua_pushstring(L, PropertyTree::props()->getString(_prop).c_str());
	} else {
		lua_pushnumber(L, -1);
	}
	
	return 1;
}

static int setprop(lua_State *L)
{
	// Get number of arguments
	int nArgs = lua_gettop(L);
	
	if (nArgs < 1) {
		cout << "[lua] error: setprop requires 2 arguments! ['path/to/property', value]";
		return 0;
	}
	
	string _prop = lua_tostring(L, 1);
	
	if (lua_isinteger(L, 2)) {
		PropertyTree::props()->set(_prop, (int) lua_tointeger(L, 2));
	} else if (lua_isnumber(L, 2)) {
		PropertyTree::props()->set(_prop, (double) lua_tonumber(L, 2));
	} else {
		PropertyTree::props()->set(_prop, string(lua_tostring(L, 2)));
	}
	
	return 0;
}
