#include "../../graphics/graphics.hxx"

int keypressed(void)
{
	int KeyIsPressed = 0;
	struct timeval tv;
	fd_set rdfs;

	tv.tv_sec = tv.tv_usec = 0;

	FD_ZERO(&rdfs);
	FD_SET(STDIN_FILENO, &rdfs);

	select(STDIN_FILENO+1, &rdfs, NULL, NULL, &tv);

	if(FD_ISSET(STDIN_FILENO, &rdfs)) {
		return getchar();
	}

	return 0;
}

static int keypress(lua_State *L)
{	
	lua_pushnumber(L, keypressed());
	
	return 1;
}
