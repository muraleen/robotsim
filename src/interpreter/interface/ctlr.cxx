#include "../../controller/controller.hxx"

vector<PIDController*> PID;

static int pid_setilim(lua_State *L)
{
	// Get number of arguments
	int nArgs = lua_gettop(L);
	
	if (nArgs < 2) {
		cout << "[lua] error: pid_setilim requires 2 arguments! [id, ilim]";
		return 0;
	}
	
	// Initialize controller
	int ctlr_ID = lua_tonumber(L, 1);
	double ilim = lua_tonumber(L, 1);
	
	PID.at(ctlr_ID)->setIntegralLimit(ilim);
	
	return 1;	
}

static int pid_init(lua_State *L)
{
	// Get number of arguments
	int nArgs = lua_gettop(L);
	
	if (nArgs < 3) {
		cout << "[lua] error: pid_init requires 3 arguments! [Kp, Ki, Kd]";
		return 0;
	}
	
	// Initialize controller
	double Kp = lua_tonumber(L, 1);
	double Ki = lua_tonumber(L, 2);
	double Kd = lua_tonumber(L, 3);
	
	PID.push_back(new PIDController(Kp, Ki, Kd));
	
	// Return controller id
	lua_pushnumber(L, PID.size()-1);
	
	return 1;	
}

static int pid_run(lua_State *L)
{
	// Get number of arguments
	int nArgs = lua_gettop(L);
	
	if (nArgs < 3) {
		cout << "[lua] error: pid_run requires 3 arguments! [id, dt, error]";
		return 0;
	}
	
	int ctlr_ID = lua_tonumber(L, 1);
	double dt = lua_tonumber(L, 2);
	double error = lua_tonumber(L, 3);
	
	double output = PID.at(ctlr_ID)->run(dt, error);
	
	// Return PID Controller output
	lua_pushnumber(L, output);
	
	return 1;
}

static int saturate(lua_State *L)
{
	// Get number of arguments
	int nArgs = lua_gettop(L);
	
	if (nArgs < 3) {
		cout << "[lua] error: saturate requires 3 arguments! [val, min, max]";
		return 0;
	}
	
	double val = lua_tonumber(L, 1);
	double min = lua_tonumber(L, 2);
	double max = lua_tonumber(L, 3);
	
	// Return PID Controller output
	lua_pushnumber(L, CtlHelper::saturate(val, min, max));
	
	return 1;
}

static int wrap(lua_State *L)
{
	// Get number of arguments
	int nArgs = lua_gettop(L);
	
	if (nArgs < 3) {
		cout << "[lua] error: wrap requires 3 arguments! [val, min, max]";
		return 0;
	}
	
	double val = lua_tonumber(L, 1);
	double min = lua_tonumber(L, 2);
	double max = lua_tonumber(L, 3);
	
	// Return PID Controller output
	lua_pushnumber(L, CtlHelper::wrap(val, min, max));
	
	return 1;
}
