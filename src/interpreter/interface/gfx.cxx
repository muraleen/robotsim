#include "../../graphics/graphics.hxx"

static int gfx_load3d(lua_State *L)
{
	int nArgs = lua_gettop(L);
	
	if (nArgs < 1) {
		cout << "[lua] error: gfx_load3d requires 1 arguments! [path]";
		return 0;
	}
	
	string file_path = string(lua_tostring(L, 1));
	
	int mIndex = Gfx::load3d(file_path);
	
	lua_pushnumber(L, mIndex);
	
	return 1;
}

static int gfx_setRot(lua_State *L)
{
	int nArgs = lua_gettop(L);
	
	if (nArgs < 5) {
		cout << "[lua] error: gfx_setRot requires 5 arguments! [id, angle, x, y, z]";
		return 0;
	}
	
	int id = lua_tonumber(L, 1);
	double angle = lua_tonumber(L, 2);
	double r_x = lua_tonumber(L, 3);
	double r_y = lua_tonumber(L, 4);
	double r_z = lua_tonumber(L, 5);
	
	Gfx::setRotation(id, angle, r_x, r_y, r_z);
	
	return 0;
}

static int gfx_setTrans(lua_State *L)
{
	int nArgs = lua_gettop(L);
	
	if (nArgs < 4) {
		cout << "[lua] error: gfx_setTrans requires 4 arguments! [id, x, y, z]";
		return 0;
	}
	
	int id = lua_tonumber(L, 1);
	double t_x = lua_tonumber(L, 2);
	double t_y = lua_tonumber(L, 3);
	double t_z = lua_tonumber(L, 4);
	
	Gfx::setTranslation(id, t_x, t_y, t_z);
	
	return 0;
}

static int gfx_setScale(lua_State *L)
{
	int nArgs = lua_gettop(L);
	
	if (nArgs < 4) {
		cout << "[lua] error: gfx_setScale requires 4 arguments! [id, x, y, z]";
		return 0;
	}
	
	int id = lua_tonumber(L, 1);
	double s_x = lua_tonumber(L, 2);
	double s_y = lua_tonumber(L, 3);
	double s_z = lua_tonumber(L, 4);
	
	Gfx::setScale(id, s_x, s_y, s_z);
	
	return 0;
}
