#include "NewtonEuler.hxx"

/* #include <chrono>

auto time_prev = chrono::high_resolution_clock::now();

double delta_time()
{
	auto time_now = chrono::high_resolution_clock::now();
	double t = (chrono::duration_cast<chrono::microseconds>(time_now - time_prev).count())/1000000.0;
	time_prev = time_now;
	return t;
} */

vec z0 = {0,0,1}; // Constant vector

vector<Link_FK_params> computeAdjacentLinkFK(vector<Link_FK_params> dyn) {
	for (int link=0; link<dyn.size(); link++) {
		if (link == dyn.size()-1) {
			dyn.at(link).J_mj = 0;
			dyn.at(link).N_mj = 0;
			dyn.at(link).R_i_j = eye<mat>(3,3);
			dyn.at(link).z_i_mj << 0 << endr << 0 << endr << 0;
		} else {
			dyn.at(link).J_mj = dyn.at(link+1).J_mi;
			dyn.at(link).N_mj = dyn.at(link+1).N_mi;
			dyn.at(link).R_i_j = dyn.at(link+1).R_h_i;
			dyn.at(link).z_i_mj = dyn.at(link+1).z_h_mi;
		}
	}
	return dyn;
}

Forward_recursion_vars computeForwardRecursionVars(Link_FK_params fk, Forward_recursion_vars _f, double qd, double qdd, JointType type)
{
	Forward_recursion_vars f;
	
	switch (type) {
	case prismatic:		// Prismatic Joint
		
		// Compute angular velocity (S&S Eq. 4.106)
		f.w = fk.R_h_i.t()*_f.w;
		
		// Compute angular acceleration (S&S Eq. 4.107)
		f.wd = fk.R_h_i.t()*_f.wd;
		
		// Compute linear acceleration (S&S Eq. 4.108)
		f.pdd = fk.R_h_i.t()*(_f.pdd + qdd*z0) + cross(2*qd*f.w, fk.R_h_i.t()*z0) + cross(f.wd, fk.r_h_i) + cross(f.w, cross(f.w, fk.r_h_i));
				
		break;
	case revolute:		// Revolute Joint
		
		// Compute angular velocity (S&S Eq. 4.106)
		f.w = fk.R_h_i.t()*(_f.w + qd*z0);
		
		// Compute angular acceleration (S&S Eq. 4.107)
		f.wd = fk.R_h_i.t()*(_f.wd + qdd*z0 + cross(qd*_f.w, z0));
		
		// Compute linear acceleration (S&S Eq. 4.108)
		f.pdd = fk.R_h_i.t()*_f.pdd + cross(f.wd, fk.r_h_i) + cross(f.w, cross(f.w, fk.r_h_i));
		
		break;
	}
	
	// Compute CM linear acceleration (S&S Eq. 4.109)
	f.pCdd = f.pdd + cross(f.wd, fk.r_i_Ci) + cross(f.w, cross(f.w, fk.r_i_Ci));
	
	// Compute motor angular acceleration (S&S Eq. 4.110)
	f.wMd = _f.wd + fk.N_mj*qdd*fk.z_h_mi + cross(fk.N_mj*qd*_f.w, fk.z_h_mi);
	
	return f;
}

Backward_recursion_vars computeBackwardRecursionVars(Link_FK_params fk, Backward_recursion_vars b_, Forward_recursion_vars f, double qd_, double qdd_)
{
	Backward_recursion_vars b;
	
	// Compute force vector on the link (S&S Eq. 4.111)
	b.f = fk.R_i_j*b_.f + fk.m_i*f.pCdd;
	
	// Compute moments vector on the link (S&S Eq. 4.112)
	b.u = fk.R_i_j*b_.u + cross(fk.R_i_j*b_.f, fk.r_i_Ci) + fk.J_i*f.wd + cross(f.w, fk.J_i*f.w) + fk.N_mj*qdd_*fk.J_mj*fk.z_i_mj + cross(fk.N_mj*qd_*fk.J_mj*f.w, fk.z_i_mj) -cross(b.f, fk.r_h_i + fk.r_i_Ci);
	
	return b;
}

double computeJointTorque(Link_FK_params fk, Forward_recursion_vars f, Backward_recursion_vars b, double qd, JointType type)
{

	// Compute joint torque (S&S Eq. 4.113)
	mat tau = fk.N_mi*fk.J_mi*f.wMd.t()*fk.z_h_mi + fk.fv_i*qd + fk.fc_i*sign(qd);

	switch (type) {
	case prismatic:
		tau += b.f.t()*fk.R_h_i.t()*z0;
	case revolute:
		tau += b.u.t()*fk.R_h_i.t()*z0;
	}
	
	return tau(0,0);
}

NEout NE0(vec q, vec qd, vec qdd, vec g, vector<Link_FK_params> dyn, vec w0, vec wd0, vec pdd0)
{
	// cout << "[105] dt = " << delta_time() << "s" << endl;

	NEout out;

	vec F(q.size());

	// Step 1: Forward recursion
	vector<Forward_recursion_vars> f;

	// Base velocities and accelerations (basically zeros considering gravity)
	Forward_recursion_vars f0; // Initial conditions
	f0.w = w0;
	f0.wd = wd0;
	f0.pdd = pdd0 - g;
	f.push_back(f0);

	for (int link=0; link<dyn.size(); link++) {		
		// Compute and append forward recursion variables
		f.push_back(computeForwardRecursionVars(dyn.at(link), f.at(link), qd(link), qdd(link), dyn.at(link).type));
	}
	
	// cout << "[126] dt = " << delta_time() << "s" << endl;
	
	// Step 2: Backward recursion (and torque computation)
	vector<Backward_recursion_vars> b;
	
	b.resize(f.size());
	
	// No external forces or moments
	b.at(b.size()-1).f = zeros<vec>(3);
	b.at(b.size()-1).u = zeros<vec>(3);
	
	// cout << "[137] dt = " << delta_time() << "s" << endl;
	
	for (int link=dyn.size()-1; link>=0; link--) {		
		// Compute and insert backward recursion variables
		
		if (link == dyn.size()-1) {
			b.at(link) = computeBackwardRecursionVars(dyn.at(link), b.at(link+1), f.at(link+1), 0, 0);
		} else {
			b.at(link) = computeBackwardRecursionVars(dyn.at(link), b.at(link+1), f.at(link+1), qd(link+1), qdd(link+1));
		}
		
		// Compute torque at joint
		F(link) = computeJointTorque(dyn.at(link), f.at(link+1), b.at(link), qd(link), dyn.at(link).type);
	}
	
	// cout << "[152] dt = " << delta_time() << "s" << endl;
	
	out.F = F;
	out.f = b.at(0).f;
	out.u = b.at(0).u;
	
	// cout << "[158] dt = " << delta_time() << "s" << endl;
	
	return out;
}

solverOut solveDirectDynamics(vec q, vec qd, vec u, vec g, vector<Link_FK_params> dyn, vec w0, vec wd0, vec pdd0)
{
	// cout << "[155] dt = " << delta_time() << "s" << endl;

	solverOut out;

	dyn = computeAdjacentLinkFK(dyn);

	mat H(q.size(), q.size());
	
	// cout << "[163] dt = " << delta_time() << "s" << endl;
	
	// Construct H(q) matrix
	for (int i=0; i<q.size(); i++) {

		// Create e_i vector
		vec e_i(q.size());
		e_i.zeros();
		e_i(i) = 1;
		
		NEout h_i_NE0 = NE0(q, zeros<vec>(q.size()), e_i, zeros<vec>(3), dyn, w0, wd0, pdd0);
		
		vec h_i = h_i_NE0.F;
		
		for (int j=0; j<q.size(); j++) H(j,i) = h_i(j);
	}
	
	// cout << "[180] dt = " << delta_time() << "s" << endl;
	
	// Compute vector of centrifugal, coriolis and gravitational forces
	NEout n_NE0 = NE0(q, qd, zeros<vec>(q.size()), g, dyn, w0, wd0, pdd0);
	
	vec n = n_NE0.F;
	
	// cout << "[187] dt = " << delta_time() << "s" << endl;
	
	vec uhat(q.size());
	int j=0;
	for (int i=0; i<q.size(); i++) {
		if (dyn.at(i).active) {
			uhat(i) = u(j);
			j++;
		} else {
			uhat(i) = -dyn.at(i).k * q(i) - dyn.at(i).b * qd(i);
		}
	}
	
	// cout << "[200] dt = " << delta_time() << "s" << endl;
	
	out.qdd = solve(H, uhat - n);
	
	/* cout << "[204] dt = " << delta_time() << "s" << endl;
	
	NEout full_NE0 = NE0(q, qd, out.qdd, g, dyn, w0, wd0, pdd0);
	out.f = full_NE0.f;
	out.u = full_NE0.u;
	
	cout << "[210] dt = " << delta_time() << "s" << endl; */
	
	return out;
}
