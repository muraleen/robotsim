#ifndef DYNAMICS_HXX
#define DYNAMICS_HXX

#include <armadillo>
#include <cmath>
#include <string>
#include <sstream>
#include <map>
#include <vector>
#include "../math/math.hxx"
#include "../parser/parser.hxx"
#include "../propertytree/propertytree.hxx"
#include "NewtonEuler.hxx"

using namespace std;
using namespace arma;

double interpolate(itable table);
vec dynamics(vec X, map<string, double> U, map<string, double> params, map<string, itable> tables, string model, map<string, string> path);

#endif
