#ifndef RK4_HXX
#define RK4_HXX

#include <vector>
#include <armadillo>
#include "dynamics.hxx"
#include "NewtonEuler.hxx"
#include "../parser/parser.hxx"

// Usage: RK4::integrate(dt, X, U, params, tables, models, paths)

class RK4
{
	public:
		static vec integrate(double dt, vec X, map<string, double> U, map<string, double> params, map<string, itable> tables, string model, map<string, string> path);
};

#endif
