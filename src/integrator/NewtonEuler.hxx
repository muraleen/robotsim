#ifndef NEWTONEULER_H
#define NEWTONEULER_H

#include <armadillo>
#include <cmath>
#include <string>
#include <map>
#include <vector>
#include "../math/math.hxx"

using namespace std;
using namespace arma;

// The following aims to implement the joint torque computation using the Newton-Euler recursive algorithm

enum JointType {
	prismatic = 0,
	revolute = 1
};

struct Forward_recursion_vars {
	vec w;		// Angular velocity of current frame, measured in current frame
	vec wd;		// Angular acceleration "
	vec pdd;	// Linear acceleration "
	vec pCdd;	// Linear acceleration of current CM, measured in current frame
	vec wMd;	// Angular acceleration of motor, measured in previous frame
};

struct Backward_recursion_vars {
	vec f;		// Forces on current frame, measured in current frame
	vec u;		// Moments "
};

struct Link_FK_params { // Using LaTeX form: Sciavicco & Siciliano notation
// Book assumes current link frame is located at the of the link
	/*
				// FWD RECURSION	BCK RECURSION	TORQUE COMP
	mat R;		// R^{i-1}_i		R^i_{i+1}		R^{i-1}_i
	vec z;		// z_0								z_0
	vec r;		// r^i_{i-1,i}		...				
	vec rC;		// r^i_{i,Ci}		...				
	vec zM;		// z^{i-1}_{mi}		z^i_{mi+1}		z^{i-1}_{mi}
	mat J;		//					I^i_i			
	double Jm;	// 					I_{mi+1}		I_{mi}
	double m;	//					m_i
	double N;	// k_{ri}			...				...
	double fv;	// 									F_{vi}
	// double fc;	//									F_{si} */
	
	// The above defitions are EXTREMELY CONFUSING, the below ones have EVERYTHING
	//	even though it's a little redundant. Atleast it works - optimization later
	
	// h = i - 1
	// j = i + 1
	
	mat R_h_i;
	mat R_i_j;
	
	vec r_h_i;	// Measured in i frame - only used in forward recursion
	
	vec r_i_Ci; // Always measured in i frame
	
	vec z_h_mi;
	vec z_i_mj;
	
	mat J_i;
	
	double m_i;
	double J_mi;
	double J_mj;
	double N_mi;
	double N_mj;
	double fv_i;
	double fc_i;
	
	double k;
	double b;
	
	JointType type;
	bool active;
};

// Compute adjacent link dynamics/fk parameters for backward recursion
vector<Link_FK_params> computeAdjacentLinkFK(vector<Link_FK_params> dyn);

// Inverse dynamics equations (Sciavicco & Siciliano 4.106 - 4.113)
// Forward recursion
Forward_recursion_vars computeForwardRecursionVars(Link_FK_params fk, Forward_recursion_vars _f, double qd, double qdd, JointType type);

// Backward recursion
Backward_recursion_vars computeBackwardRecursionVars(Link_FK_params fk, Backward_recursion_vars b_, Forward_recursion_vars f, double qd_, double qdd_);

// Torque computation (with friction)
double computeJointTorque(Link_FK_params fk, Forward_recursion_vars f, Backward_recursion_vars b, double qd, JointType type);

struct NEout {
	vec F;
	vec f;
	vec u;
};

struct solverOut {
	vec qdd;
	vec f;
	vec u;
};

// These functions implement the entire Newton-Euler recursive algorithm used to compute the direct dynamics parameters H(q) and n(q,qd)
NEout NE0(vec q, vec qd, vec qdd, vec g, vector<Link_FK_params> dyn, vec w0, vec wd0, vec pdd0); // set g = [0;0;0] for NE0 and g = [0;0;-9.81] to NE0g
// NOTE: dyn must be computed and passed from the dynamics function where the NE recursive solver is called

// Direct dynamics solver: qdd = H(q) \ (u - n(q,qd))
solverOut solveDirectDynamics(vec q, vec qd, vec u, vec g, vector<Link_FK_params> dyn, vec w0, vec wd0, vec pdd0); // This function makes calls to NE0 functions

#endif
