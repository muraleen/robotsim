#include "RK4.hxx"
/* #include <chrono>

auto time_prev = chrono::high_resolution_clock::now();

double delta_time()
{
	auto time_now = chrono::high_resolution_clock::now();
	double t = (chrono::duration_cast<chrono::microseconds>(time_now - time_prev).count())/1000000.0;
	time_prev = time_now;
	return t;
} */

vec RK4::integrate(double dt, vec X, map<string, double> U, map<string, double> params, map<string, itable> tables, string model, map<string, string> path)
{
	
	// cout << "[17] dt = " << delta_time() << "s" << endl;
	
	vec k1 = dynamics(X, U, params, tables, model, path);
	
	// cout << "[21] dt = " << delta_time() << "s" << endl;
	
	vec k2 = dynamics(X + k1*dt/2, U, params, tables, model, path);
	
	// cout << "[25] dt = " << delta_time() << "s" << endl;
	
	vec k3 = dynamics(X + k2*dt/2, U, params, tables, model, path);
	
	// cout << "[29] dt = " << delta_time() << "s" << endl;
	
	vec k4 = dynamics(X + k3*dt, U, params, tables, model, path);
	
	// cout << "[33] dt = " << delta_time() << "s" << endl;
	
	vec X_next = X + ((k1/6) + (k2/3) + (k3/3) + (k4/6))*dt;
	
	// cout << "[37] dt = " << delta_time() << "s" << endl;
	
	return X_next;
	
	
	// Euler method for now
	// return X + dynamics(X, U, params, tables, model, path) * dt;
}
