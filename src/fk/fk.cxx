#include "fk.hxx"
#include "derivatives.hxx"

#include <iostream>

ForwardKinematics::ForwardKinematics(FK _fk, map<string, double> _params, map<string, double> gamma, double _dt, bool FKDeriv_cxx, string _model)
{
	this->fk = _fk;	
	this->params = _params;
	this->dt = _dt;
	this->cxxDeriv = FKDeriv_cxx;
	this->model = _model;
	
	_IIRX.resize(fk.links.size());
	_ITX.resize(fk.links.size());
	IIRXdot.resize(fk.links.size());
	ITXdot.resize(fk.links.size());
	
	vec IIRX(3);
	mat ITX(3,3);
	
	for (int i=0; i<fk.links.size(); i++) {
	
		Link link = fk.links.at(i);
		string ref = link.ref;
		
		IIRX.zeros();
		ITX.eye();
		
		vector<Link> links;
		
		while (ref.compare("I") != 0) { // Trace back to inertial frame
			links.push_back(link);
			link = fk.links.at(fk.linkMap[ref]);
			ref = link.ref;
		}
		
		links.push_back(link);
		
		for (int j=links.size()-1; j>=0; j--) {
			IIRX += ITX*genRvec(links.at(j).R, gamma);
			ITX *= genTmat(links.at(j).T, gamma);
		}
		
		_IIRX.at(i) = IIRX;
		_ITX.at(i) = ITX;
	}
}

double ForwardKinematics::getTotalMass()
{
	double m = 0;
	
	for (int i=0; i<fk.links.size(); i++) {
		m += params[fk.links.at(i).name + "_m"];
	}
	
	return m;
}

vector<vec> ForwardKinematics::calcMomentum(vec X, map<string, double> gamma)
{
	vec p(3), p_X(3), h(3), XXwI(3);
	p.zeros(); h.zeros();
	
	double m_X;
	vec G_X(3);
	mat J_X(3,3);
	
	vec IIRX(3);
	mat ITX(3,3);
	
	if (cxxDeriv) {
		IIRXdot = computeRdot(X, params, model);
		ITXdot = computeTdot(X, params, model);
	}
	
	for (int i=0; i<fk.links.size(); i++) {
	
		Link link = fk.links.at(i);
		string ref = link.ref;
	
		IIRX.zeros();
		ITX.eye();
		
		vector<Link> links;
		
		while (ref.compare("I") != 0) { // Trace back to inertial frame
			links.push_back(link);
			link = fk.links.at(fk.linkMap[ref]);
			ref = link.ref;
		}
		
		links.push_back(link);
		link = fk.links.at(i);
		
		for (int j=links.size()-1; j>=0; j--) {
			IIRX += ITX*genRvec(links.at(j).R, gamma);
			ITX *= genTmat(links.at(j).T, gamma);
		}
		
		if (!cxxDeriv) {
			IIRXdot.at(i) = (IIRX - _IIRX.at(i)) / dt;
			ITXdot.at(i) = (ITX - _ITX.at(i)) / dt;
		}
		
		m_X = params[link.name + "_m"];
		
		G_X << params[link.name + "_Gx"] << endr
			<< params[link.name + "_Gy"] << endr
			<< params[link.name + "_Gz"] << endr;
		
		J_X << params[link.name + "_Jxx"] << params[link.name + "_Jxy"] << params[link.name + "_Jxz"] << endr
			<< params[link.name + "_Jxy"] << params[link.name + "_Jyy"] << params[link.name + "_Jyz"] << endr
			<< params[link.name + "_Jxz"] << params[link.name + "_Jyz"] << params[link.name + "_Jzz"] << endr;
		
		p_X = IIRXdot.at(i)*m_X + ITXdot.at(i)*G_X;
		p += p_X;
		
		h += cross(IIRX, p_X) - cross(IIRXdot.at(i), ITX*G_X) + ITX*J_X*unskew(inv(ITX)*ITXdot.at(i));
		
		_IIRX.at(i) = IIRX;
		_ITX.at(i) = ITX;
	}
	
	vector<vec> momentum;
	momentum.push_back(p);
	momentum.push_back(h);
	
	return momentum;
}

vec ForwardKinematics::getCenterOfMass(map<string, double> gamma)
{

	vec m_cm(3), cm_X(3);
	double m = 0, m_X = 0;
	
	vec IIRX(3);
	mat ITX(3,3);
	
	m_cm.zeros();
	
	for (int i=0; i<fk.links.size(); i++) {
	
		Link fklink = fk.links.at(i);
		string link = fklink.name;
		string ref = fklink.ref;

		IIRX.zeros();
		ITX.eye();
		
		vector<Link> links;
		
		while (ref.compare("I") != 0) { // Trace back to inertial frame
			links.push_back(fklink);
			fklink = fk.links.at(fk.linkMap[ref]);
			ref = fklink.ref;
		}
		
		links.push_back(fklink);
		
		for (int j=links.size()-1; j>=0; j--) {
			IIRX += ITX*genRvec(links.at(j).R, gamma);
			ITX *= genTmat(links.at(j).T, gamma);
		}
		
		vec G(3), IcmX(3);
		G << params[link + "_Gx"] << endr
		  << params[link + "_Gy"] << endr
		  << params[link + "_Gz"] << endr;
		m_X = params[link + "_m"];
		m += m_X;
		cm_X = G / m_X;
		IcmX = IIRX + ITX*cm_X;
		m_cm += m_X * IcmX;
	}
	
	vec cm(3);
	cm = m_cm / m;
	
	return cm;
}

double ForwardKinematics::getKE()
{
	
}

double ForwardKinematics::getPE()
{

}

mat ForwardKinematics::genTmat(string Tstr, map<string, double> gamma)
{
	mat T = eye<mat>(3,3);
	
	vector<string> rotMats = Parser::splitStr(Tstr, "*");
	
	for (int i=0; i<rotMats.size(); i++) {
		string rot = rotMats.at(i);
		
		mat t(3,3);
		
		rot.erase(remove(rot.begin(), rot.end(), ')'), rot.end());
		vector<string> fnSet = Parser::splitStr(rot, "(");
		
		if (fnSet.at(0).compare("rotx") == 0) {
			T *= rotx(findVar(fnSet.at(1), gamma));
		} else if (fnSet.at(0).compare("roty") == 0) {
			T *= roty(findVar(fnSet.at(1), gamma));
		} else if (fnSet.at(0).compare("rotz") == 0) {
			T *= rotz(findVar(fnSet.at(1), gamma));
		} else if (fnSet.at(0).compare("rot") == 0) {
			vector<string> argSet = Parser::splitStr(fnSet.at(1), ",");
			vec axis(3);
			axis << findVar(argSet.at(1), gamma) << endr
				 << findVar(argSet.at(2), gamma) << endr
				 << findVar(argSet.at(3), gamma) << endr;
				
			T *= rotk(findVar(argSet.at(0), gamma), axis);
		}
	}
	
	return T;
}

vec ForwardKinematics::genRvec(vector<string> Rstr, map<string, double> gamma)
{
	vec R(3);
	R << findVar(Rstr.at(0), gamma) << endr
	  << findVar(Rstr.at(1), gamma) << endr
	  << findVar(Rstr.at(2), gamma) << endr;
	return R;
}

double ForwardKinematics::findVar(string vStr, map<string, double> gamma)
{
	double m = 1;
	
	if (vStr[0] == '-') {
		m = -1;
		vStr.erase(0,1);
	}
	
	if (Parser::isNumber(vStr)) {
		return m*stod(vStr);
	} else {
		if (gamma.find(vStr) == gamma.end()) {
			return m*params[vStr];
		} else {
			return m*gamma[vStr];
		}
	}
}
