#ifndef DERIVATIVES_HXX
#define DERIVATIVES_HXX

#include <armadillo>
#include <vector>
#include <string>
#include <map>

using namespace std;
using namespace arma;

vector<vec> computeRdot(vec X, map<string, double> params, string model);
vector<mat> computeTdot(vec X, map<string, double> params, string model);

#endif
