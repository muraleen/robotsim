#ifndef FK_HXX
#define FK_HXX

#include <armadillo>
#include <string>
#include <vector>
#include <map>

#include "../parser/parser.hxx"
#include "../math/math.hxx"

using namespace std;
using namespace arma;

class ForwardKinematics
{
	public:
		ForwardKinematics(FK _fk, map<string, double> _params, map<string, double> gamma, double _dt, bool FKDeriv_cxx, string _model);
		double getTotalMass();
		vec getCenterOfMass(map<string, double> gamma);
		vector<vec> calcMomentum(vec X, map<string, double> gamma);
		double getKE();
		double getPE();
	private:
		FK fk;
		map<string, double> params;
		mat genTmat(string Tstr, map<string, double> gamma);
		vec genRvec(vector<string> Rstr, map<string, double> gamma);
		double findVar(string vStr, map<string, double> gamma);
		vector<vec> _IIRX;
		vector<mat> _ITX;
		vector<vec> IIRXdot;
		vector<mat> ITXdot;
		bool cxxDeriv;
		double dt;
		string model;
};

#endif
