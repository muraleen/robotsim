#ifndef PARSER_HXX
#define PARSER_HXX

#include <armadillo>
#include <vector>
#include <string>
#include <map>

using namespace arma;
using namespace std;

struct Animation
{
	string object;
	string type;
	vector<double> center;
	vector<double> axis;
	double factor;
	string property;
};

struct AnimationSet
{
	string object;
	vector<Animation> animations;
};

struct VarsList
{
	vector<vector<string>> vars;
	vector<map<string, double>> varsMap;
	map<string, vector<double>> wrap;
	map<string, vector<double>> sat;
};

struct Link
{
	string name;
	string type;
	string ref;
	vector<string> R;
	string T;
};

struct itable
{
	string row;
	string col;
	vector<double> rowLookUp;
	vector<double> colLookUp;
	vector<vector<double>> data;
};

struct FK
{
	map<string, double> linkMap;
	vector<Link> links;
};

class Parser
{
	public:
		static vector<string> splitStr(string input, string delimiter);
		static string replaceStr(string mStr, string fStr, string rStr);
		static map<string, string> parseModelFile(string model);
		static vector<AnimationSet> parseAnimationsFile(string file);
		static map<string, double> parseParamsFile(string file);
		static map<string, itable> parseParamsFileTables(string file);
		static VarsList parseVarsFile(string file);
		static FK parseFKFile(string file);
		static vector<string> parseLogCfg(map<string, double> gamma, map<string, double> inputs);
		static string removeUnwantedCharacters(string line);
		static bool isNumber(string str);
};

#endif
