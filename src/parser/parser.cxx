#include "parser.hxx"

#include <iostream>

#include <fstream>
#include <algorithm>

vector<string> Parser::splitStr(string input, string delimiter)
{
	vector<string> output;
	
	size_t start = 0;
	size_t end = 0;
	
	while(start != string::npos && end != string::npos) {
		start = input.find_first_not_of(delimiter, end);
		
		if(start != string::npos) {
			end = input.find_first_of(delimiter, start);
			
			if(end != string::npos) {
				output.push_back(input.substr(start, end-start));
			} else {
				output.push_back(input.substr(start));
			}
		}
	}
	
	return output;
}

vector<string> Parser::parseLogCfg(map<string, double> gamma, map<string, double> inputs)
{
	vector<string> logProps;
	
	ifstream cfg ("log.cfg");
	if (cfg.is_open()) {
		string line;
		while(getline(cfg, line)) {
			line = removeUnwantedCharacters(line);
			if (line.substr(0,1).compare("#") != 0) {
				vector<string> set = splitStr(line, "=");
				if (set.size() >= 1) {
					if (set.at(0).compare("_GAMMA") == 0) {
						for (auto it: gamma) {
							logProps.push_back("/dynamics/gamma/" + it.first);
						}
					} else if (set.at(0).compare("_GAMMA_DOT") == 0) {
						for (auto it: gamma) {
							logProps.push_back("/dynamics/gamma-dot/" + it.first);
						}
					} else if (set.at(0).compare("_INPUTS") == 0) {
						for (auto it: inputs) {
							logProps.push_back("/dynamics/inputs/" + it.first);
						}
					} else {
						logProps.push_back(set.at(0));
					}
				}
			}
		}
	}
	cfg.close();
	
	return logProps;
}

map<string, string> Parser::parseModelFile(string model)
{
	map<string, string> path;
	
	ifstream modelFile ("model/" + model + "/" + model + ".model");
	if (modelFile.is_open()) {
		string line;
		while(getline(modelFile, line)) {
			line = removeUnwantedCharacters(line);
			if (line.substr(0,1).compare("#") != 0) {
				vector<string> set = splitStr(line, "=");
				if (set.size() == 2) {
					if ((set.at(0).compare("SCRIPTS") == 0) || (set.at(0).substr(0,4).compare("CFG:") == 0)) {
						path[set.at(0)] = set.at(1);
					} else {
						path[set.at(0)] = "model/" + model + "/" + set.at(1);
					}
				}
			}
		}
	}
	modelFile.close();
	
	return path;
}

vector<AnimationSet> Parser::parseAnimationsFile(string file)
{
	vector<string> objects;
	map<string, vector<Animation>> objectMap;
	
	ifstream animFile (file);
	if (animFile.is_open()) {
		string line;
		bool inSegment = false;
		vector<string> anim_objects;
		Animation anim;
		while (getline(animFile, line)) {
			line = removeUnwantedCharacters(line);
			if (line.substr(0,1).compare("#") != 0) {
				if (!inSegment && line.substr(0,8).compare("animate:") == 0) {
					inSegment = true;
					vector<string> set = splitStr(line, ":");
					anim_objects = splitStr(set.at(1), ",");
				} else if (inSegment) {
					if (line.substr(0,3).compare("end") == 0) {
						inSegment = false;
						// animations.push_back(anim);
						for (int i=0; i<anim_objects.size(); i++) {
							anim.object = anim_objects.at(i);
							objectMap[anim.object].push_back(anim);
							if (find(objects.begin(), objects.end(), anim.object) == objects.end()) {
								objects.push_back(anim.object); // Append to vector
							}
						}
					} else {
						vector<string> set = splitStr(line, ":");
						if (set.at(0).compare("type") == 0) {
							anim.type = set.at(1);
						} else if (set.at(0).compare("center") == 0) {
							anim.center.resize(3);
							vector<string> data = splitStr(set.at(1), ",");
							anim.center.at(0) = stod(data.at(0));
							anim.center.at(1) = stod(data.at(1));
							anim.center.at(2) = stod(data.at(2));
						} else if (set.at(0).compare("axis") == 0) {
							anim.axis.resize(3);
							vector<string> data = splitStr(set.at(1), ",");
							anim.axis.at(0) = stod(data.at(0));
							anim.axis.at(1) = stod(data.at(1));
							anim.axis.at(2) = stod(data.at(2));
						} else if (set.at(0).compare("prop") == 0 || set.at(0).compare("value") == 0) {
							anim.property = set.at(1);
						} else if (set.at(0).compare("factor") == 0) {
							anim.factor = stod(set.at(1));
						}
					}
				}
			}
		}
	}
	animFile.close();
	
	vector<AnimationSet> animations;
	
	for (int i=0; i<objects.size() ; i++) {
		AnimationSet animSet;
		animSet.object = objects.at(i);
		animSet.animations = objectMap[objects.at(i)];
		animations.push_back(animSet);
	}
	
	return animations;
}

map<string, double> Parser::parseParamsFile(string file)
{
	map<string, double> params;
	
	ifstream paramsFile (file);
	if (paramsFile.is_open()) {
		string line;
		bool usePrefix = false;
		bool intable = false;
		string prefix;
		while (getline(paramsFile, line)) {
			line = removeUnwantedCharacters(line);
			if (!usePrefix && line.substr(0,10).compare("useprefix:") == 0) {
				usePrefix = true;
				vector<string> set = splitStr(line, ":");
				prefix = set.at(1);
			} else if (!usePrefix && line.substr(0,8).compare("noprefix") == 0) {
				usePrefix = true;
				prefix = "";
			} else {
				if (!intable) {
					if (line.substr(0,3).compare("end") == 0) {
						usePrefix = false;
					} else {
						vector<string> set = splitStr(line, "=");
						if (set.size() > 1) {
							if (set.at(1).substr(0,11).compare("interpolate") == 0) {
								intable = true;
							} else {
								if (usePrefix) {
									params[prefix + set.at(0)] = stod(set.at(1));
								} else {
									params[set.at(0)] = stod(set.at(1));
								}
							}
						}
					}
				} else {
					if (line.compare("}") == 0) {
						intable = false;
					}
				}
			}
		}
	}
	paramsFile.close();
	
	return params;
}

map<string, itable> Parser::parseParamsFileTables(string file)
{
	map<string, itable> tables;
	
	ifstream paramsFile (file);
	if (paramsFile.is_open()) {
		string line;
		bool usePrefix = false;
		bool intable = false;
		bool header = false;
		itable table;
		string tname;
		vector<double> rowLookUp;
		vector<vector <double>> tableData;
		string prefix;
		while (getline(paramsFile, line)) {
			line = removeUnwantedCharacters(line);
			if (!usePrefix && line.substr(0,10).compare("useprefix:") == 0) {
				usePrefix = true;
				vector<string> set = splitStr(line, ":");
				prefix = set.at(1);
			} else {
				if (!intable) {
					if (line.substr(0,3).compare("end") == 0) {
						usePrefix = false;
					} else {
						vector<string> set = splitStr(line, "=");
						if (set.size() > 1) {
							if (set.at(1).substr(0,11).compare("interpolate") == 0) {
								intable = true;
								tname = set.at(0);
								vector<string> rc = splitStr(splitStr(splitStr(set.at(1),"[").at(1),"]").at(0), ",");
								table.row = rc.at(0);
								rowLookUp.clear();
								tableData.clear();
								if (rc.size() > 1) {
									table.col = rc.at(1);
									header = true;
								} else {
									table.col = "null";
								}
							}
						}
					}
				} else {
					if (line.compare("}") == 0) {
						intable = false;
						table.rowLookUp = rowLookUp;
						table.data = tableData;
						if (usePrefix) {
							tables[prefix + tname] = table;
						} else {
							tables[tname] = table;
						}
					} else {
						if (header) {
							vector<string> data = splitStr(line, ",");
							vector<double> colLookUp;
							for (int i=0; i<data.size(); i++) {
								colLookUp.push_back(stod(data.at(i)));
							}
							table.colLookUp = colLookUp;							
							header = false;
						} else {
							vector<string> data = splitStr(line, ",");
							rowLookUp.push_back(stod(data.at(0)));
							vector<double> rowData;
							for (int i=1; i<data.size(); i++) {
								rowData.push_back(stod(data.at(i)));
							}
							tableData.push_back(rowData);
						}
					}
				}
			}
		}
	}
	paramsFile.close();
	
	return tables;
}

FK Parser::parseFKFile(string file)
{
	vector<Link> links;
	map<string, double> linkMap;
	
	Link robotLink;
	robotLink.R.resize(3);
	
	ifstream FKfile (file);
	if (FKfile.is_open()) {
		string line;
		string segment;
		bool inSegment = false;
		while (getline(FKfile, line)) {
			line = removeUnwantedCharacters(line);
			line = replaceStr(line, "PI", "3.141592653597");
			line = replaceStr(line, "matrix.I", "rotz(0)");
			vector<string> set = splitStr(line, ":");
			if (set.size() > 0) {
				if (!inSegment) {
					if (set.at(0).compare("definelink") == 0) {
						linkMap[set.at(1)] = links.size();
						robotLink.name = set.at(1);
						inSegment = true;
					}
				} else {
					if (set.at(0).compare("end") == 0) {
						links.push_back(robotLink);
						inSegment = false;
					} else if (set.at(0).compare("ref") == 0) {
						robotLink.ref = set.at(1);
					} else if (set.at(0).compare("type") == 0) {
						robotLink.type = set.at(1);
					} else if (set.at(0).compare("R") == 0) {
						vector<string> R_vect = splitStr(set.at(1), ",");
						for (int i=0; i<3; i++) {
							robotLink.R.at(i) = R_vect.at(i);
						}
					} else if (set.at(0).compare("T") == 0) {
						robotLink.T = set.at(1);
					}
				}
			}
		}
	}
	
	FK robotFK;
	robotFK.linkMap = linkMap;
	robotFK.links = links;
	
	return robotFK;
}

VarsList Parser::parseVarsFile(string file)
{
	map<string, double> gamma;
	map<string, double> gammad;
	map<string, double> inputs;
	map<string, double> others;
	map<string, vector<double>> wrap;
	map<string, vector<double>> sat;
	
	vector<string> gammaStr;
	vector<string> gammadStr;
	vector<string> inputsStr;
	vector<string> othersStr;
	
	ifstream varsFile (file);
	if (varsFile.is_open()) {
		string line;
		string segment;
		bool inSegment = false;
		while (getline(varsFile, line)) {
			line = removeUnwantedCharacters(line);
			line = replaceStr(line, "PI", "3.141592653597");
			if (!inSegment) {
				if (line.compare("gamma") == 0) {
					inSegment = true;
					segment = "gamma";
				} else if (line.compare("inputs") == 0) {
					inSegment = true;
					segment = "inputs";
				} else if (line.compare("others") == 0) {
					inSegment = true;
					segment = "others";
				} else if (line.compare("gammad") == 0) {
					inSegment = true;
					segment = "gammad";
				}
			} else if (line.compare("end") == 0) {
				inSegment = false;
			} else {
				vector<string> set = splitStr(line, "=");
				if (set.size() > 1) {
					if (segment.compare("gamma") == 0) {
						gamma[set.at(0)] = stod(set.at(1));
						gammaStr.push_back(set.at(0));
					} else if (segment.compare("inputs") == 0) {
						inputs[set.at(0)] = stod(set.at(1));
						inputsStr.push_back(set.at(0));
					} else if (segment.compare("others") == 0) {
						others[set.at(0)] = stod(set.at(1));
						othersStr.push_back(set.at(0));
					} else if (segment.compare("gammad") == 0) {
						gammad[set.at(0)] = stod(set.at(1));
						gammadStr.push_back(set.at(0));
					}
				}
				if (set.size() > 2) {
					vector<string> fnSet = splitStr(set.at(2), ":");
					if (fnSet.at(0).compare("wrap") == 0) {
						vector<string> argSet = splitStr(fnSet.at(1), ",");
						wrap[set.at(0)].push_back(stod(argSet.at(0)));
						wrap[set.at(0)].push_back(stod(argSet.at(1)));
					} else if (fnSet.at(0).compare("sat") == 0) {
						vector<string> argSet = splitStr(fnSet.at(1), ",");
						sat[set.at(0)].push_back(stod(argSet.at(0)));
						sat[set.at(0)].push_back(stod(argSet.at(1)));
					}
				}
			}
		}
	}
	varsFile.close();
	
	vector<map<string,double>> vars;
	vars.push_back(gamma);
	vars.push_back(inputs);
	vars.push_back(others);
	vars.push_back(gammad);
	
	vector<vector<string>> varStr;
	varStr.push_back(gammaStr);
	varStr.push_back(inputsStr);
	varStr.push_back(othersStr);
	varStr.push_back(gammadStr);
	
	
	VarsList vList;
	vList.vars = varStr;
	vList.varsMap = vars;
	vList.wrap = wrap;
	vList.sat = sat;
	
	return vList;
}

string Parser::removeUnwantedCharacters(string line)
{
	line.erase(remove(line.begin(), line.end(), ' '), line.end());
	line.erase(remove(line.begin(), line.end(), '\t'), line.end());
	line.erase(remove(line.begin(), line.end(), '\r'), line.end());
	line.erase(remove(line.begin(), line.end(), '\n'), line.end());

	return line;
}

string Parser::replaceStr(string mStr, string fStr, string rStr)
{
	size_t f = mStr.find(fStr);
	while (f != string::npos) {
		mStr.replace(f, fStr.length(), rStr);
		f = mStr.find(fStr);
	}
	return mStr;
}

bool Parser::isNumber(string str)
{
	return isdigit(str[0]);
}
