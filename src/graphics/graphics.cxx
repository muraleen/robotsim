#include "graphics.hxx"

#include "../propertytree/propertytree.hxx"

#define PI 3.14159265358979323846

bool KeyboardEventHandler::handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa)
{
	switch(ea.getEventType()) {
		case (osgGA::GUIEventAdapter::KEYDOWN):
			PropertyTree::props()->set("/keyboard/state", 1);
			PropertyTree::props()->set("/keyboard/key", (int)ea.getKey());
			break;
		case (osgGA::GUIEventAdapter::KEYUP):
			PropertyTree::props()->set("/keyboard/state", 0);
			break;
	}
	
	return false;
}

Graphics::Graphics()
{
	this->scene = new Group();
	viewer.setSceneData(scene);
	viewer.setUpViewInWindow(50, 50, 1280, 800);
	viewer.setCameraManipulator(new osgGA::TrackballManipulator());
	
	keyboardEventHandler = new KeyboardEventHandler();
	viewer.addEventHandler(keyboardEventHandler);
	
	PropertyTree::props()->set("/keyboard/state", 0);
	PropertyTree::props()->set("/keyboard/key", 0);
}

void Graphics::visualize()
{
	viewer.realize();
}

Node* Graphics::loadModel(string file)
{
	Node *node = osgDB::readNodeFile(file);
	if (!node) {
        std::cerr << "Can't load model from file '" << file << "'" << endl;
        exit(0);
    }
    
    return node;
}

Node* Graphics::findObjectNode(Node *node, const string &name)
{
	if (!node) {
		return NULL;
	}
	
	if (node->getName() == name) {
		return node;
	}
	
	Group *group = node->asGroup();
	if (group) {
		for (int i=0; i<group->getNumChildren(); i++) {
			Node *object = findObjectNode(group->getChild(i), name);
			if (object)
				return object;
		}
	}
	
	return NULL;
}

void Graphics::setAnimations(Node *node, vector<AnimationSet> animations)
{
	this->node = node;
	this->animations = animations;

	for (int i=0; i<animations.size(); i++) {		
		MatrixTransform *transform = new MatrixTransform;
		transform->addChild(findObjectNode(node, animations.at(i).object));
		
		this->transforms.push_back(transform);
		
		this->scene->addChild(transform);
	}
}

void Graphics::update(map<string, double> gamma, map<string, double> inputs)
{
	map<string, double> vars = gamma;
	for (map<string, double>::iterator it = inputs.begin(); it != inputs.end(); ++it) {
		vars[it->first] = inputs[it->first];
	}

	for (int i=0; i<animations.size(); i++) {
		Matrix mat = Matrix::identity();
		for (int j=0; j<animations.at(i).animations.size(); j++) {
			Animation anim = animations.at(i).animations.at(j);
			double value;
			if (anim.property[0] == '/') {
				value = PropertyTree::props()->getDouble(anim.property);
			} else {
				value = vars[anim.property];
			}
			if (anim.type.compare("rotate") == 0) {
				const Vec3d axis(anim.axis.at(0), anim.axis.at(2), -anim.axis.at(1));
				mat = mat * Matrix::translate(-anim.center.at(0), -anim.center.at(2), anim.center.at(1)) * Matrix::rotate(anim.factor * value, axis) * Matrix::translate(anim.center.at(0), anim.center.at(2), -anim.center.at(1));
			} else if (anim.type.compare("translate") == 0) {
				mat = mat * Matrix::translate(value * anim.axis.at(0), value * anim.axis.at(2), -value * anim.axis.at(1));
			} else if (anim.type.compare("scale") == 0) {
				const Vec3d axis(value * anim.axis.at(0), value * anim.axis.at(2), value * anim.axis.at(1));
				mat = mat * Matrix::translate(-anim.center.at(0), -anim.center.at(2), anim.center.at(1)) * Matrix::scale(axis) * Matrix::translate(anim.center.at(0), anim.center.at(2), -anim.center.at(1));
			} // "static" (or anything else at the moment) just displays the model
		}
		this->transforms.at(i)->setMatrix(mat);
	}

	viewer.frame();
}

int Graphics::addModel(string file) {

	Node *model = loadModel(file);
	MatrixTransform *transform = new MatrixTransform;
	transform->addChild(model);
	
	this->loaded_xforms.push_back(transform);
	lx_rot.push_back(Matrix::identity());
	lx_trans.push_back(Matrix::identity());
	lx_scale.push_back(Matrix::identity());
	
	this->scene->addChild(transform);
	
	return (this->loaded_xforms.size() - 1);
}

void Graphics::setRotation(int mIndex, double angle, double r_x, double r_y, double r_z)
{
	const Vec3d axis(r_x, r_z, -r_y);
	lx_rot.at(mIndex) = Matrix::rotate(angle, axis);
	updateTransform(mIndex);
}

void Graphics::setTranslation(int mIndex, double t_x, double t_y, double t_z)
{
	lx_trans.at(mIndex) = Matrix::translate(t_x, t_z, -t_y);
	updateTransform(mIndex);
}

void Graphics::setScale(int mIndex, double s_x, double s_y, double s_z)
{
	const Vec3d axis(s_x, s_z, -s_y);
	lx_scale.at(mIndex) = Matrix::scale(axis);
	updateTransform(mIndex);
}

void Graphics::updateTransform(int mIndex)
{	
	loaded_xforms.at(mIndex)->setMatrix(lx_rot.at(mIndex) * lx_trans.at(mIndex) * lx_scale.at(mIndex));
}

bool Graphics::isRunning()
{
	return !viewer.done();
}

Graphics *Gfx::gfx = NULL;

void Gfx::setGfxInstance(Graphics *_gfx)
{
	gfx = _gfx;
}

int Gfx::load3d(string file)
{
	return gfx->addModel(file);
}

void Gfx::setRotation(int mIndex, double angle, double r_x, double r_y, double r_z)
{
	gfx->setRotation(mIndex, angle, r_x, r_y, r_z);
}

void Gfx::setTranslation(int mIndex, double t_x, double t_y, double t_z)
{
	gfx->setTranslation(mIndex, t_x, t_y, t_z);
}

void Gfx::setScale(int mIndex, double s_x, double s_y, double s_z)
{
	gfx->setScale(mIndex, s_x, s_y, s_z);
}
