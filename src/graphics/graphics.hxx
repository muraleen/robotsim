#ifndef GRAPHICS_HXX
#define GRAPHICS_HXX

#include <vector>
#include <string>
#include <osg/Object>
#include <osg/Node>
#include <osg/Transform>
#include <osg/Matrix>
#include <osg/MatrixTransform>
#include <osgDB/ReadFile>
#include <osgViewer/Viewer>
#include <osgGA/TrackballManipulator>
#include <osgGA/GUIEventHandler>

#include "../parser/parser.hxx"

using namespace std;
using namespace osg;

class KeyboardEventHandler : public osgGA::GUIEventHandler
{
	public:
		virtual bool handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter&);
		virtual void accept(osgGA::GUIEventHandlerVisitor& v) { v.visit(*this); };
};

class Graphics
{
	public:
		Graphics();
		Node *loadModel(string file);
		Node *findObjectNode(Node *node, const string &name);
		void setAnimations(Node *node, vector<AnimationSet> animations);
		void update(map<string, double> gamma, map<string, double> inputs);
		int addModel(string file);
		void visualize();
		void setRotation(int mIndex, double angle, double r_x, double r_y, double r_z);
		void setTranslation(int mIndex, double t_x, double t_y, double t_z);
		void setScale(int mIndex, double s_x, double s_y, double s_z);
		bool isRunning();
		Group *scene;
		KeyboardEventHandler* keyboardEventHandler;
	private:
		Node *node;
		vector<AnimationSet> animations;
		void updateTransform(int mIndex);
		osgViewer::Viewer viewer;
		vector<MatrixTransform*> transforms;
		vector<MatrixTransform*> loaded_xforms;
		vector<Matrix> lx_rot;
		vector<Matrix> lx_trans;
		vector<Matrix> lx_scale;
};

// Local Graphics Interface
class Gfx
{
	public:
		static void setGfxInstance(Graphics *_gfx);
		static int load3d(string file);
		static void setRotation(int mIndex, double angle, double r_x, double r_y, double r_z);
		static void setTranslation(int mIndex, double t_x, double t_y, double t_z);
		static void setScale(int mIndex, double s_x, double s_y, double s_z);
	private:
		static Graphics *gfx;
};

#endif
