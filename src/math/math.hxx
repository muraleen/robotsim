#ifndef MATH_HXX
#define MATH_HXX

#include <cmath>
#include <armadillo>

using namespace arma;

double sign(double val);
double roundto(double val, double factor);

mat eye();
mat rotx(double angle);
mat roty(double angle);
mat rotz(double angle);
mat rotk(double angle, vec axis);

vec rdot2w(mat rdot);

mat skew(vec m);
vec unskew(mat M);

#endif
