#include "math.hxx"

double sign(double val)
{
	if (val > 0) {
		return 1;
	} else if (val < 0) {
		return -1;
	} else {
		return 0;
	}
}

double roundto(double val, double factor)
{
	return ((double) round(val/factor))*factor;
}

mat eye()
{
	mat r(3,3);
	
	r << 1 << 0 << 0 << endr
	  << 0 << 1 << 0 << endr
	  << 0 << 0 << 1 << endr;
	  
	return r;
}

mat rotx(double angle)
{
	mat r(3,3);
	
	r << 1 << 0 		 << 0 			<< endr
	  << 0 << cos(angle) << -sin(angle) << endr
	  << 0 << sin(angle) << cos(angle)  << endr;
	
	return r;
}

mat roty(double angle)
{
	mat r(3,3);
	
	r << cos(angle)  << 0 << sin(angle) << endr
	  << 0 			 << 1 << 0		    << endr
	  << -sin(angle) << 0 << cos(angle) << endr;
	
	return r;
}

mat rotz(double angle)
{
	mat r(3,3);
	
	r << cos(angle) << -sin(angle) << 0 << endr
	  << sin(angle) << cos(angle)  << 0 << endr
	  << 0          << 0           << 1 << endr;
	
	return r;
}

mat rotk(double angle, vec axis)
{
	mat r(3,3);
	
	mat K(3,3);
	K << 0        << -axis(2) << axis(1)  << endr
	  << axis(2)  << 0        << -axis(0) << endr
	  << -axis(1) << axis(0)  << 0        << endr;
	
	r = eye<mat>(3,3) + sin(angle)*K + (1 - cos(angle))*K*K;
	
	return r;
}

vec rdot2w(mat rdot)
{
	vec w(3), k(3);
	
	double angle = acos((rdot(0,0) + rdot(1,1) + rdot(2,2) - 1)/2);
	double sqrt_term = sqrt(pow(rdot(2,1) - rdot(1,2),2) + pow(rdot(0,2) - rdot(2,0),2) + pow(rdot(1,0) - rdot(0,1),2));
	
	k << (rdot(2,1) - rdot(1,2)) / sqrt_term << endr
	  << (rdot(0,2) - rdot(2,0)) / sqrt_term << endr
	  << (rdot(1,0) - rdot(0,1)) / sqrt_term << endr;
	
	w = angle * k;
	
	return w;
}

mat skew(vec m)
{
	mat M (3, 3);
	
	M << 0 << -m(2) << m(1)		<< endr
	  << m(2) << 0 << -m(0)	<< endr
	  << -m(1) << m(0) << 0 << endr;
	
	return M;
}

vec unskew(mat M)
{
	vec m (3);
	
	m << M(2,1) << endr
	  << M(0,2) << endr
	  << M(1,0) << endr;
	
	return m;
}
