CXX:= g++

EXE := robotsim

LIBS := -larmadillo -losg -losgDB -losgFX -losgGA -losgParticle -losgSim -losgText -losgUtil -losgTerrain -losgManipulator -losgViewer -losgWidget -losgShadow -losgAnimation -losgVolume -lOpenThreads -ldl -lm -llua -l:libdl.so -L/usr/lib/x86_64-linux-gnu -L/usr/lib

FLAGS := -O2 -std=c++11 -Wno-unused-result -Wno-format-security -DARMA_DONT_PRINT_CXX11_WARNING

INTG_SRC := src/integrator/RK4.cxx
INTG_OBJ := integrator.o

PARS_SRC := src/parser/parser.cxx
PARS_OBJ := parser.o

NERD_SRC :=	src/integrator/NewtonEuler.cxx
NERD_OBJ := newton_euler.o

DYNM_SRC := model/dynamics.cxx
DYNM_OBJ := dynamics.o

GRFX_SRC := src/graphics/graphics.cxx
GRFX_OBJ := graphics.o

CTLR_SRC := model/controller.cxx
CTLR_OBJ := controller.o

CTLH_SRC := src/controller/controller.cxx
CTLH_OBJ := ctl_helper.o

MATH_SRC := src/math/math.cxx
MATH_OBJ := math.o

INTP_SRC := src/interpreter/lua.cxx
INTP_OBJ := interpreter.o

PROP_SRC := src/propertytree/propertytree.cxx
PROP_OBJ := propertytree.o

FK_SRC := src/fk/fk.cxx
FK_OBJ := fk.o

INPT_SRC := src/inputs/joystick.cxx
INPT_OBJ := inputs.o

DERV_SRC := model/derivatives.cxx
DERV_OBJ := derivatives.o

MAIN_SRC := src/main.cxx
MAIN_OBJ := main.o

DYN_RCXX := $(wildcard dyn/*.cxx)
DYN_ROBJ := $(addprefix obj/,$(notdir $(DYN_RCXX:.cxx=.o)))

EXE_DEP := $(MAIN_OBJ) $(INTG_OBJ) $(PARS_OBJ) $(DYNM_OBJ) $(GRFX_OBJ) $(CTLR_OBJ) $(CTLH_OBJ) $(MATH_OBJ) $(INTP_OBJ) $(FK_OBJ) $(PROP_OBJ) $(DERV_OBJ) $(NERD_OBJ) # $(DYN_ROBJ) # $(INPT_OBJ)

all: $(EXE)

$(EXE): $(EXE_DEP)
	$(CXX) $(EXE_DEP) -o $(EXE) $(LIBS) $(FLAGS)

# Comment if not simulating P3D
# obj/%.o: dyn/%.cxx
# 	$(CXX) -c $< -o $@ $(LIBS) $(FLAGS)

$(MAIN_OBJ): $(MAIN_SRC)
	$(CXX) -o $(MAIN_OBJ) -c $(MAIN_SRC) $(LIBS) $(FLAGS)

$(INTG_OBJ): $(INTG_SRC)
	$(CXX) -o $(INTG_OBJ) -c $(INTG_SRC) $(LIBS) $(FLAGS)

$(DYNM_OBJ): $(DYNM_SRC)
	$(CXX) -o $(DYNM_OBJ) -c $(DYNM_SRC) $(LIBS) $(FLAGS)

$(PARS_OBJ): $(PARS_SRC)
	$(CXX) -o $(PARS_OBJ) -c $(PARS_SRC) $(LIBS) $(FLAGS)

$(GRFX_OBJ): $(GRFX_SRC)
	$(CXX) -o $(GRFX_OBJ) -c $(GRFX_SRC) $(LIBS) $(FLAGS)

$(CTLR_OBJ): $(CTLR_SRC)
	$(CXX) -o $(CTLR_OBJ) -c $(CTLR_SRC) $(LIBS) $(FLAGS)

$(CTLH_OBJ): $(CTLH_SRC)
	$(CXX) -o $(CTLH_OBJ) -c $(CTLH_SRC) $(LIBS) $(FLAGS)
	
$(MATH_OBJ): $(MATH_SRC)
	$(CXX) -o $(MATH_OBJ) -c $(MATH_SRC) $(LIBS) $(FLAGS)
	
$(INTP_OBJ): $(INTP_SRC)
	$(CXX) -o $(INTP_OBJ) -c $(INTP_SRC) $(LIBS) $(FLAGS)
	
$(FK_OBJ): $(FK_SRC)
	$(CXX) -o $(FK_OBJ) -c $(FK_SRC) $(LIBS) $(FLAGS)

$(NERD_OBJ): $(NERD_SRC)
	$(CXX) -o $(NERD_OBJ) -c $(NERD_SRC) $(LIBS) $(FLAGS)

$(PROP_OBJ): $(PROP_SRC)
	$(CXX) -o $(PROP_OBJ) -c $(PROP_SRC) $(LIBS) $(FLAGS)

$(DERV_OBJ): $(DERV_SRC)
	$(CXX) -o $(DERV_OBJ) -c $(DERV_SRC) $(LIBS) $(FLAGS)

# $(INPT_OBJ): $(INPT_SRC)
# 	$(CXX) -o $(INPT_OBJ) -c $(INPT_SRC) $(LIBS) $(FLAGS)

clean:
	rm -rf *.o *.csv
