-- Initialize Robot Controller

t = 0 -- initialize time for time-varying control

t0 = 0;

Kp = 1;
Ki = 0;
Kd = 0;

Fx_ctlr = pid_init(Kp, Ki, Kd)
Fy_ctlr = pid_init(Kp, Ki, Kd)
Fz_ctlr = pid_init(Kp, Ki, Kd)
Tphi_ctlr = pid_init(Kp, Ki, Kd)
Ttheta_ctlr = pid_init(Kp, Ki, Kd)
Tpsi_ctlr = pid_init(Kp, Ki, Kd)
