if init then
	plot_data_ft = io.open("plot/data/ft_p3d.dat", "w")
end

t = getprop("/sim/time/elapsed")

if t >= 0 then
	
	Fx = getprop("/dynamics/params/link8_k")*getprop("/dynamics/gamma/dx");
	Fy = getprop("/dynamics/params/link9_k")*getprop("/dynamics/gamma/dy");
	Fz = getprop("/dynamics/params/link10_k")*getprop("/dynamics/gamma/dz");
	Tphi = getprop("/dynamics/params/link11_k")*getprop("/dynamics/gamma/dphi");
	Ttheta = getprop("/dynamics/params/link12_k")*getprop("/dynamics/gamma/dtheta");
	Tpsi = getprop("/dynamics/params/link13_k")*getprop("/dynamics/gamma/dpsi");
	
	io.output(plot_data_ft)
	io.write(t, "\t", Fx, "\t", Fy, "\t", Fz, "\t", Tphi, "\t", Ttheta, "\t", Tpsi, "\n")
	io.flush()

end
