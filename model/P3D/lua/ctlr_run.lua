t = t + dt;

Fx = getprop("/dynamics/params/link8_k")*X_dx;
Fy = getprop("/dynamics/params/link9_k")*X_dy;
Fz = getprop("/dynamics/params/link10_k")*X_dz;
Tphi = getprop("/dynamics/params/link11_k")*X_dphi;
Ttheta = getprop("/dynamics/params/link12_k")*X_dtheta;
Tpsi = getprop("/dynamics/params/link13_k")*X_dpsi;

--print(string.format("Fx = %.2fN\tFy = %.2fN\tFz = %.2fN\tTphi = %.6fNm\tTtheta = %.6fNm\tTpsi = %.6fNm\n", Fx, Fy, Fz, Tphi, Ttheta, Tpsi));

U_Fx = pid_run(Fx_ctlr, dt, Fx);
U_Fy = pid_run(Fy_ctlr, dt, Fy);
U_Fz = pid_run(Fz_ctlr, dt, Fz);
U_Tphi = pid_run(Tphi_ctlr, dt, Tphi);
U_Ttheta = pid_run(Ttheta_ctlr, dt, Ttheta);
U_Tpsi = pid_run(Tpsi_ctlr, dt, Tpsi);

-- SR Control Inputs

if t > t0 then
	U_F1 = 0.2 * math.cos(4*(t-t0))
	U_F2 = 0.2 * math.cos(4*(t-t0))
	U_F3 = 0.2 * math.cos(4*(t-t0))
	U_F4 = 0.2 * math.cos(4*(t-t0))
	U_F5 = 0.2 * math.cos(4*(t-t0))
	U_F6 = 0.2 * math.cos(4*(t-t0))
end
