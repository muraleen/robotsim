--[[ For all lua scripts defined in the SCRIPTS path variable of the model file,
the global variable 'init' is set to true on the first call. Then, the variable
value is false. Use this to control loops instead of an infinite while ]]

blink = 60;

if init then
	k = 0
else
	
	-- Eyelids
	if k % blink == 0 then
		setprop('/sawyer/eyelids-norm', 0.33);
	elseif k % blink == 1 then
		setprop('/sawyer/eyelids-norm', 0.67);
	elseif k % blink == 2 then
		setprop('/sawyer/eyelids-norm', 1.00);
	elseif k % blink == 3 then
		setprop('/sawyer/eyelids-norm', 0.67);
	elseif k % blink == 4 then
		setprop('/sawyer/eyelids-norm', 0.33);
	else
		setprop('/sawyer/eyelids-norm', 0);
	end
	
	setprop('/sawyer/eyeballs-horiz', math.cos(k*0.2));
	setprop('/sawyer/eyeballs-vert', math.sin(k*0.2));
	
	k = k + 1
end
