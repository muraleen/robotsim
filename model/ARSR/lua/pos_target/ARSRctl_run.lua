--[[
>> ARSR Control System

> Sampling interval is pre-loaded into global space as dt

> Generalized co-ordinates are pre-loaded into global space as X_<var> and
previous inputs are pre-loaded into global as U_<var>

> The computed system inputs must be written back into the U_<var> variables

> Generalized co-ordinates:
	X_x
	X_y
	X_psi
	X_thMW
	X_th1
	X_th2
	X_th3
	X_th4

> Input Variables: < REPLACE with computed variables
	U_F1
	U_F2
	U_F3
	U_F4
	U_F5
	U_F6
	U_F7
	U_F8
	U_Vmw
	U_V1
	U_V2
	U_V3
	U_V4

> IMPORTANT! - make sure you call set_inputs() after inputs are generated
]]

-- IMPLEMENT CONTROLLER HERE

u_des = saturate(pid_run(xvel_ctlr, dt, x_des - X_x), -0.25, 0.25)
v_des = saturate(pid_run(yvel_ctlr, dt, y_des - X_y), -0.25, 0.25)

u = (X_x - x_prev) / dt
v = (X_y - y_prev) / dt

x_prev = X_x
y_prev = X_y

--io.write(string.format("x: %.4f\ty: %.4f\tu: %.4f\tv: %.4f\n", X_x, X_y, u, v))

IFx = pid_run(xpos_ctlr, dt, u_des - u)
IFy = pid_run(ypos_ctlr, dt, v_des - v)
	
Fpsi = saturate(pid_run(psi_ctlr, dt, psi_des - X_psi), -2, 2)

U_V1 = pid_run(link1_ctlr, dt, th1_des - X_th1)
U_V2 = pid_run(link2_ctlr, dt, th2_des - X_th2)
U_V3 = pid_run(link3_ctlr, dt, th3_des - X_th3)
U_V4 = pid_run(link4_ctlr, dt, th4_des - X_th4)


bFx = saturate(IFx*math.cos(X_psi) + IFy*math.sin(X_psi), -1, 1)
bFy = saturate(IFy*math.cos(X_psi) - IFx*math.sin(X_psi), -1, 1)

U_F1 = saturate(Fpsi - bFy, 0, 1)
U_F2 = saturate(-Fpsi - bFy, 0, 1)
U_F3 = saturate(Fpsi - bFx, 0, 1)
U_F4 = saturate(-Fpsi - bFx, 0, 1)
U_F5 = saturate(Fpsi + bFy, 0, 1)
U_F6 = saturate(-Fpsi + bFy, 0, 1)
U_F7 = saturate(Fpsi + bFx, 0, 1)
U_F8 = saturate(-Fpsi + bFx, 0, 1)
