% THRUSTERS

freq = 8; % 2 Hz steps
tsim = 20; % 20 second simulation

nrows = 8; % 8 thrusters
ncols = freq * tsim;

n = rand(nrows, ncols);

for ii = 1:nrows
	row = n(ii,:);
	
	rowStr = ['set_F' num2str(ii) ' = {' num2str(row(1))];
	
	for jj = 2:ncols
		rowStr = [rowStr ', ' num2str(row(jj))];
	end
	
	rowStr = [rowStr '}'];
	
	disp(rowStr);
end
