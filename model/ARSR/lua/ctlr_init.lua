-- Initialize ARSR control system (this script is called once when the simulator starts)

t = 0 -- initialize time for time-varying control

x_prev = 0
y_prev = 0

-- Initialize PID controllers
xvel_ctlr = pid_init(4, 0, 4)
yvel_ctlr = pid_init(4, 0, 4)
xpos_ctlr = pid_init(8, 0, 0.6)
ypos_ctlr = pid_init(8, 0, 0.6)
psi_ctlr = pid_init(2, 0, 6)
link1_ctlr = pid_init(4, 0, 1)
link2_ctlr = pid_init(4, 0, 1)
link3_ctlr = pid_init(4, 0, 1)
link4_ctlr = pid_init(4, 0, 1)
mw_ctlr = pid_init(64,0,8)
