--[[ For all lua scripts defined in the SCRIPTS path variable of the model file,
the global variable 'init' is set to true on the first call. Then, the variable
value is false. Use this to control loops instead of an infinite while ]]

if init then
	io.write("Initializing ARSR Systems...\n")
	cm_model = gfx_load3d('data/3d/cm_marker.ac')
end

cm_x = getprop('/dynamics/center-of-mass/x')
cm_y = getprop('/dynamics/center-of-mass/y')
cm_z = getprop('/dynamics/center-of-mass/z')

gfx_setTrans(cm_model, cm_x, cm_y, cm_z)
--io.write("CoM: x = ", cm_x, " \ty = ", cm_y, " \tz = ", cm_z, "\n")

h_x = getprop('/dynamics/angular-momentum/x')
h_y = getprop('/dynamics/angular-momentum/y')
h_z = getprop('/dynamics/angular-momentum/z')

--io.write("h: x = ", h_x, "\ty = ", h_y, "\tz = ", h_z, "\n")
