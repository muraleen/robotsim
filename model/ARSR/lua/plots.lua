if init then
	plotData = io.open("plot/data/pose_ARSR.dat", "w")
	linkAngle = io.open("plot/data/links_ARSR.dat", "w")
	-- pfile = io.open("plot/data/plot_p.dat", "w")
	-- hfile = io.open("plot/data/plot_h.dat", "w")
end

t = getprop("/sim/time/elapsed")

if t >= 0 then

	--[[p_x = getprop("/dynamics/linear-momentum/x")
	p_y = getprop("/dynamics/linear-momentum/y")
	p_z = getprop("/dynamics/linear-momentum/z")

	io.output(pfile)
	io.write(t, "\t", p_x, "\t", p_y, "\t", p_z, "\n")
	io.flush()
	
	h_x = getprop("/dynamics/angular-momentum/x")
	h_y = getprop("/dynamics/angular-momentum/y")
	h_z = getprop("/dynamics/angular-momentum/z")

	io.output(hfile)
	io.write(t, "\t", h_x, "\t", h_y, "\t", h_z, "\n")
	io.flush() ]]
	
	x = getprop("/dynamics/gamma/x")
	y = getprop("/dynamics/gamma/y")
	psi = getprop("/dynamics/gamma/psi")
	
	io.output(plotData)
	io.write(t, "\t", x, "\t", y, "\t", psi, "\n")
	io.flush()
	
	th1 = getprop("/dynamics/gamma/th1")
	th2 = getprop("/dynamics/gamma/th2")
	th3 = getprop("/dynamics/gamma/th3")
	th4 = getprop("/dynamics/gamma/th4")
	
	io.output(linkAngle)
	io.write(t, "\t", th1, "\t", th2, "\t", th3, "\t", th4, "\n")
	io.flush()

end
