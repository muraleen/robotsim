io.write(getprop("/keyboard/key"), "\n")

if getprop("/keyboard/state") == 1 then

	key = getprop("/keyboard/key")
	
	if key == 113 then
		U_V1 = 0
		U_V2 = 0
		U_V3 = 0
		U_V4 = -0.5
	elseif key == 97 then
		U_V1 = 0
		U_V2 = 0
		U_V3 = 0
		U_V4 = 0.5
	elseif key == 119 then
		U_V1 = 0
		U_V2 = 0
		U_V3 = -0.5
		U_V4 = 0
	elseif key == 115 then
		U_V1 = 0
		U_V2 = 0
		U_V3 = 0.5
		U_V4 = 0
	elseif key == 101 then
		U_V1 = 0
		U_V2 = -0.8
		U_V3 = 0
		U_V4 = 0
	elseif key == 100 then
		U_V1 = 0
		U_V2 = 0.8
		U_V3 = 0
		U_V4 = 0
	elseif key == 114 then
		U_V1 = -1
		U_V2 = 0
		U_V3 = 0
		U_V4 = 0
	elseif key == 102 then
		U_V1 = 1
		U_V2 = 0
		U_V3 = 0
		U_V4 = 0
	elseif key == 116 then
		U_V1 = 0
		U_V2 = 0
		U_V3 = 0
		U_V4 = 0
	elseif key == 65438 then -- ROTATE LEFT
		U_F1 = 1
		U_F2 = 0
		U_F3 = 1
		U_F4 = 0
		U_F5 = 1
		U_F6 = 0
		U_F7 = 1
		U_F8 = 0
	elseif key == 65421 then -- ROTATE RIGHT
		U_F1 = 0
		U_F2 = 1
		U_F3 = 0
		U_F4 = 1
		U_F5 = 0
		U_F6 = 1
		U_F7 = 0
		U_F8 = 1
	elseif key == 65361 or key == 65430 then -- LEFT
		U_F1 = 0
		U_F2 = 0
		U_F3 = 0
		U_F4 = 0
		U_F5 = 0
		U_F6 = 0
		U_F7 = 1
		U_F8 = 1
	elseif key == 65362 or key == 65431 then -- UP
		U_F1 = 1
		U_F2 = 1
		U_F3 = 0
		U_F4 = 0
		U_F5 = 0
		U_F6 = 0
		U_F7 = 0
		U_F8 = 0
	elseif key == 65363 or key == 65432 then -- RIGHT
		U_F1 = 0
		U_F2 = 0
		U_F3 = 1
		U_F4 = 1
		U_F5 = 0
		U_F6 = 0
		U_F7 = 0
		U_F8 = 0
	elseif key == 65364 or key == 65433 then -- DOWN
		U_F1 = 0
		U_F2 = 0
		U_F3 = 0
		U_F4 = 0
		U_F5 = 1
		U_F6 = 1
		U_F7 = 0
		U_F8 = 0
	end
else
	U_F1 = 0
	U_F2 = 0
	U_F3 = 0
	U_F4 = 0
	U_F5 = 0
	U_F6 = 0
	U_F7 = 0
	U_F8 = 0
	U_V1 = 0
	U_V2 = 0
	U_V3 = 0
	U_V4 = 0
end
