% VOLTAGES

freq = 8; % 2 Hz steps
tsim = 20; % 20 second simulation

nrows = 5; % 5 inputs
ncols = freq * tsim;

n = (2*rand(nrows, ncols)) - 1;

for ii = 1:nrows
	row = n(ii,:);
	
	rowStr = ['set_V' num2str(ii) ' = {' num2str(row(1))];
	
	for jj = 2:ncols
		rowStr = [rowStr ', ' num2str(row(jj))];
	end
	
	rowStr = [rowStr '}'];
	
	disp(rowStr);
end
