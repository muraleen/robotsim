--[[
>> ARSR Control System

> Sampling interval is pre-loaded into global space as dt

> Generalized co-ordinates are pre-loaded into global space as X_<var> and
previous inputs are pre-loaded into global as U_<var>

> The computed system inputs must be written back into the U_<var> variables

> Generalized co-ordinates:
	X_x
	X_y
	X_psi
	X_thMW
	X_th1
	X_th2
	X_th3
	X_th4

> Input Variables: < REPLACE with computed variables
	U_F1
	U_F2
	U_F3
	U_F4
	U_F5
	U_F6
	U_F7
	U_F8
	U_Vmw
	U_V1
	U_V2
	U_V3
	U_V4

> IMPORTANT! - make sure you call set_inputs() after inputs are generated
]]

-- IMPLEMENT CONTROLLER HERE
t = t + dt

if ctlSet < 32 then -- sinusoidal voltage inputs

	-- Control Input Frequencies
	Ufreq = {0.5, 4}

	Vmw_freq = Ufreq[(math.floor(ctlSet / 16) % 2) + 1]
	V1_freq = Ufreq[(math.floor(ctlSet / 8) % 2) + 1]
	V2_freq = Ufreq[(math.floor(ctlSet / 4) % 2) + 1]
	V3_freq = Ufreq[(math.floor(ctlSet / 2) % 2) + 1]
	V4_freq = Ufreq[(ctlSet % 2) + 1]

	-- Control Input Magnitudes [Vmw, V1, V2, V3, V4]
	Umag = {-24, 1, 2, 2, 1};

	U_Vmw = Umag[1] * math.cos(Vmw_freq*t)
	U_V1 = Umag[2] * math.cos(V1_freq*t)
	U_V2 = Umag[3] * math.cos(V2_freq*t)
	U_V3 = Umag[4] * math.cos(V3_freq*t)
	U_V4 = Umag[5] * math.cos(V4_freq*t)
	
	U_F1 = 0
	U_F2 = 0
	U_F3 = 0
	U_F4 = 0
	U_F5 = 0
	U_F6 = 0
	U_F7 = 0
	U_F8 = 0
	
elseif ctlSet > 31 and ctlSet < 40 then

	U_Vmw = 0
	U_V1 = 0
	U_V2 = 0
	U_V3 = 0
	U_V4 = 0

	if ctlSet == 32 then -- sinusoidal thruster inputs

		U_F1 = saturate(math.cos(0.5*t),0,1)
		U_F2 = saturate(-math.cos(0.5*t),0,1)
		U_F3 = saturate(math.cos(0.5*t),0,1)
		U_F4 = saturate(-math.cos(0.5*t),0,1)
		U_F5 = saturate(math.cos(0.5*t),0,1)
		U_F6 = saturate(-math.cos(0.5*t),0,1)
		U_F7 = saturate(math.cos(0.5*t),0,1)
		U_F8 = saturate(-math.cos(0.5*t),0,1)
	
	elseif ctlSet == 33 then -- sinusoidal thruster inputs

		U_F1 = saturate(math.cos(2*t),0,1)
		U_F2 = saturate(-math.cos(2*t),0,1)
		U_F3 = saturate(math.cos(2*t),0,1)
		U_F4 = saturate(-math.cos(2*t),0,1)
		U_F5 = saturate(math.cos(2*t),0,1)
		U_F6 = saturate(-math.cos(2*t),0,1)
		U_F7 = saturate(math.cos(2*t),0,1)
		U_F8 = saturate(-math.cos(2*t),0,1)
	
	elseif ctlSet == 34 then -- sinusoidal thruster inputs

		U_F1 = saturate(math.cos(4*t),0,1)
		U_F2 = saturate(-math.cos(4*t),0,1)
		U_F3 = saturate(math.cos(4*t),0,1)
		U_F4 = saturate(-math.cos(4*t),0,1)
		U_F5 = saturate(math.cos(4*t),0,1)
		U_F6 = saturate(-math.cos(4*t),0,1)
		U_F7 = saturate(math.cos(4*t),0,1)
		U_F8 = saturate(-math.cos(4*t),0,1)
	
	elseif ctlSet == 35 then -- sinusoidal thruster inputs

		U_F1 = math.abs(math.cos(2*t))
		U_F2 = math.abs(math.sin(2*t))
		U_F3 = math.abs(math.cos(2*t))
		U_F4 = math.abs(math.sin(2*t))
		U_F5 = math.abs(math.cos(2*t))
		U_F6 = math.abs(math.sin(2*t))
		U_F7 = math.abs(math.cos(2*t))
		U_F8 = math.abs(math.sin(2*t))
	
	elseif ctlSet == 36 then
	
		U_F1 = math.abs(math.cos(2*t))
		U_F2 = 0
		U_F3 = math.abs(math.cos(2*t))
		U_F4 = 0
		U_F5 = 0
		U_F6 = math.abs(math.sin(2*t))
		U_F7 = 0
		U_F8 = math.abs(math.sin(2*t))
		
	elseif ctlSet == 37 then
	
		U_F1 = math.abs(math.cos(2*t))
		U_F2 = math.abs(math.cos(2*t))
		U_F3 = 0
		U_F4 = 0
		U_F5 = math.abs(math.sin(2*t))
		U_F6 = math.abs(math.sin(2*t))
		U_F7 = 0
		U_F8 = 0
	
	elseif ctlSet == 38 then
	
		U_F3 = math.abs(math.cos(2*t))
		U_F4 = math.abs(math.cos(2*t))
		U_F1 = 0
		U_F2 = 0
		U_F7 = math.abs(math.sin(2*t))
		U_F8 = math.abs(math.sin(2*t))
		U_F5 = 0
		U_F6 = 0
	
	elseif ctlSet == 39 then
	
		U_F1 = math.abs(math.cos(0.5*t))
		U_F2 = math.abs(math.cos(4*t))
		U_F3 = 0
		U_F4 = 0
		U_F5 = math.abs(math.sin(0.5*t))
		U_F6 = math.abs(math.sin(4*t))
		U_F7 = 0.02
		U_F8 = 0.02
	
	end

else -- 40 -> 45

	if ctlSet == 40 then

		-- Desired joint variables
		x_des = 0.5
		y_des = -0.2
		psi_des = 0.75*math.pi
		th1_des = math.pi/6
		th2_des = -math.pi/6
		th3_des = -math.pi/6
		th4_des = -math.pi/4
		
	elseif ctlSet == 41 then
	
		x_des = 0
		y_des = 0
		psi_des = 0.75*math.pi
		th1_des = math.pi/6
		th2_des = -math.pi/6
		th3_des = -math.pi/6
		th4_des = -math.pi/4
	
	elseif ctlSet == 42 then
	
		x_des = 0
		y_des = 0
		psi_des = 0
		th1_des = math.pi/6
		th2_des = -math.pi/6
		th3_des = -math.pi/6
		th4_des = -math.pi/4
	
	elseif ctlSet == 43 then
	
		x_des = 0
		y_des = 0
		psi_des = 0
		th1_des = -math.pi/6
		th2_des = math.pi/6
		th3_des = math.pi/6
		th4_des = math.pi/4
	
	elseif ctlSet == 44 then
	
		x_des = 0
		y_des = 0
		psi_des = 0
		th1_des = math.pi/4
		th2_des = -math.pi/2
		th3_des = -math.pi/2
		th4_des = -math.pi/2
	
	elseif ctlSet == 45 then
	
		x_des = 0
		y_des = 0
		psi_des = math.pi
		th1_des = math.pi/4
		th2_des = -math.pi/2
		th3_des = -math.pi/2
		th4_des = -math.pi/2
		
	elseif ctlSet == 46 then
	
		th1_des = math.pi/4
		th2_des = -math.pi/2
		th3_des = -math.pi/2
		th4_des = -math.pi/2
	
	elseif ctlSet == 47 then

		th1_des = math.pi/6
		th2_des = -math.pi/6
		th3_des = -math.pi/6
		th4_des = -math.pi/4
	
	elseif ctlSet == 48 then

		psi_des = math.pi
		th1_des = 0
		th2_des = 0
		th3_des = 0
		th4_des = 0
	
	elseif ctlSet == 49 then

		psi_des = math.pi/6
		th1_des = math.pi/6
		th2_des = -math.pi/6
		th3_des = -math.pi/6
		th4_des = -math.pi/4
	
	elseif ctlSet == 50 then

		psi_des = math.pi/4
		th1_des = math.pi/4
		th2_des = -math.pi/2
		th3_des = -math.pi/2
		th4_des = -math.pi/2
	
	elseif ctlSet == 51 then

		psi_des = -math.pi/4
		th1_des = math.pi/4
		th2_des = -math.pi/2
		th3_des = -math.pi/2
		th4_des = -math.pi/2
	
	elseif ctlSet == 52 then

		psi_des = 0
		th1_des = math.pi/4
		th2_des = -math.pi/2
		th3_des = -math.pi/2
		th4_des = -math.pi/2
	
	elseif ctlSet == 53 then

		psi_des = 0
		th1_des = -math.pi/4
		th2_des = -math.pi/4
		th3_des = -math.pi/4
		th4_des = -math.pi/4
		
	elseif ctlSet == 54 then

		psi_des = math.pi/2
		th1_des = -math.pi/3
		th2_des = -math.pi/3
		th3_des = -math.pi/3
		th4_des = -math.pi/2
	
	end
	
	if ctlSet < 46 then
	
		u_des = saturate(pid_run(xvel_ctlr, dt, x_des - X_x), -0.25, 0.25)
		v_des = saturate(pid_run(yvel_ctlr, dt, y_des - X_y), -0.25, 0.25)

		u = (X_x - x_prev) / dt
		v = (X_y - y_prev) / dt

		x_prev = X_x
		y_prev = X_y

		--io.write(string.format("x: %.4f\ty: %.4f\tu: %.4f\tv: %.4f\n", X_x, X_y, u, v))

		IFx = pid_run(xpos_ctlr, dt, u_des - u)
		IFy = pid_run(ypos_ctlr, dt, v_des - v)
	
		Fpsi = saturate(pid_run(psi_ctlr, dt, psi_des - X_psi), -2, 2)

		bFx = saturate(IFx*math.cos(X_psi) + IFy*math.sin(X_psi), -1, 1)
		bFy = saturate(IFy*math.cos(X_psi) - IFx*math.sin(X_psi), -1, 1)

		U_F1 = saturate(Fpsi - bFy, 0, 1)
		U_F2 = saturate(-Fpsi - bFy, 0, 1)
		U_F3 = saturate(Fpsi - bFx, 0, 1)
		U_F4 = saturate(-Fpsi - bFx, 0, 1)
		U_F5 = saturate(Fpsi + bFy, 0, 1)
		U_F6 = saturate(-Fpsi + bFy, 0, 1)
		U_F7 = saturate(Fpsi + bFx, 0, 1)
		U_F8 = saturate(-Fpsi + bFx, 0, 1)
	
	elseif ctlSet > 47 then
	
		U_Vmw = saturate(pid_run(mw_ctlr, dt, X_psi - psi_des),-50,50)
	
	end

	U_V1 = saturate(pid_run(link1_ctlr, dt, th1_des - X_th1),-12,12)
	U_V2 = saturate(pid_run(link2_ctlr, dt, th2_des - X_th2),-12,12)
	U_V3 = saturate(pid_run(link3_ctlr, dt, th3_des - X_th3),-12,12)
	U_V4 = saturate(pid_run(link4_ctlr, dt, th4_des - X_th4),-12,12)

end

Fx = getprop("/dynamics/params/FTsensor_Kx")*X_dx;
Fy = getprop("/dynamics/params/FTsensor_Ky")*X_dy;
Tpsi = getprop("/dynamics/params/FTsensor_Kpsi")*X_dpsi;

U_Vx = saturate(pid_run(Vx_ctlr, dt, Fx*math.cos(X_psi+X_dpsi) - Fy*math.sin(X_psi+X_dpsi)), -12, 12);
U_Vy = saturate(pid_run(Vy_ctlr, dt, Fy*math.cos(X_psi+X_dpsi) + Fx*math.sin(X_psi+X_dpsi)), -12, 12);
U_Vpsi = saturate(pid_run(Vpsi_ctlr, dt, Tpsi), -24, 24);
