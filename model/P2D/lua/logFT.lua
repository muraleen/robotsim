Fx = getprop("/dynamics/params/FTsensor_Kx")*getprop("/dynamics/gamma/dx")
Fy = getprop("/dynamics/params/FTsensor_Ky")*getprop("/dynamics/gamma/dy")
Tz = getprop("/dynamics/params/FTsensor_Kpsi")*getprop("/dynamics/gamma/dpsi")

setprop("/sensors/FTsensor/Fx", Fx);
setprop("/sensors/FTsensor/Fy", Fy);
setprop("/sensors/FTsensor/Tpsi", Tz);
