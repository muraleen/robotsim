--[[
Generalized Co-ordinates:
	X_x
	X_y
	X_psi
	X_dx
	X_dy
	X_dpsi
	X_thMW
	X_th1
	X_th2
	X_th3
	X_th4

Input Variables: PLATFORM
	U_Vx
	U_Vy
	U_Vpsi

Input Variables: ARSR
	U_F1
	U_F2
	U_F3
	U_F4
	U_F5
	U_F6
	U_F7
	U_F8
	U_Vmw
	U_V1
	U_V2
	U_V3
	U_V4
]]

-- Simulate Force/Torque Sensor (Load Cell) Outputs
Fx = getprop("/dynamics/params/FTsensor_Kx")*X_dx;
Fy = getprop("/dynamics/params/FTsensor_Ky")*X_dy;
Tpsi = getprop("/dynamics/params/FTsensor_Kpsi")*X_dpsi;

-- PLATFORM FORCE-NULLIFYING CONTROLLER
U_Vx = saturate(pid_run(xpos_ctlr, dt, Fx*math.cos(X_psi+X_dpsi) - Fy*math.sin(X_psi+X_dpsi)), -12, 12)
U_Vy = saturate(pid_run(ypos_ctlr, dt, Fy*math.cos(X_psi+X_dpsi) + Fx*math.sin(X_psi+X_dpsi)), -12, 12)
U_Vpsi = saturate(pid_run(psi_ctlr, dt, Tpsi), -24, 24)

-- ARSR CONTROLLER

t = t + dt

U_Vmw = Umag[1] * math.cos(Vmw_freq*t)
U_V1 = Umag[2] * math.cos(V1_freq*t)
U_V2 = Umag[3] * math.cos(V2_freq*t)
U_V3 = Umag[4] * math.cos(V3_freq*t)
U_V4 = Umag[5] * math.cos(V4_freq*t)

--[[U_Vmw = 12*math.cos(t/2)
U_V3 = math.cos(t)
U_V4 = math.cos(2*t)

t = t + dt]]
