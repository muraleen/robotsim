if init then
	plotData = io.open("plot/data/pose_P2D.dat", "w")
	FTsensor = io.open("plot/data/P2D_FTsensor.dat", "w")
	poseError = io.open("plot/data/P2D_poseErr.dat", "w")
	linkAngle = io.open("plot/data/links_P2D.dat", "w")
end

t = getprop("/sim/time/elapsed")

if t >= 0 then

	x = getprop("/dynamics/gamma/x") + getprop("/dynamics/gamma/dx")
	y = getprop("/dynamics/gamma/y") + getprop("/dynamics/gamma/dy")
	psi = getprop("/dynamics/gamma/psi") + getprop("/dynamics/gamma/dpsi")
	
	xD = getprop("/controller/desired/gamma/x")
	yD = getprop("/controller/desired/gamma/y")
	psiD = getprop("/controller/desired/gamma/psi")
	
	io.output(plotData)
	io.write(t, "\t", x, "\t", y, "\t", psi, "\t", xD, "\t", yD, "\t", psiD, "\n")
	io.flush()
	
	io.output(poseError)
	io.write(t, "\t", x - xD, "\t", y - yD, "\t", psi - psiD, "\n")
	io.flush()
	
	Fx = getprop("/dynamics/params/FTsensor_Kx")*getprop("/dynamics/gamma/dx")
	Fy = getprop("/dynamics/params/FTsensor_Ky")*getprop("/dynamics/gamma/dy")
	Tz = getprop("/dynamics/params/FTsensor_Kpsi")*getprop("/dynamics/gamma/dpsi")
	
	io.output(FTsensor)
	io.write(t, "\t", Fx, "\t", Fy, "\t", Tz, "\n")
	io.flush()
	
	th1 = getprop("/dynamics/gamma/th1")
	th2 = getprop("/dynamics/gamma/th2")
	th3 = getprop("/dynamics/gamma/th3")
	th4 = getprop("/dynamics/gamma/th4")
	
	io.output(linkAngle)
	io.write(t, "\t", th1, "\t", th2, "\t", th3, "\t", th4, "\n")
	io.flush()

end
