FT.PE = 0.5*(Kx * dx^2 + Ky*dy^2 + Kz*dz^2 + Kphi*dphi^2 + Ktheta*dtheta^2 + Kpsi*dpsi^2);

J_L_gammad = zeros(1,length(gamma));
J_L_gamma = jacobian(FT.PE, gamma); % Start with PE of FT sensor (not included in frames[])

for f = 1:length(frames)
    J_L_gammad = J_L_gammad + jacobian(frames(f).L, gammad);
    J_L_gamma = J_L_gamma + jacobian(frames(f).L,gamma); 
end

% Damping force
F_FTdamp = [zeros(7,1); bx*dxd; by*dyd; bz*dzd; bphi*dphid; btheta*dthetad; bpsi*dpsid; zeros(6,1)];

H = jacobian(J_L_gammad.', gammad); % Essentially hessian(L, gammad);
N = jacobian(J_L_gammad.', gamma)*gammad - J_L_gamma.' + F_FTdamp;

% Save to file
save H.mat H
save N.mat N