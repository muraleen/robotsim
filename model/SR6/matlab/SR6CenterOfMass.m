syms b_m b_s b_x0 b_z0 ll_m ll_l ll_r sl_m sl_l sl_r
syms q1 q2 q3 q4 q5 q6 q7 th1 th2 th3 th4 th5 th6
syms b_Gx b_Gz sl_Gx ll_Gx GZ

% Rigid bodies: b, l1, l2, l3, l4, l5, l6
%   X.r: relative position between FT sensor and frame X measured in the
%        inertial frame
%   X.T: relative rotation matrix to transform from frame X to inertial
%        frame
%   X.m: mass of frame m
%   X.G: Vector of first mass moments of frame X measured wrt. frame X

ITs = rotz(q1)*roty(q2)*rotx(q3)*roty(q4)*rotz(q5)*roty(q6)*rotz(q7);

b.r = ITs*[0; 0; 0.0733];
b.T = ITs; % Ignore FT sensor deformations
b.m = b_m;
b.G = [b_Gx; 0; b_Gz];

l1.r = b.r + b.T*[b_x0; 0; b_z0];
l1.T = b.T*roty(th1);
l1.m = sl_m;
l1.G = [sl_Gx; 0; 0];

l2.r = l1.r + l1.T*[sl_l; 0; 0];
l2.T = l1.T*rotx(th2);
l2.m = ll_m;
l2.G = [ll_Gx; 0; 0];

l3.r = l2.r + l2.T*[b_x0; 0; b_z0];
l3.T = l2.T*roty(th3);
l3.m = sl_m;
l3.G = [sl_Gx; 0; 0];

l4.r = l3.r + l3.T*[sl_l; 0; 0];
l4.T = l3.T*rotx(th4);
l4.m = ll_m;
l4.G = [ll_Gx; 0; 0];

l5.r = l4.r + l4.T*[b_x0; 0; b_z0];
l5.T = l4.T*roty(th5);
l5.m = sl_m;
l5.G = [sl_Gx; 0; 0];

l6.r = l5.r + l5.T*[sl_l; 0; 0];
l6.T = l5.T*rotx(th6);
l6.m = ll_m;
l6.G = [ll_Gx; 0; 0];

frames = [b; l1; l2; l3; l4; l5; l6];

Sigma_m_cm = 0;

for beta = 1:length(frames)
    Sigma_m_cm = Sigma_m_cm + frames(beta).r * frames(beta).m + frames(beta).T * frames(beta).G;
end

IbrCM_m_g = Sigma_m_cm * GZ;

diary SR6_CM.txt
ccode(IbrCM_m_g)
diary off