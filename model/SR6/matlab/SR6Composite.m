syms H11 H12 H21 H22 D1 D2 J N K i b theta thetad thetadd L V R eta

H = [H11 H12; H21 H22];
D = [D1; D2];

Hhat = diag([zeros(1,1) J*N*ones(1,1)]) + H/(eta*N)

Nhat = [zeros(1,1); K*i] - [zeros(1,1); b*N*thetad] - D/(eta*N)

id = V/L - i*R/L - K*N*thetad/L