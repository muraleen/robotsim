%% Frames:
%   1:  sawyer 1
frames(1).r = [0; 0; 0.08];
frames(1).T = rotz(q1);
frames(1).m = 1;
frames(1).cm = [0.0180;    0;  0.2380] - [0;   0;  0.08];

%   2:  sawyer 2
frames(2).r = [0.0755; 0.0614; 0.3162-0.08];
frames(2).T = roty(q2);
frames(2).m = 0.5;
frames(2).cm = [0.1100;    0.1510;  0.32] - [0.085;   0.062;  0.32];

%   3:  sawyer 3
frames(3).r = [0.1134; 0.1840; 0];
frames(3).T = rotx(q3);
frames(3).m = 0.5;
frames(3).cm = [0.3000;    0.1850;  0.32] - [0.222;   0.1850;  0.32];

%   4:  sawyer 4
frames(4).r = [0.2912; 0.036; 0];
frames(4).T = roty(q4);
frames(4).m = 0.5;
frames(4).cm = [0.4805;    0.3123;  0.3512] - [0.4805;   0.2365;  0.32];

%   5:  sawyer 5
frames(5).r = [0; 0.1181; 0.1243];
frames(5).T = rotz(q5);
frames(5).m = 0.5;
frames(5).cm = [0.4805;    0.3550;  0.5636] - [0.4805;   0.3550;  0.4441];

%   6:  sawyer 6
frames(6).r = [0; 0.0388; 0.2743];
frames(6).T = roty(q6);
frames(6).m = 0.3;
frames(6).cm = [0.48;    0.4700;  0.7463] - [0.48;   0.40;  0.7136];

%   7:  sawyer 7
frames(7).r = [0; 0.0952; 0.1463];
frames(7).T = rotz(q7);
frames(7).m = 0.2;
frames(7).cm = [0.48;    0.4961;  0.82] - [0.48;   0.48;  0.8];

%   8:  astrobee
frames(8).r = [dx; dy; dz+0.0733];
frames(8).T = rotz(dpsi)*roty(dtheta)*rotx(dphi);
frames(8).m = b_m;
frames(8).G = [b_Gx; 0; b_Gz];
frames(8).J = [b_Jxx, 0, b_Jxz; ...
       0, b_Jyy, 0; ...
       b_Jxz, 0, b_Jzz];

%   9:  SR 1
frames(9).r = [b_x0; 0; b_z0];
frames(9).T = roty(th1);
frames(9).m = sl_m;
frames(9).G = [sl_Gx; 0; 0];
frames(9).J = diag([sl_Jxx, sl_Jyy, sl_Jzz]);

%  10:  SR 2
frames(10).r = [sl_l; 0; 0];
frames(10).T = rotx(th2);
frames(10).m = ll_m;
frames(10).G = [ll_Gx; 0; 0];
frames(10).J = diag([ll_Jxx, ll_Jyy, ll_Jzz]);

%  11:  SR 3
frames(11).r = [ll_l; 0; 0];
frames(11).T = roty(th3);
frames(11).m = sl_m;
frames(11).G = frames(9).G;
frames(11).J = frames(9).J;

%  12:  SR 4
frames(12).r = [sl_l; 0; 0];
frames(12).T = rotx(th4);
frames(12).m = ll_m;
frames(12).G = frames(10).G;
frames(12).J = frames(10).J;

%  13:  SR 5
frames(13).r = [ll_l; 0; 0];
frames(13).T = roty(th5);
frames(13).m = sl_m;
frames(13).G = frames(9).G;
frames(13).J = frames(9).J;

%  14:  SR 6
frames(14).r = [sl_l; 0; 0];
frames(14).T = rotx(th6);
frames(14).m = ll_m;
frames(14).G = frames(10).G;
frames(14).J = frames(10).J;

for f = 1:7
    [frames(f).G, frames(f).J] = DynamicParameters(frames(f).m, frames(f).cm);
end

for f = 1:length(frames)
    if f == 1
        frames(1).IIrf = frames(1).r;
        frames(1).ITf = frames(1).T;
    else
        frames(f).IIrf = simplify(frames(f-1).IIrf + frames(f-1).ITf * frames(f).r);
        frames(f).ITf = simplify(frames(f-1).ITf * frames(f).T);    
    end
    
    IIrfdot = simplify(chainDiff(frames(f).IIrf, vars, dvars));
    ITfdot = simplify(chainDiff(frames(f).ITf, vars, dvars));
    
    frames(f).KE = simplify(KineticEnergy(IIrfdot, frames(f).m, ITfdot, frames(f).G, frames(f).J));
    frames(f).PE = simplify(PotentialEnergy(frames(f).IIrf, frames(f).m, frames(f).ITf, frames(f).G, GZ));
    % frames(f).L = simplify(frames(f).KE - frames(f).PE);
    frames(f).L = frames(f).KE - frames(f).PE;
    
    f
end

% Save to file
save frames_L.mat frames2