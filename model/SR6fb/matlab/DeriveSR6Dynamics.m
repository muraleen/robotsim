syms x y z phi theta psi th1 th2 th3 th4 th5 th6
syms xd yd zd phid thetad psid th1d th2d th3d th4d th5d th6d
syms xdd ydd zdd phidd thetadd psidd th1dd th2dd th3dd th4dd th5dd th6dd
syms th1ddd th2ddd th3ddd th4ddd th5ddd th6ddd

syms b_m b_s b_x0 b_z0 ll_m ll_l ll_r sl_m sl_l sl_r

gamma = [x; y; z; phi; theta; psi; th1; th2; th3; th4; th5; th6];
gammad = [xd; yd; zd; phid; thetad; psid; th1d; th2d; th3d; th4d; th5d; th6d];
gammadd = [xdd; ydd; zdd; phidd; thetadd; psidd; th1dd; th2dd; th3dd; th4dd; th5dd; th6dd];

vars = [gamma; gammad];
dvars = [gammad; gammadd];

% FK: I --> b --> l1 --> l2 --> l3 --> l4 --> l5 --> l6

% Body frame
b.r = [x; y; z];
b.T = rotx(phi)*roty(theta)*rotz(psi);

b.m = b_m;
% b.G = [sl_m*b_x0/2; 0; sl_m*b_z0]./(b_m + sl_m);
% b.J = [b_m*b_s^2/6 + ll_m*ll_r^2/2 + ll_m*b_z0^2, 0, -ll_m*ll_l*b_z0/2; ...
%        0, b_m*b_s^2/6 + ll_m*(3*ll_r^2 + ll_l^2)/12 + ll_m*(b_z0^2 + ll_l^2/4), 0; ...
%        -ll_m*ll_l*b_z0/2, 0, b_m*b_s^2/6 + ll_m*(3*ll_r^2 + ll_l^2)/12 + ll_m*ll_l^2/4];

syms b_Gx b_Gz b_Jxx b_Jyy b_Jzz b_Jxz

b.G = [b_Gx; 0; b_Gz];
b.J = [b_Jxx, 0, b_Jxz; ...
       0, b_Jyy, 0; ...
       b_Jxz, 0, b_Jzz];

IIrb = b.r;
IIrbdot = chainDiff(IIrb, vars, dvars);
ITb = b.T;
ITbdot = chainDiff(ITb, vars, dvars);

b.KE = KineticEnergy(IIrbdot, b_m, ITbdot, b.G, b.J);
b.PE = 0; % No gravity

   
% Link 1
l1.r = [b_x0; 0; b_z0];
l1.T = roty(th1);

l1.m = sl_m;
% l1.G = [sl_l/2; 0; 0];
% l1.J = diag([sl_m*sl_r^2/2, sl_m*(3*sl_m^2 + sl_m^2)/12 + sl_m*sl_r^2/2, sl_m*(3*sl_r^2 + sl_l^2)/12 + sl_m*sl_r^2/2]);

syms sl_Gx sl_Jxx sl_Jyy sl_Jzz
l1.G = [sl_Gx; 0; 0];
l1.J = diag([sl_Jxx, sl_Jyy, sl_Jzz]);

IIr1 = IIrb + ITb * l1.r;
IIr1dot = chainDiff(IIr1, vars, dvars);
IT1 = ITb * l1.T;
IT1dot = chainDiff(IT1, vars, dvars);

l1.KE = KineticEnergy(IIr1dot, l1.m, IT1dot, l1.G, l1.J);
l1.PE = 0; % No gravity


% Link 2
l2.r = [sl_l; 0; 0];
l2.T = rotx(th2);

l2.m = ll_m;
% l2.G = [ll_l/2; 0; 0];
% l2.J = diag([ll_m*ll_r^2/2, ll_m*(3*ll_m^2 + ll_m^2)/12 + ll_m*ll_r^2/2, ll_m*(3*ll_r^2 + ll_l^2)/12 + ll_m*ll_r^2/2]);

syms ll_Gx ll_Jxx ll_Jyy ll_Jzz
l2.G = [ll_Gx; 0; 0];
l2.J = diag([ll_Jxx, ll_Jyy, ll_Jzz]);

IIr2 = IIr1 + IT1 * l2.r;
IIr2dot = chainDiff(IIr2, vars, dvars);
IT2 = IT1 * l2.T;
IT2dot = chainDiff(IT2, vars, dvars);

l2.KE = KineticEnergy(IIr2dot, l2.m, IT2dot, l2.G, l2.J);
l2.PE = 0; % No gravity


% Link 3
l3.r = [ll_l; 0; 0];
l3.T = roty(th3);

l3.m = sl_m;
l3.G = l1.G;
l3.J = l1.J;

IIr3 = IIr2 + IT2 * l3.r;
IIr3dot = chainDiff(IIr3, vars, dvars);
IT3 = IT2 * l3.T;
IT3dot = chainDiff(IT3, vars, dvars);

l3.KE = KineticEnergy(IIr3dot, l3.m, IT3dot, l3.G, l3.J);
l3.PE = 0; % No gravity


% Link 4
l4.r = [sl_l; 0; 0];
l4.T = rotx(th4);

l4.m = ll_m;
l4.G = l2.G;
l4.J = l2.J;

IIr4 = IIr3 + IT3 * l4.r;
IIr4dot = chainDiff(IIr4, vars, dvars);
IT4 = IT3 * l4.T;
IT4dot = chainDiff(IT4, vars, dvars);

l4.KE = KineticEnergy(IIr4dot, l4.m, IT4dot, l4.G, l4.J);
l4.PE = 0; % No gravity


% Link 5
l5.r = [ll_l; 0; 0];
l5.T = roty(th5);

l5.m = sl_m;
l5.G = l1.G;
l5.J = l1.J;

IIr5 = IIr4 + IT4 * l5.r;
IIr5dot = chainDiff(IIr5, vars, dvars);
IT5 = IT4 * l5.T;
IT5dot = chainDiff(IT5, vars, dvars);

l5.KE = KineticEnergy(IIr5dot, l5.m, IT5dot, l5.G, l5.J);
l5.PE = 0; % No gravity


% Link 6
l6.r = [sl_l; 0; 0];
l6.T = rotx(th6);

l6.m = ll_m;
l6.G = l2.G;
l6.J = l2.J;

IIr6 = IIr5 + IT5 * l6.r;
IIr6dot = chainDiff(IIr6, vars, dvars);
IT6 = IT5 * l6.T;
IT6dot = chainDiff(IT6, vars, dvars);

l6.KE = KineticEnergy(IIr6dot, l6.m, IT6dot, l6.G, l6.J);
l6.PE = 0; % No gravity


KE = b.KE + l1.KE + l2.KE + l3.KE + l4.KE + l5.KE + l6.KE;
PE = b.PE + l1.PE + l2.PE + l3.PE + l4.PE + l5.PE + l6.PE;

L = KE - PE;

% dL_dgammad = vectPartial(L, gammad);
% dL_dgamma = vectPartial(L, gamma);

% F = vectChainDiff(dL_dgammad, vars, dvars) - dL_dgamma;

H = jacobian(jacobian(KE, gammad).', gammad);
D = jacobian(jacobian(KE, gammad).', gamma)*gammad - jacobian(KE, gamma).';
% G = simplify(jacobian(PE, gamma).');

% Rm = diag([zeros(1,6) a_R_K*ones(1,6)]);
% 
% a_gammaddd = [zeros(6,1); th1ddd; th2ddd; th3ddd; th4ddd; th5ddd; th6ddd];
% a_gammadd = [zeros(6,1); gammadd(7:12)];
% a_gammad = [zeros(6,1); gammad(7:12)];
% 
% syms V1 V2 V3 V4 V5 V6
% 
% V = [V1; V2; V3; V4; V5; V6];
% 
% C = a_a1*a_gammadd/a_b0 + a_a0*a_gammad/a_b0 + Rm*F;

% Include actuator dynamics
syms a_J a_N a_eta a_K a_b a_L a_R
syms V1 V2 V3 V4 V5 V6
syms i1 i2 i3 i4 i5 i6
syms i1d i2d i3d i4d i5d i6d

i = [i1; i2; i3; i4; i5; i6];
V = [V1; V2; V3; V4; V5; V6];

% Hhat * gammadd = Nhat => gammadd = Hhat \ Nhat
% Hhat = H./(a_eta*a_N) + diag([zeros(1,6) ones(1,6).*a_J./a_N]);
% Nhat = D./(a_eta*a_N) - a_K.*[zeros(6,1); i] - a_b*a_N.*[zeros(6,1); gammad(7:12,1)];
% 
% id = V./a_L - i.*(a_R/a_L) + (a_K*a_N/a_L).*gammad(7:12,1);

Hhat = diag([zeros(1,6) a_J*a_N.*ones(1,6)]) + H./(a_eta*a_N);
Nhat = [zeros(6,1); a_K.*i] - [zeros(6,1); a_b*a_N.*gammad(7:12,1)] - D./(a_eta*a_N);

id = V./a_L - i.*a_R/a_L - a_K*a_N.*gammad(7:12)./a_L;

diary ccode.txt
ccode(Hhat)
ccode(Nhat)
ccode(id)
diary off