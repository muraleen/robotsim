clc; clear all;

syms q1 q2 q3 q4 q5 q6 q7 dx dy dz dphi dtheta dpsi th1 th2 th3 th4 th5 th6
syms q1d q2d q3d q4d q5d q6d q7d dxd dyd dzd dphid dthetad dpsid th1d th2d th3d th4d th5d th6d
syms q1dd q2dd q3dd q4dd q5dd q6dd q7dd dxdd dydd dzdd dphidd dthetadd dpsidd th1dd th2dd th3dd th4dd th5dd th6dd

syms b_m b_s b_x0 b_z0 ll_m ll_l ll_r sl_m sl_l sl_r gz

gamma = [q1 q2 q3 q4 q5 q6 q7 dx dy dz dphi dtheta dpsi th1 th2 th3 th4 th5 th6].';
gammad = [q1d q2d q3d q4d q5d q6d q7d dxd dyd dzd dphid dthetad dpsid th1d th2d th3d th4d th5d th6d].';
gammadd = [q1dd q2dd q3dd q4dd q5dd q6dd q7dd dxdd dydd dzdd dphidd dthetadd dpsidd th1dd th2dd th3dd th4dd th5dd th6dd].';

vars = [gamma; gammad];
dvars = [gammad; gammadd];

% FK: I --> s1 --> s2 --> s3 --> s4 --> s5 --> s6 --> s7 --> b --> l1 -->
% l2 --> l3 --> l4 --> l5 --> l6

% Sawyer Link 1
s1.r = [0; 0; 0.08];
s1.T = rotz(q1);

s1.m = 1;
s1.cm = [0.0180;    0;  0.2380] - [0;   0;  0.08];

[s1.G, s1.J] = DynamicParameters(s1.m, s1.cm);

IIrS1 = s1.r;
IIrS1dot = chainDiff(IIrS1, vars, dvars);
ITS1 = s1.T;
ITS1dot = chainDiff(ITS1, vars, dvars);

s1.KE = KineticEnergy(IIrS1dot, s1.m, ITS1dot, s1.G, s1.J);
s1.PE = PotentialEnergy(IIrS1, s1.m, ITS1, s1.G, gz);


% Sawyer Link 2
s2.r = [0.0755; 0.0614; 0.3162-0.08];
s2.T = roty(q2);

s2.m = 0.5;
s2.cm = [0.1100;    0.1510;  0.32] - [0.085;   0.062;  0.32];

[s2.G, s2.J] = DynamicParameters(s2.m, s2.cm);

IIrS2 = IIrS1 + ITS1*s2.r;
IIrS2dot = chainDiff(IIrS2, vars, dvars);
ITS2 = ITS1 * s2.T;
ITS2dot = chainDiff(ITS2, vars, dvars);

s2.KE = KineticEnergy(IIrS2dot, s2.m, ITS2dot, s2.G, s2.J);
s2.PE = PotentialEnergy(IIrS2, s2.m, ITS2, s2.G, gz);


% Sawyer Link 3
s3.r = [0.1134; 0.1840; 0];
s3.T = rotx(q3);

s3.m = 0.5;
s3.cm = [0.3000;    0.1850;  0.32] - [0.222;   0.1850;  0.32];

[s3.G, s3.J] = DynamicParameters(s3.m, s3.cm);

IIrS3 = IIrS2 + ITS2*s3.r;
IIrS3dot = chainDiff(IIrS3, vars, dvars);
ITS3 = ITS2 * s3.T;
ITS3dot = chainDiff(ITS3, vars, dvars);

s3.KE = KineticEnergy(IIrS3dot, s3.m, ITS3dot, s3.G, s3.J);
s3.PE = PotentialEnergy(IIrS3, s3.m, ITS3, s3.G, gz);

% Sawyer Link 4
s4.r = [0.2912; 0.036; 0];
s4.T = roty(q4);

s4.m = 0.5;
s4.cm = [0.4805;    0.3123;  0.3512] - [0.4805;   0.2365;  0.32];

[s4.G, s4.J] = DynamicParameters(s4.m, s4.cm);

IIrS4 = IIrS3 + ITS3*s4.r;
IIrS4dot = chainDiff(IIrS4, vars, dvars);
ITS4 = ITS3 * s4.T;
ITS4dot = chainDiff(ITS4, vars, dvars);

s4.KE = KineticEnergy(IIrS4dot, s4.m, ITS4dot, s4.G, s4.J);
s4.PE = PotentialEnergy(IIrS4, s4.m, ITS4, s4.G, gz);

% Sawyer Link 5
s5.r = [0; 0.1181; 0.1243];
s5.T = rotz(q5);

s5.m = 0.5;
s5.cm = [0.4805;    0.3550;  0.5636] - [0.4805;   0.3550;  0.4441];

[s5.G, s5.J] = DynamicParameters(s5.m, s5.cm);

IIrS5 = IIrS4 + ITS4*s5.r;
IIrS5dot = chainDiff(IIrS5, vars, dvars);
ITS5 = ITS4 * s5.T;
ITS5dot = chainDiff(ITS5, vars, dvars);

s5.KE = KineticEnergy(IIrS5dot, s5.m, ITS5dot, s5.G, s5.J);
s5.PE = PotentialEnergy(IIrS5, s5.m, ITS5, s5.G, gz);

% Sawyer Link 6
s6.r = [0; 0.0388; 0.2743];
s6.T = roty(q6);

s6.m = 0.3;
s6.cm = [0.48;    0.4700;  0.7463] - [0.48;   0.40;  0.7136];

[s6.G, s6.J] = DynamicParameters(s6.m, s6.cm);

IIrS6 = IIrS5 + ITS5*s6.r;
IIrS6dot = chainDiff(IIrS6, vars, dvars);
ITS6 = ITS5 * s6.T;
ITS6dot = chainDiff(ITS6, vars, dvars);

s6.KE = KineticEnergy(IIrS6dot, s6.m, ITS6dot, s6.G, s6.J);
s6.PE = PotentialEnergy(IIrS6, s6.m, ITS6, s6.G, gz);

% Sawyer Link 7
s7.r = [0; 0.0952; 0.1463];
s7.T = rotz(q7);

s7.m = 0.2;
s7.cm = [0.48;    0.4961;  0.82] - [0.48;   0.48;  0.8];

[s7.G, s7.J] = DynamicParameters(s7.m, s7.cm);

IIrS7 = IIrS6 + ITS6*s7.r;
IIrS7dot = chainDiff(IIrS7, vars, dvars);
ITS7 = ITS6 * s7.T;
ITS7dot = chainDiff(ITS7, vars, dvars);

s7.KE = KineticEnergy(IIrS7dot, s7.m, ITS7dot, s7.G, s7.J);
s7.PE = PotentialEnergy(IIrS7, s7.m, ITS7, s7.G, gz);

% SR Body frame
b.r = [dx; dy; dz+0.0733];
b.T = rotz(dpsi)*roty(dtheta)*rotx(dphi);

b.m = b_m;
syms b_Gx b_Gz b_Jxx b_Jyy b_Jzz b_Jxz

b.G = [b_Gx; 0; b_Gz];
b.J = [b_Jxx, 0, b_Jxz; ...
       0, b_Jyy, 0; ...
       b_Jxz, 0, b_Jzz];

IIrb = IIrS7 + ITS7*b.r;
IIrbdot = chainDiff(IIrb, vars, dvars);
ITb = ITS7 * b.T;
ITbdot = chainDiff(ITb, vars, dvars);

b.KE = KineticEnergy(IIrbdot, b.m, ITbdot, b.G, b.J);
b.PE = PotentialEnergy(IIrb, b.m, ITb, b.G, gz);


% SR Link 1
l1.r = [b_x0; 0; b_z0];
l1.T = roty(th1);

l1.m = sl_m;

syms sl_Gx sl_Jxx sl_Jyy sl_Jzz
l1.G = [sl_Gx; 0; 0];
l1.J = diag([sl_Jxx, sl_Jyy, sl_Jzz]);

IIr1 = IIrb + ITb * l1.r;
IIr1dot = chainDiff(IIr1, vars, dvars);
IT1 = ITb * l1.T;
IT1dot = chainDiff(IT1, vars, dvars);

l1.KE = KineticEnergy(IIr1dot, l1.m, IT1dot, l1.G, l1.J);
l1.PE = PotentialEnergy(IIr1, l1.m, IT1, l1.G, gz);


% SR Link 2
l2.r = [sl_l; 0; 0];
l2.T = rotx(th2);

l2.m = ll_m;

syms ll_Gx ll_Jxx ll_Jyy ll_Jzz
l2.G = [ll_Gx; 0; 0];
l2.J = diag([ll_Jxx, ll_Jyy, ll_Jzz]);

IIr2 = IIr1 + IT1 * l2.r;
IIr2dot = chainDiff(IIr2, vars, dvars);
IT2 = IT1 * l2.T;
IT2dot = chainDiff(IT2, vars, dvars);

l2.KE = KineticEnergy(IIr2dot, l2.m, IT2dot, l2.G, l2.J);
l2.PE = PotentialEnergy(IIr2, l2.m, IT2, l2.G, gz);


% SR Link 3
l3.r = [ll_l; 0; 0];
l3.T = roty(th3);

l3.m = sl_m;
l3.G = l1.G;
l3.J = l1.J;

IIr3 = IIr2 + IT2 * l3.r;
IIr3dot = chainDiff(IIr3, vars, dvars);
IT3 = IT2 * l3.T;
IT3dot = chainDiff(IT3, vars, dvars);

l3.KE = KineticEnergy(IIr3dot, l3.m, IT3dot, l3.G, l3.J);
l3.PE = PotentialEnergy(IIr3, l3.m, IT3, l3.G, gz);


% SR Link 4
l4.r = [sl_l; 0; 0];
l4.T = rotx(th4);

l4.m = ll_m;
l4.G = l2.G;
l4.J = l2.J;

IIr4 = IIr3 + IT3 * l4.r;
IIr4dot = chainDiff(IIr4, vars, dvars);
IT4 = IT3 * l4.T;
IT4dot = chainDiff(IT4, vars, dvars);

l4.KE = KineticEnergy(IIr4dot, l4.m, IT4dot, l4.G, l4.J);
l4.PE = PotentialEnergy(IIr4, l4.m, IT4, l4.G, gz);


% SR Link 5
l5.r = [ll_l; 0; 0];
l5.T = roty(th5);

l5.m = sl_m;
l5.G = l1.G;
l5.J = l1.J;

IIr5 = IIr4 + IT4 * l5.r;
IIr5dot = chainDiff(IIr5, vars, dvars);
IT5 = IT4 * l5.T;
IT5dot = chainDiff(IT5, vars, dvars);

l5.KE = KineticEnergy(IIr5dot, l5.m, IT5dot, l5.G, l5.J);
l5.PE = PotentialEnergy(IIr5, l5.m, IT5, l5.G, gz);


% SR Link 6
l6.r = [sl_l; 0; 0];
l6.T = rotx(th6);

l6.m = ll_m;
l6.G = l2.G;
l6.J = l2.J;

IIr6 = IIr5 + IT5 * l6.r;
IIr6dot = chainDiff(IIr6, vars, dvars);
IT6 = IT5 * l6.T;
IT6dot = chainDiff(IT6, vars, dvars);

l6.KE = KineticEnergy(IIr6dot, l6.m, IT6dot, l6.G, l6.J);
l6.PE = PotentialEnergy(IIr6, l6.m, IT6, l6.G, gz);

syms Kx Ky Kz Kphi Ktheta Kpsi

% Compute Lagrangian
KE = s1.KE + s2.KE + s3.KE + s4.KE + s5.KE + s6.KE + s7.KE + b.KE + l1.KE + l2.KE + l3.KE + l4.KE + l5.KE + l6.KE;
PE = s1.PE + s2.PE + s3.PE + s4.PE + s5.PE + s6.PE + s7.PE + b.PE + l1.PE + l2.PE + l3.PE + l4.PE + l5.PE + l6.PE + 0.5*(Kx * dx^2 + Ky*dy^2 + Kz*dz^2 + Kphi*dphi^2 + Ktheta*dtheta^2 + Kpsi*dpsi^2);

L = KE - PE;

syms bx by bz bphi btheta bpsi

F_FTdamp = [zeros(7,1); bx*dxd; by*dyd; bz*dzd; bphi*dphid; btheta*dthetad; bpsi*dpsid; zeros(6,1)];

H = jacobian(jacobian(L, gammad).', gammad);
N = jacobian(jacobian(L, gammad).', gamma)*gammad - jacobian(L, gamma).' + F_FTdamp;

% F = H gammadd + N

syms s_J s_N s_eta s_K s_b s_L s_R
syms Vs1 Vs2 Vs3 Vs4 Vs5 Vs6 Vs7
syms is1 is2 is3 is4 is5 is6 is7

syms a_J a_N a_eta a_K a_b a_L a_R
syms V1 V2 V3 V4 V5 V6
syms i1 i2 i3 i4 i5 i6

i_RP = [is1; is2; is3; is4; is5; is6; is7];
i_SR = [i1; i2; i3; i4; i5; i6];
V_RP = [Vs1; Vs2; Vs3; Vs4; Vs5; Vs6; Vs7];
V_SR = [V1; V2; V3; V4; V5; V6];

V = [V_RP; V_SR];
i = [i_RP; i_SR];

gammad_RP = gammad(1:7,1);
gammad_SR = gammad(14:19);

Jm = diag([s_J*s_N*ones(1,7) zeros(1,6) a_J*a_N*ones(1,6)]);
Nm = [s_K*i_RP - s_b*s_N*gammad_RP; ...
      zeros(6,1); ...
      a_K*i_SR - a_b*a_N*gammad_SR];

mech_map = diag([ones(1,7)./(s_eta*s_N) ones(1,6) ones(1,6)./(a_eta*a_N)]);
  
Hhat = Jm + mech_map*H;
Nhat = Nm - mech_map*N;

idot = diag([ones(1,7)./s_L, ones(1,6)./a_L])*V - diag([ones(1,7).*(s_R/s_L), ones(1,6).*(a_R/a_L)])*i - diag([ones(1,7).*(s_K*s_N/s_L), ones(1,6).*(a_K*a_N/a_L)])*[gammad(1:7,1); gammad(14:19,1)];

diary ccode_P3D_Hhat.txt
ccode(Hhat)
diary ccode_P3D_Nhat.txt
ccode(Nhat)
diary ccode_P3D_id.txt
ccode(idot)
diary off
