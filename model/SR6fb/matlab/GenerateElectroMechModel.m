Jm = diag([s_J*s_N*ones(1,7) zeros(1,6) a_J*a_N*ones(1,6)]);
Nm = [s_K*i_RP - s_b*s_N*gammad_RP; ...
      zeros(6,1); ...
      a_K*i_SR - a_b*a_N*gammad_SR];

mech_map = diag([ones(1,7)./(s_eta*s_N) ones(1,6) ones(1,6)./(a_eta*a_N)]);
  
Hhat = Jm + mech_map*H;
Nhat = Nm - mech_map*N;

idot = diag([ones(1,7)./s_L, ones(1,6)./a_L])*V - diag([ones(1,7).*(s_R/s_L), ones(1,6).*(a_R/a_L)])*i - diag([ones(1,7).*(s_K*s_N/s_L), ones(1,6).*(a_K*a_N/a_L)])*[gammad(1:7,1); gammad(14:19,1)];

save Hhat.mat Hhat
save Nhat.mat Nhat
save idot.mat idot