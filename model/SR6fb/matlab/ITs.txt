ccode(ITs)

ans =

  ITs[0][0] = -cos(q7)*(sin(q6)*(cos(q4)*(sin(q1)*sin(q3)+cos(q1)*cos(q3)*sin(q2))+cos(q1)*cos(q2)*sin(q4))+cos(q6)*(cos(q5)*(sin(q4)*(sin(q1)*sin(q3)+cos(q1)*cos(q3)*sin(q2))-cos(q1)*cos(q2)*cos(q4))+sin(q5)*(cos(q3)*sin(q1)-cos(q1)*sin(q2)*sin(q3))))+sin(q7)*(sin(q5)*(sin(q4)*(sin(q1)*sin(q3)+cos(q1)*cos(q3)*sin(q2))-cos(q1)*cos(q2)*cos(q4))-cos(q5)*(cos(q3)*sin(q1)-cos(q1)*sin(q2)*sin(q3)));
  ITs[0][1] = sin(q7)*(sin(q6)*(cos(q4)*(sin(q1)*sin(q3)+cos(q1)*cos(q3)*sin(q2))+cos(q1)*cos(q2)*sin(q4))+cos(q6)*(cos(q5)*(sin(q4)*(sin(q1)*sin(q3)+cos(q1)*cos(q3)*sin(q2))-cos(q1)*cos(q2)*cos(q4))+sin(q5)*(cos(q3)*sin(q1)-cos(q1)*sin(q2)*sin(q3))))+cos(q7)*(sin(q5)*(sin(q4)*(sin(q1)*sin(q3)+cos(q1)*cos(q3)*sin(q2))-cos(q1)*cos(q2)*cos(q4))-cos(q5)*(cos(q3)*sin(q1)-cos(q1)*sin(q2)*sin(q3)));
  ITs[0][2] = cos(q6)*(cos(q4)*(sin(q1)*sin(q3)+cos(q1)*cos(q3)*sin(q2))+cos(q1)*cos(q2)*sin(q4))-sin(q6)*(cos(q5)*(sin(q4)*(sin(q1)*sin(q3)+cos(q1)*cos(q3)*sin(q2))-cos(q1)*cos(q2)*cos(q4))+sin(q5)*(cos(q3)*sin(q1)-cos(q1)*sin(q2)*sin(q3)));
  ITs[1][0] = cos(q7)*(sin(q6)*(cos(q4)*(cos(q1)*sin(q3)-cos(q3)*sin(q1)*sin(q2))-cos(q2)*sin(q1)*sin(q4))+cos(q6)*(cos(q5)*(sin(q4)*(cos(q1)*sin(q3)-cos(q3)*sin(q1)*sin(q2))+cos(q2)*cos(q4)*sin(q1))+sin(q5)*(cos(q1)*cos(q3)+sin(q1)*sin(q2)*sin(q3))))-sin(q7)*(sin(q5)*(sin(q4)*(cos(q1)*sin(q3)-cos(q3)*sin(q1)*sin(q2))+cos(q2)*cos(q4)*sin(q1))-cos(q5)*(cos(q1)*cos(q3)+sin(q1)*sin(q2)*sin(q3)));
  ITs[1][1] = -sin(q7)*(sin(q6)*(cos(q4)*(cos(q1)*sin(q3)-cos(q3)*sin(q1)*sin(q2))-cos(q2)*sin(q1)*sin(q4))+cos(q6)*(cos(q5)*(sin(q4)*(cos(q1)*sin(q3)-cos(q3)*sin(q1)*sin(q2))+cos(q2)*cos(q4)*sin(q1))+sin(q5)*(cos(q1)*cos(q3)+sin(q1)*sin(q2)*sin(q3))))-cos(q7)*(sin(q5)*(sin(q4)*(cos(q1)*sin(q3)-cos(q3)*sin(q1)*sin(q2))+cos(q2)*cos(q4)*sin(q1))-cos(q5)*(cos(q1)*cos(q3)+sin(q1)*sin(q2)*sin(q3)));
  ITs[1][2] = -cos(q6)*(cos(q4)*(cos(q1)*sin(q3)-cos(q3)*sin(q1)*sin(q2))-cos(q2)*sin(q1)*sin(q4))+sin(q6)*(cos(q5)*(sin(q4)*(cos(q1)*sin(q3)-cos(q3)*sin(q1)*sin(q2))+cos(q2)*cos(q4)*sin(q1))+sin(q5)*(cos(q1)*cos(q3)+sin(q1)*sin(q2)*sin(q3)));
  ITs[2][0] = sin(q7)*(sin(q5)*(cos(q4)*sin(q2)+cos(q2)*cos(q3)*sin(q4))+cos(q2)*cos(q5)*sin(q3))-cos(q7)*(cos(q6)*(cos(q5)*(cos(q4)*sin(q2)+cos(q2)*cos(q3)*sin(q4))-cos(q2)*sin(q3)*sin(q5))-sin(q6)*(sin(q2)*sin(q4)-cos(q2)*cos(q3)*cos(q4)));
  ITs[2][1] = cos(q7)*(sin(q5)*(cos(q4)*sin(q2)+cos(q2)*cos(q3)*sin(q4))+cos(q2)*cos(q5)*sin(q3))+sin(q7)*(cos(q6)*(cos(q5)*(cos(q4)*sin(q2)+cos(q2)*cos(q3)*sin(q4))-cos(q2)*sin(q3)*sin(q5))-sin(q6)*(sin(q2)*sin(q4)-cos(q2)*cos(q3)*cos(q4)));
  ITs[2][2] = -sin(q6)*(cos(q5)*(cos(q4)*sin(q2)+cos(q2)*cos(q3)*sin(q4))-cos(q2)*sin(q3)*sin(q5))-cos(q6)*(sin(q2)*sin(q4)-cos(q2)*cos(q3)*cos(q4));

diary off
