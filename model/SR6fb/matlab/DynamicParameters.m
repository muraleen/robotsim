function [ G, J ] = DynamicParameters( m, cm )
    
    G = m .* cm;
    
    J = m .* diag([cm(2)^2 + cm(3)^2, cm(1)^2 + cm(3)^2, cm(1)^2 + cm(2)^2]);
    
    for m = 1:3
        for n = 1:3
            if m ~= n
                J(m,n) = - m * cm(m) * cm(n);
            end
        end
    end

end

