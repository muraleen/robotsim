clc; clear all;

%% Initialize symbolic variables
% Generalized coordinates and derivatives
syms q1 q2 q3 q4 q5 q6 q7 dx dy dz dphi dtheta dpsi th1 th2 th3 th4 th5 th6
syms q1d q2d q3d q4d q5d q6d q7d dxd dyd dzd dphid dthetad dpsid th1d th2d th3d th4d th5d th6d
syms q1dd q2dd q3dd q4dd q5dd q6dd q7dd dxdd dydd dzdd dphidd dthetadd dpsidd th1dd th2dd th3dd th4dd th5dd th6dd

% Dynamic parameters
syms b_m b_s b_x0 b_z0 ll_m ll_l ll_r sl_m sl_l sl_r GZ
syms b_Gx b_Gz b_Jxx b_Jyy b_Jzz b_Jxz
syms sl_Gx sl_Jxx sl_Jyy sl_Jzz
syms ll_Gx ll_Jxx ll_Jyy ll_Jzz

% Force-torque sensor parameters
syms Kx Ky Kz Kphi Ktheta Kpsi
syms bx by bz bphi btheta bpsi

% Actuator dynamic parameters
syms s_J s_N s_eta s_K s_b s_L s_R
syms Vs1 Vs2 Vs3 Vs4 Vs5 Vs6 Vs7
syms is1 is2 is3 is4 is5 is6 is7
syms a_J a_N a_eta a_K a_b a_L a_R
syms V1 V2 V3 V4 V5 V6
syms i1 i2 i3 i4 i5 i6

gamma = [q1 q2 q3 q4 q5 q6 q7 dx dy dz dphi dtheta dpsi th1 th2 th3 th4 th5 th6].';
gammad = [q1d q2d q3d q4d q5d q6d q7d dxd dyd dzd dphid dthetad dpsid th1d th2d th3d th4d th5d th6d].';
gammadd = [q1dd q2dd q3dd q4dd q5dd q6dd q7dd dxdd dydd dzdd dphidd dthetadd dpsidd th1dd th2dd th3dd th4dd th5dd th6dd].';

vars = [gamma; gammad];
dvars = [gammad; gammadd];

i_RP = [is1; is2; is3; is4; is5; is6; is7];
i_SR = [i1; i2; i3; i4; i5; i6];
V_RP = [Vs1; Vs2; Vs3; Vs4; Vs5; Vs6; Vs7];
V_SR = [V1; V2; V3; V4; V5; V6];

V = [V_RP; V_SR];
i = [i_RP; i_SR];

gammad_RP = gammad(1:7,1);
gammad_SR = gammad(14:19);

frames = [];

%% Derive lagrangians (KE - PE) for each frame
DeriveFrameLagrangians;
% load frames_L.mat frames

%% Compute system mass matrix (H) and vector of frictional, coriolis and
% gravitational forces for the system
%ComputeEOMComponents;
% load H.mat H
% load N.mat N

%% Generate electro-mechanical model using actuator dynamics
%GenerateElectroMechModel;
% load Hhat.mat Hhat
% load Nhat.mat Nhat
% load idot.mat idot
% 
% %% Generate C-Code and write to file
% diary ccode_P3D_Hhat.txt
% ccode(Hhat)
% diary ccode_P3D_Nhat.txt
% ccode(Nhat)
% diary ccode_P3D_id.txt
% ccode(idot)
% diary off