--[[ OBSERVER MODEL

Sensors:
	Bosch BNO055 IMU: Mounted at the center of the sphere
			Angle and rate about inertial x-axis of pendulum
			Angle and rate about inertial y-axis of sphere
			Angle and rate about inertial z-axis of sphere
	Encoder 1: At actuator 1
			Angle of rotation of actuator 1
	Encoder 2: At actuator 2
			Angle of rotation of actuator 2
	GPS: Mounted at the center of the sphere
			Inertial x position of the sphere
			Inertial y position of the sphere

--]]

if init then
	setprop("/sensors/imu/device", "Bosch-BNO055");
	io.write("Initializing Observer Model...\n")
end

-- Generate measurements

-- IMU (TO-DO: noise model)
imu_phi_m = getprop("/dynamics/gamma/phi") + getprop("/dynamics/gamma/a1");
imu_theta_m = getprop("/dynamics/gamma/theta");
imu_psi_m = getprop("/dynamics/gamma/psi");
imu_phid_m = getprop("/dynamics/gamma-dot/phi") + getprop("/dynamics/gamma-dot/a1");
imu_thetad_m = getprop("/dynamics/gamma-dot/theta");
imu_psid_m = getprop("/dynamics/gamma-dot/psi");

-- Encoder 1 (TO-DO: quantization)
enc1_m = getprop("/dynamics/gamma/a1");
enc1_r_m = getprop("/dynamics/gamma-dot/a1");

-- Encoder 2 (TO-DO: quantization)
enc2_m = getprop("/dynamics/gamma/a2");
enc2_r_m = getprop("/dynamics/gamma-dot/a2");

-- GPS (TO-DO: noise model)
gps_lat = -112.449843 + getprop("/dynamics/gamma/x")/111120;
gps_lon = 34.615093 + getprop("/dynamics/gamma/y")/111120;

-- Write measurements to property tree
setprop("/sensors/imu/phi", imu_phi_m);
setprop("/sensors/imu/theta", imu_theta_m);
setprop("/sensors/imu/psi", imu_psi_m);
setprop("/sensors/imu/phid", imu_phid_m);
setprop("/sensors/imu/thetad", imu_thetad_m);
setprop("/sensors/imu/psid", imu_psid_m);
setprop("/sensors/enc1/angle", enc1_m);
setprop("/sensors/enc2/angle", enc2_m);
setprop("/sensors/enc1/rate", enc1_r_m);
setprop("/sensors/enc2/rate", enc2_r_m);
setprop("/sensors/gps/lat", gps_lat);
setprop("/sensors/gps/lon", gps_lon);
