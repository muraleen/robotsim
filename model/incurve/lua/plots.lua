if init then
	plotData = io.open("plot/data/incurve_pos.dat", "w")
	vData = io.open("plot/data/incurve_vel.dat", "w")
end

t = getprop("/sim/time/elapsed")

if t >= 0 then

	x = getprop("/dynamics/gamma/x")
	y = getprop("/dynamics/gamma/y")
	
	io.output(plotData)
	io.write(t, "\t", x, "\t", y, "\n")
	io.flush()
	
	xd = getprop("/dynamics/gamma-dot/x")
	yd = getprop("/dynamics/gamma-dot/y")
	
	io.output(vData)
	io.write(t, "\t", xd, "\t", yd, "\t", 0.15, "\t", 0.25, "\n")
	io.flush()
end
