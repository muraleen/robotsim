--[[ For all lua scripts defined in the SCRIPTS path variable of the model file,
the global variable 'init' is set to true on the first call. Then, the variable
value is false. Use this to control loops instead of an infinite while ]]

if init then
	io.write("Initializing Onboard Systems...\n")
	t = 0
	dt = 0.05
	
	f = 0.05
	w = 2*math.pi*f
	
	zeta = 0.707
	wn = 1
	
	Kp = wn*wn
	Kd = 2*zeta*wn
	
	--xctlr = pid_init(1,0.2,0.02)
	--yctlr = pid_init(1,0.2,0.02)
	
	xd_prev = 0
	yd_prev = 0
end

xd_des = 0.15
yd_des = 0.15

xd = getprop("/dynamics/gamma-dot/x")
yd = getprop("/dynamics/gamma-dot/y")

io.write(xd, "\t", yd, "\n");


xdd = Kd*(xd-xd_prev) + Kp*(xd_des - xd)
ydd = Kd*(yd-yd_prev) + Kp*(yd_des - yd)

xd_prev = xd
yd_prev = yd

b1 = saturate(math.sqrt(xdd^2 + ydd^2),-1.5,1.5)
b2 = math.atan2(ydd, xdd) - math.pi/2

setprop("/controller/desired/b1", b1)
setprop("/controller/desired/b2", b2)

t = t + dt
