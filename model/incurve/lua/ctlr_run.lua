--[[ For all lua scripts defined in the SCRIPTS path variable of the model file,
the global variable 'init' is set to true on the first call. Then, the variable
value is false. Use this to control loops instead of an infinite while ]]

if getprop("/keyboard/state") == 1 then

	key = getprop("/keyboard/key")
	
	if key == 65430 then
		xd_d = 1
		yd_d = 0
	elseif key == 65432 then
		xd_d = -1
		yd_d = 0
	elseif key == 65431 then
		xd_d = 0
		yd_d = -1
	elseif key == 65433 then
		xd_d = 0
		yd_d = 1
	elseif key == 65429 then
		xd_d = 1
		yd_d = -1
	elseif key == 65434 then
		xd_d = -1
		yd_d = -1
	elseif key == 65436 then
		xd_d = 1
		yd_d = 1
	elseif key == 65435 then
		xd_d = -1
		yd_d = 1
	end
else
	xd_d = 0
	yd_d = 0
end

U_F1 = yd_d*4
U_F2 = xd_d*4

t = t + dt
