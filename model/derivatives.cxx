#include "../src/fk/derivatives.hxx"

vector<vec> computeRdot(vec X, map<string, double> params, string model)
{
	vector<vec> Rdot;

	if (model.compare("ARSR") == 0) { // ARSR
		
		// Copy variables and derivatives from X vector
		double x = X(0);
		double y = X(1);
		double psi_Var = X(2);
		double thMW = X(3);
		double th1 = X(4);
		double th2 = X(5);
		double th3 = X(6);
		double th4 = X(7);
		double xd = X(8);
		double yd = X(9);
		double psid = X(10);
		double thMWd = X(11);
		double th1d = X(12);
		double th2d = X(13);
		double th3d = X(14);
		double th4d = X(15);
		
		// Copy used parameters
		double l1_l = params["l1_l"];
		double l2_l = params["l2_l"];
		double l3_l = params["l3_l"];
		double l1_xOff = params["l1_xOff"];
		double l1_yOff = params["l1_yOff"];
		
		Rdot.resize(6);
		
		for (auto it : Rdot) {
			it.resize(3);
		}
		
		// Body frame
		Rdot.at(0) << xd << endr
				   << yd << endr
				   << 0 << endr;
		
		// Momentum Wheel
		Rdot.at(1) = Rdot.at(0);
		
		// Link 1
		Rdot.at(2) << xd-psid*(l1_yOff*cos(psi_Var)+l1_xOff*sin(psi_Var)) << endr
				   << yd+psid*(l1_xOff*cos(psi_Var)-l1_yOff*sin(psi_Var)) << endr
				   << 0 << endr;
		
		// Link 2
		Rdot.at(3) << xd-psid*(l1_l*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+l1_yOff*cos(psi_Var)+l1_xOff*sin(psi_Var))-l1_l*th1d*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)) << endr
				   << yd+psid*(l1_l*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))+l1_xOff*cos(psi_Var)-l1_yOff*sin(psi_Var))+l1_l*th1d*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)) << endr
				   << 0 << endr;
		
		// Link 3
		Rdot.at(4) << xd-th1d*(l1_l*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+l2_l*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))))-psid*(l1_l*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+l1_yOff*cos(psi_Var)+l1_xOff*sin(psi_Var)+l2_l*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))))-l2_l*th2d*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))) << endr
				   << yd+th1d*(l1_l*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))+l2_l*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))))+psid*(l1_l*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))+l1_xOff*cos(psi_Var)-l1_yOff*sin(psi_Var)+l2_l*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))))+l2_l*th2d*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))) << endr
				   << 0 << endr;
		
		// Link 4
		Rdot.at(5) << xd-th1d*(l1_l*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+l3_l*(cos(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))+sin(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))))+l2_l*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))))-psid*(l1_l*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+l1_yOff*cos(psi_Var)+l1_xOff*sin(psi_Var)+l3_l*(cos(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))+sin(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))))+l2_l*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))))-th2d*(l3_l*(cos(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))+sin(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))))+l2_l*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))))-l3_l*th3d*(cos(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))+sin(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))) << endr
				   << yd+th1d*(l1_l*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))+l3_l*(cos(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))-sin(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))))+l2_l*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))))+psid*(l1_l*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))+l1_xOff*cos(psi_Var)-l1_yOff*sin(psi_Var)+l3_l*(cos(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))-sin(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))))+l2_l*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))))+th2d*(l3_l*(cos(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))-sin(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))))+l2_l*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))))+l3_l*th3d*(cos(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))-sin(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))) << endr
				   << 0 << endr;
	}
	
	return Rdot;
}

vector<mat> computeTdot(vec X, map<string, double> params, string model)
{
	vector<mat> Tdot;
	
	if (model.compare("ARSR") == 0) { // ARSR
		
		// Copy variables and derivatives from X vector
		double x = X(0);
		double y = X(1);
		double psi_Var = X(2);
		double thMW = X(3);
		double th1 = X(4);
		double th2 = X(5);
		double th3 = X(6);
		double th4 = X(7);
		double xd = X(8);
		double yd = X(9);
		double psid = X(10);
		double thMWd = X(11);
		double th1d = X(12);
		double th2d = X(13);
		double th3d = X(14);
		double th4d = X(15);
		
		Tdot.resize(6);
		
		for (int i=0; i<6; i++) {
			Tdot.at(i).resize(3,3);
			Tdot.at(i).zeros();
		}
		
		// Body frame
		Tdot.at(0)(0,0) = -psid*sin(psi_Var);
		Tdot.at(0)(0,1) = -psid*cos(psi_Var);
		Tdot.at(0)(1,0) = psid*cos(psi_Var);
		Tdot.at(0)(1,1) = -psid*sin(psi_Var);
		
		// Momentum Wheel
		Tdot.at(1)(0,0) = -psid*(cos(psi_Var)*sin(thMW)+cos(thMW)*sin(psi_Var))-thMWd*(cos(psi_Var)*sin(thMW)+cos(thMW)*sin(psi_Var));
		Tdot.at(1)(0,1) = -psid*(cos(psi_Var)*cos(thMW)-sin(psi_Var)*sin(thMW))-thMWd*(cos(psi_Var)*cos(thMW)-sin(psi_Var)*sin(thMW));
		Tdot.at(1)(1,0) = psid*(cos(psi_Var)*cos(thMW)-sin(psi_Var)*sin(thMW))+thMWd*(cos(psi_Var)*cos(thMW)-sin(psi_Var)*sin(thMW));
		Tdot.at(1)(1,1) = -psid*(cos(psi_Var)*sin(thMW)+cos(thMW)*sin(psi_Var))-thMWd*(cos(psi_Var)*sin(thMW)+cos(thMW)*sin(psi_Var));

		// Link 1
		Tdot.at(2)(0,0) = -psid*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))-th1d*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var));
		Tdot.at(2)(0,1) = -psid*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-th1d*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1));
		Tdot.at(2)(1,0) = psid*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))+th1d*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1));
		Tdot.at(2)(1,1) = -psid*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))-th1d*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var));

		// Link 2
		Tdot.at(3)(0,0) = -psid*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))-th1d*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))-th2d*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)));
		Tdot.at(3)(0,1) = -psid*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))-th1d*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))-th2d*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)));
		Tdot.at(3)(1,0) = psid*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))+th1d*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))+th2d*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)));
		Tdot.at(3)(1,1) = -psid*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))-th1d*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))-th2d*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)));

		// Link 3
		Tdot.at(4)(0,0) = -psid*(cos(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))+sin(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))))-th1d*(cos(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))+sin(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))))-th2d*(cos(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))+sin(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))))-th3d*(cos(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))+sin(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))));
		Tdot.at(4)(0,1) = -psid*(cos(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))-sin(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))))-th1d*(cos(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))-sin(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))))-th2d*(cos(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))-sin(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))))-th3d*(cos(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))-sin(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))));
		Tdot.at(4)(1,0) = psid*(cos(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))-sin(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))))+th1d*(cos(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))-sin(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))))+th2d*(cos(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))-sin(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))))+th3d*(cos(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))-sin(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))));
		Tdot.at(4)(1,1) = -psid*(cos(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))+sin(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))))-th1d*(cos(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))+sin(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))))-th2d*(cos(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))+sin(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))))-th3d*(cos(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))+sin(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))));

		// Link 4
		Tdot.at(5)(0,0) = -psid*(cos(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))+sin(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))))+sin(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))-sin(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))))-th1d*(cos(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))+sin(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))))+sin(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))-sin(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))))-th2d*(cos(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))+sin(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))))+sin(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))-sin(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))))-th3d*(cos(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))+sin(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))))+sin(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))-sin(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))))-th4d*(cos(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))+sin(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))))+sin(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))-sin(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))));
		Tdot.at(5)(0,1) = -psid*(cos(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))-sin(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))))-sin(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))+sin(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))))-th1d*(cos(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))-sin(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))))-sin(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))+sin(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))))-th2d*(cos(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))-sin(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))))-sin(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))+sin(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))))-th3d*(cos(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))-sin(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))))-sin(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))+sin(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))))-th4d*(cos(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))-sin(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))))-sin(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))+sin(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))));
		Tdot.at(5)(1,0) = psid*(cos(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))-sin(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))))-sin(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))+sin(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))))+th1d*(cos(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))-sin(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))))-sin(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))+sin(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))))+th2d*(cos(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))-sin(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))))-sin(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))+sin(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))))+th3d*(cos(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))-sin(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))))-sin(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))+sin(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))))+th4d*(cos(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))-sin(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))))-sin(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))+sin(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))));
		Tdot.at(5)(1,1) = -psid*(cos(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))+sin(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))))+sin(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))-sin(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))))-th1d*(cos(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))+sin(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))))+sin(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))-sin(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))))-th2d*(cos(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))+sin(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))))+sin(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))-sin(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))))-th3d*(cos(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))+sin(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))))+sin(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))-sin(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))))-th4d*(cos(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))+sin(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))))+sin(th4)*(cos(th3)*(cos(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1))-sin(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var)))-sin(th3)*(cos(th2)*(cos(psi_Var)*sin(th1)+cos(th1)*sin(psi_Var))+sin(th2)*(cos(psi_Var)*cos(th1)-sin(psi_Var)*sin(th1)))));
	}
	
	return Tdot;
}
