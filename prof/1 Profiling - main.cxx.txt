[360] dt = 1e-06s
[374] dt = 1e-06s
[397] dt = 3e-06s
[405] dt = 4e-06s
[408] dt = 2e-06s
[425] dt = 2e-06s
[445] dt = 7.3e-05s
[450] dt = 0.001063s		<<<<<
[459] dt = 1.1e-05s

SUMMARY OF CALLS:

[360]	Beginning of simulation loop

[374]	Run Lua/C++ controller function

[397]	CoM computation using FK

[405]	Lua scripts call

[425]	Data logging

[445]	Write core variables to property tree

[450]	RK4::integrate call

[459]	Copy variables for next run
