# Install script for directory: /home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/src/osgGA

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "libopenscenegraph")
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgGA.so.3.2.2"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgGA.so.100"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgGA.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHECK
           FILE "${file}"
           RPATH "")
    endif()
  endforeach()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib64" TYPE SHARED_LIBRARY FILES
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/build/lib/libosgGA.so.3.2.2"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/build/lib/libosgGA.so.100"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/build/lib/libosgGA.so"
    )
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgGA.so.3.2.2"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgGA.so.100"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgGA.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_REMOVE
           FILE "${file}")
      if(CMAKE_INSTALL_DO_STRIP)
        execute_process(COMMAND "/usr/bin/strip" "${file}")
      endif()
    endif()
  endforeach()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "libopenscenegraph-dev")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/osgGA" TYPE FILE FILES
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgGA/AnimationPathManipulator"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgGA/DriveManipulator"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgGA/Device"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgGA/EventQueue"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgGA/EventVisitor"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgGA/Export"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgGA/FirstPersonManipulator"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgGA/FlightManipulator"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgGA/GUIActionAdapter"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgGA/GUIEventAdapter"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgGA/GUIEventHandler"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgGA/KeySwitchMatrixManipulator"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgGA/CameraManipulator"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgGA/MultiTouchTrackballManipulator"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgGA/NodeTrackerManipulator"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgGA/OrbitManipulator"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgGA/StandardManipulator"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgGA/SphericalManipulator"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgGA/StateSetManipulator"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgGA/TerrainManipulator"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgGA/TrackballManipulator"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgGA/UFOManipulator"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgGA/Version"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgGA/CameraViewSwitchManipulator"
    )
endif()

