# Install script for directory: /home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/src

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/build/src/OpenThreads/cmake_install.cmake")
  include("/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/build/src/osg/cmake_install.cmake")
  include("/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/build/src/osgDB/cmake_install.cmake")
  include("/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/build/src/osgUtil/cmake_install.cmake")
  include("/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/build/src/osgGA/cmake_install.cmake")
  include("/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/build/src/osgText/cmake_install.cmake")
  include("/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/build/src/osgViewer/cmake_install.cmake")
  include("/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/build/src/osgAnimation/cmake_install.cmake")
  include("/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/build/src/osgFX/cmake_install.cmake")
  include("/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/build/src/osgManipulator/cmake_install.cmake")
  include("/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/build/src/osgParticle/cmake_install.cmake")
  include("/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/build/src/osgPresentation/cmake_install.cmake")
  include("/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/build/src/osgShadow/cmake_install.cmake")
  include("/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/build/src/osgSim/cmake_install.cmake")
  include("/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/build/src/osgTerrain/cmake_install.cmake")
  include("/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/build/src/osgWidget/cmake_install.cmake")
  include("/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/build/src/osgVolume/cmake_install.cmake")
  include("/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/build/src/osgWrappers/serializers/cmake_install.cmake")
  include("/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/build/src/osgWrappers/deprecated-dotosg/cmake_install.cmake")
  include("/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/build/src/osgPlugins/cmake_install.cmake")

endif()

