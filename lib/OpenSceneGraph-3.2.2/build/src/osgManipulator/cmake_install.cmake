# Install script for directory: /home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/src/osgManipulator

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "libopenscenegraph")
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgManipulator.so.3.2.2"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgManipulator.so.100"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgManipulator.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHECK
           FILE "${file}"
           RPATH "")
    endif()
  endforeach()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib64" TYPE SHARED_LIBRARY FILES
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/build/lib/libosgManipulator.so.3.2.2"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/build/lib/libosgManipulator.so.100"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/build/lib/libosgManipulator.so"
    )
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgManipulator.so.3.2.2"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgManipulator.so.100"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgManipulator.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_REMOVE
           FILE "${file}")
      if(CMAKE_INSTALL_DO_STRIP)
        execute_process(COMMAND "/usr/bin/strip" "${file}")
      endif()
    endif()
  endforeach()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "libopenscenegraph-dev")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/osgManipulator" TYPE FILE FILES
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgManipulator/AntiSquish"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgManipulator/Command"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgManipulator/CommandManager"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgManipulator/Constraint"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgManipulator/Dragger"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgManipulator/Export"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgManipulator/Projector"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgManipulator/RotateCylinderDragger"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgManipulator/RotateSphereDragger"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgManipulator/Scale1DDragger"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgManipulator/Scale2DDragger"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgManipulator/ScaleAxisDragger"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgManipulator/Selection"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgManipulator/TabBoxDragger"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgManipulator/TabBoxTrackballDragger"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgManipulator/TabPlaneDragger"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgManipulator/TabPlaneTrackballDragger"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgManipulator/TrackballDragger"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgManipulator/Translate1DDragger"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgManipulator/Translate2DDragger"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgManipulator/TranslateAxisDragger"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgManipulator/TranslatePlaneDragger"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgManipulator/Version"
    )
endif()

