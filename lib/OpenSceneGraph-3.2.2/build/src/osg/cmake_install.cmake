# Install script for directory: /home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/src/osg

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "libopenscenegraph")
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosg.so.3.2.2"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosg.so.100"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosg.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHECK
           FILE "${file}"
           RPATH "")
    endif()
  endforeach()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib64" TYPE SHARED_LIBRARY FILES
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/build/lib/libosg.so.3.2.2"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/build/lib/libosg.so.100"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/build/lib/libosg.so"
    )
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosg.so.3.2.2"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosg.so.100"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosg.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_REMOVE
           FILE "${file}")
      if(CMAKE_INSTALL_DO_STRIP)
        execute_process(COMMAND "/usr/bin/strip" "${file}")
      endif()
    endif()
  endforeach()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "libopenscenegraph-dev")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/osg" TYPE FILE FILES
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/AlphaFunc"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/AnimationPath"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/ApplicationUsage"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/ArgumentParser"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Array"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/ArrayDispatchers"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/AudioStream"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/AutoTransform"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Billboard"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/BlendColor"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/BlendEquation"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/BlendFunc"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/BoundingBox"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/BoundingSphere"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/BoundsChecking"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/buffered_value"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/BufferIndexBinding"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/BufferObject"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Camera"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/CameraNode"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/CameraView"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/ClampColor"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/ClearNode"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/ClipNode"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/ClipPlane"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/ClusterCullingCallback"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/CollectOccludersVisitor"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/ColorMask"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/ColorMatrix"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/ComputeBoundsVisitor"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/ConvexPlanarOccluder"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/ConvexPlanarPolygon"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/CoordinateSystemNode"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/CopyOp"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/CullFace"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/CullingSet"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/CullSettings"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/CullStack"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/DeleteHandler"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Depth"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/DisplaySettings"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Drawable"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/DrawPixels"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Endian"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Export"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/fast_back_stack"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Fog"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/FragmentProgram"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/FrameBufferObject"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/FrameStamp"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/FrontFace"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Geode"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Geometry"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/GL"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/GL2Extensions"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/GLExtensions"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/GLBeginEndAdapter"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/GLObjects"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/GLU"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/GraphicsCostEstimator"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/GraphicsContext"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/GraphicsThread"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Group"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Hint"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Image"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/ImageSequence"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/ImageStream"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/ImageUtils"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/io_utils"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/KdTree"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Light"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/LightModel"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/LightSource"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/LineSegment"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/LineStipple"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/LineWidth"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/LOD"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/LogicOp"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Material"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Math"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Matrix"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Matrixd"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Matrixf"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/MatrixTransform"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/MixinVector"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Multisample"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Node"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/NodeCallback"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/NodeTrackerCallback"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/NodeVisitor"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Notify"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Object"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/observer_ptr"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Observer"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/ObserverNodePath"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/OccluderNode"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/OcclusionQueryNode"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/OperationThread"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/PatchParameter"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/PagedLOD"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Plane"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Point"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/PointSprite"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/PolygonMode"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/PolygonOffset"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/PolygonStipple"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Polytope"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/PositionAttitudeTransform"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/PrimitiveSet"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/PrimitiveRestartIndex"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Program"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Projection"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/ProxyNode"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Quat"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Referenced"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/ref_ptr"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/RenderInfo"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/SampleMaski"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Scissor"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Sequence"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/ShadeModel"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Shader"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/ShaderAttribute"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/ShaderComposer"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/ShadowVolumeOccluder"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Shape"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/ShapeDrawable"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/State"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/StateAttribute"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/StateAttributeCallback"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/StateSet"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Stats"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Stencil"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/StencilTwoSided"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Switch"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/TemplatePrimitiveFunctor"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/TexEnv"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/TexEnvCombine"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/TexEnvFilter"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/TexGen"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/TexGenNode"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/TexMat"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Texture"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Texture1D"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Texture2D"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Texture2DMultisample"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Texture2DArray"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Texture3D"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/TextureBuffer"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/TextureCubeMap"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/TextureRectangle"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Timer"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/TransferFunction"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Transform"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/TriangleFunctor"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/TriangleIndexFunctor"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Uniform"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/UserDataContainer"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/ValueObject"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Vec2"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Vec2b"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Vec2d"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Vec2f"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Vec2i"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Vec2s"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Vec2ub"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Vec2ui"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Vec2us"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Vec3"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Vec3b"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Vec3d"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Vec3f"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Vec3i"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Vec3s"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Vec3ub"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Vec3ui"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Vec3us"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Vec4"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Vec4b"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Vec4d"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Vec4f"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Vec4i"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Vec4s"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Vec4ub"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Vec4ui"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Vec4us"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Version"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/VertexProgram"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/View"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osg/Viewport"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/build/include/osg/Config"
    )
endif()

