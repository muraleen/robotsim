# Install script for directory: /home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/src/osgTerrain

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "libopenscenegraph")
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgTerrain.so.3.2.2"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgTerrain.so.100"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgTerrain.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHECK
           FILE "${file}"
           RPATH "")
    endif()
  endforeach()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib64" TYPE SHARED_LIBRARY FILES
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/build/lib/libosgTerrain.so.3.2.2"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/build/lib/libosgTerrain.so.100"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/build/lib/libosgTerrain.so"
    )
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgTerrain.so.3.2.2"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgTerrain.so.100"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgTerrain.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_REMOVE
           FILE "${file}")
      if(CMAKE_INSTALL_DO_STRIP)
        execute_process(COMMAND "/usr/bin/strip" "${file}")
      endif()
    endif()
  endforeach()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "libopenscenegraph-dev")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/osgTerrain" TYPE FILE FILES
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgTerrain/Export"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgTerrain/Locator"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgTerrain/Layer"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgTerrain/TerrainTile"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgTerrain/TerrainTechnique"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgTerrain/Terrain"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgTerrain/GeometryTechnique"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgTerrain/ValidDataOperator"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgTerrain/Version"
    )
endif()

