# Install script for directory: /home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/src/osgParticle

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "libopenscenegraph")
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgParticle.so.3.2.2"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgParticle.so.100"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgParticle.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHECK
           FILE "${file}"
           RPATH "")
    endif()
  endforeach()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib64" TYPE SHARED_LIBRARY FILES
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/build/lib/libosgParticle.so.3.2.2"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/build/lib/libosgParticle.so.100"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/build/lib/libosgParticle.so"
    )
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgParticle.so.3.2.2"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgParticle.so.100"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgParticle.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_REMOVE
           FILE "${file}")
      if(CMAKE_INSTALL_DO_STRIP)
        execute_process(COMMAND "/usr/bin/strip" "${file}")
      endif()
    endif()
  endforeach()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "libopenscenegraph-dev")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/osgParticle" TYPE FILE FILES
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/AccelOperator"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/AngularAccelOperator"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/BoxPlacer"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/CenteredPlacer"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/ConnectedParticleSystem"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/ConstantRateCounter"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/Counter"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/Emitter"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/ExplosionDebrisEffect"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/ExplosionEffect"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/Export"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/FireEffect"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/FluidFrictionOperator"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/FluidProgram"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/ForceOperator"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/Interpolator"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/LinearInterpolator"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/ModularEmitter"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/ModularProgram"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/MultiSegmentPlacer"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/Operator"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/Particle"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/ParticleEffect"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/ParticleProcessor"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/ParticleSystem"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/ParticleSystemUpdater"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/Placer"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/PointPlacer"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/PrecipitationEffect"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/Program"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/RadialShooter"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/RandomRateCounter"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/range"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/SectorPlacer"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/SegmentPlacer"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/Shooter"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/SmokeEffect"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/SmokeTrailEffect"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/VariableRateCounter"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/Version"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/CompositePlacer"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/AngularDampingOperator"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/DampingOperator"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/ExplosionOperator"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/OrbitOperator"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/DomainOperator"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/BounceOperator"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.2.2/include/osgParticle/SinkOperator"
    )
endif()

