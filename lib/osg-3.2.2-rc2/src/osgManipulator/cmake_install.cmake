# Install script for directory: /home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgManipulator

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr/local")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Release")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "libopenscenegraph")
  FOREACH(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgManipulator.so.3.2.2"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgManipulator.so.100"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgManipulator.so"
      )
    IF(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      FILE(RPATH_CHECK
           FILE "${file}"
           RPATH "")
    ENDIF()
  ENDFOREACH()
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib64" TYPE SHARED_LIBRARY FILES
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/lib/libosgManipulator.so.3.2.2"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/lib/libosgManipulator.so.100"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/lib/libosgManipulator.so"
    )
  FOREACH(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgManipulator.so.3.2.2"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgManipulator.so.100"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgManipulator.so"
      )
    IF(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      FILE(RPATH_REMOVE
           FILE "${file}")
      IF(CMAKE_INSTALL_DO_STRIP)
        EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "${file}")
      ENDIF(CMAKE_INSTALL_DO_STRIP)
    ENDIF()
  ENDFOREACH()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "libopenscenegraph")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "libopenscenegraph-dev")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/osgManipulator" TYPE FILE FILES
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgManipulator/AntiSquish"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgManipulator/Command"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgManipulator/CommandManager"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgManipulator/Constraint"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgManipulator/Dragger"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgManipulator/Export"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgManipulator/Projector"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgManipulator/RotateCylinderDragger"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgManipulator/RotateSphereDragger"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgManipulator/Scale1DDragger"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgManipulator/Scale2DDragger"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgManipulator/ScaleAxisDragger"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgManipulator/Selection"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgManipulator/TabBoxDragger"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgManipulator/TabBoxTrackballDragger"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgManipulator/TabPlaneDragger"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgManipulator/TabPlaneTrackballDragger"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgManipulator/TrackballDragger"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgManipulator/Translate1DDragger"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgManipulator/Translate2DDragger"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgManipulator/TranslateAxisDragger"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgManipulator/TranslatePlaneDragger"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgManipulator/Version"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "libopenscenegraph-dev")

