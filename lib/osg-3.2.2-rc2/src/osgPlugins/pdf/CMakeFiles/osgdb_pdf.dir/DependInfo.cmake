# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgPlugins/pdf/ReaderWriterPDF.cpp" "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgPlugins/pdf/CMakeFiles/osgdb_pdf.dir/ReaderWriterPDF.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/OpenThreads/pthreads/CMakeFiles/OpenThreads.dir/DependInfo.cmake"
  "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osg/CMakeFiles/osg.dir/DependInfo.cmake"
  "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgDB/CMakeFiles/osgDB.dir/DependInfo.cmake"
  "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgUtil/CMakeFiles/osgUtil.dir/DependInfo.cmake"
  "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgWidget/CMakeFiles/osgWidget.dir/DependInfo.cmake"
  "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgViewer/CMakeFiles/osgViewer.dir/DependInfo.cmake"
  "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgText/CMakeFiles/osgText.dir/DependInfo.cmake"
  "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgGA/CMakeFiles/osgGA.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "include"
  "/usr/include/cairo"
  "/usr/include/glib-2.0"
  "/usr/lib/x86_64-linux-gnu/glib-2.0/include"
  "/usr/include/pixman-1"
  "/usr/include/freetype2"
  "/usr/include/libpng12"
  "/usr/include/poppler/glib"
  "/usr/include/poppler"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
