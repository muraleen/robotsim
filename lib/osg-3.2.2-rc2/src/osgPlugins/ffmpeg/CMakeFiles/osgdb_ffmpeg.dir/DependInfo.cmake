# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgPlugins/ffmpeg/FFmpegAudioStream.cpp" "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgPlugins/ffmpeg/CMakeFiles/osgdb_ffmpeg.dir/FFmpegAudioStream.o"
  "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgPlugins/ffmpeg/FFmpegClocks.cpp" "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgPlugins/ffmpeg/CMakeFiles/osgdb_ffmpeg.dir/FFmpegClocks.o"
  "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgPlugins/ffmpeg/FFmpegDecoder.cpp" "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgPlugins/ffmpeg/CMakeFiles/osgdb_ffmpeg.dir/FFmpegDecoder.o"
  "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgPlugins/ffmpeg/FFmpegDecoderAudio.cpp" "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgPlugins/ffmpeg/CMakeFiles/osgdb_ffmpeg.dir/FFmpegDecoderAudio.o"
  "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgPlugins/ffmpeg/FFmpegDecoderVideo.cpp" "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgPlugins/ffmpeg/CMakeFiles/osgdb_ffmpeg.dir/FFmpegDecoderVideo.o"
  "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgPlugins/ffmpeg/FFmpegImageStream.cpp" "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgPlugins/ffmpeg/CMakeFiles/osgdb_ffmpeg.dir/FFmpegImageStream.o"
  "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgPlugins/ffmpeg/FFmpegParameters.cpp" "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgPlugins/ffmpeg/CMakeFiles/osgdb_ffmpeg.dir/FFmpegParameters.o"
  "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgPlugins/ffmpeg/ReaderWriterFFmpeg.cpp" "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgPlugins/ffmpeg/CMakeFiles/osgdb_ffmpeg.dir/ReaderWriterFFmpeg.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "USE_SWSCALE"
  "__STDC_CONSTANT_MACROS"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/OpenThreads/pthreads/CMakeFiles/OpenThreads.dir/DependInfo.cmake"
  "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osg/CMakeFiles/osg.dir/DependInfo.cmake"
  "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgDB/CMakeFiles/osgDB.dir/DependInfo.cmake"
  "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgUtil/CMakeFiles/osgUtil.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "include"
  "/usr/include/libswscale"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
