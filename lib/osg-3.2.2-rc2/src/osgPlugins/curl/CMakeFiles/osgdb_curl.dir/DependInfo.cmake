# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgPlugins/curl/ReaderWriterCURL.cpp" "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgPlugins/curl/CMakeFiles/osgdb_curl.dir/ReaderWriterCURL.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "USE_ZLIB"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/OpenThreads/pthreads/CMakeFiles/OpenThreads.dir/DependInfo.cmake"
  "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osg/CMakeFiles/osg.dir/DependInfo.cmake"
  "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgDB/CMakeFiles/osgDB.dir/DependInfo.cmake"
  "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgUtil/CMakeFiles/osgUtil.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "include"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
