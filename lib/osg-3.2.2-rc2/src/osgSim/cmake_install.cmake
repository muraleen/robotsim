# Install script for directory: /home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgSim

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr/local")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Release")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "libopenscenegraph")
  FOREACH(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgSim.so.3.2.2"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgSim.so.100"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgSim.so"
      )
    IF(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      FILE(RPATH_CHECK
           FILE "${file}"
           RPATH "")
    ENDIF()
  ENDFOREACH()
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib64" TYPE SHARED_LIBRARY FILES
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/lib/libosgSim.so.3.2.2"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/lib/libosgSim.so.100"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/lib/libosgSim.so"
    )
  FOREACH(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgSim.so.3.2.2"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgSim.so.100"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgSim.so"
      )
    IF(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      FILE(RPATH_REMOVE
           FILE "${file}")
      IF(CMAKE_INSTALL_DO_STRIP)
        EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "${file}")
      ENDIF(CMAKE_INSTALL_DO_STRIP)
    ENDIF()
  ENDFOREACH()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "libopenscenegraph")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "libopenscenegraph-dev")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/osgSim" TYPE FILE FILES
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgSim/BlinkSequence"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgSim/ColorRange"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgSim/DOFTransform"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgSim/ElevationSlice"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgSim/HeightAboveTerrain"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgSim/Export"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgSim/GeographicLocation"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgSim/Impostor"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgSim/ImpostorSprite"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgSim/InsertImpostorsVisitor"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgSim/LightPoint"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgSim/LightPointNode"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgSim/LightPointSystem"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgSim/LineOfSight"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgSim/MultiSwitch"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgSim/OverlayNode"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgSim/ObjectRecordData"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgSim/ScalarBar"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgSim/ScalarsToColors"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgSim/Sector"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgSim/ShapeAttribute"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgSim/SphereSegment"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgSim/Version"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgSim/VisibilityGroup"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "libopenscenegraph-dev")

