# Install script for directory: /home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgVolume

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr/local")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Release")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "libopenscenegraph")
  FOREACH(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgVolume.so.3.2.2"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgVolume.so.100"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgVolume.so"
      )
    IF(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      FILE(RPATH_CHECK
           FILE "${file}"
           RPATH "")
    ENDIF()
  ENDFOREACH()
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib64" TYPE SHARED_LIBRARY FILES
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/lib/libosgVolume.so.3.2.2"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/lib/libosgVolume.so.100"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/lib/libosgVolume.so"
    )
  FOREACH(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgVolume.so.3.2.2"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgVolume.so.100"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgVolume.so"
      )
    IF(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      FILE(RPATH_REMOVE
           FILE "${file}")
      IF(CMAKE_INSTALL_DO_STRIP)
        EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "${file}")
      ENDIF(CMAKE_INSTALL_DO_STRIP)
    ENDIF()
  ENDFOREACH()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "libopenscenegraph")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "libopenscenegraph-dev")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/osgVolume" TYPE FILE FILES
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgVolume/Export"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgVolume/FixedFunctionTechnique"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgVolume/Layer"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgVolume/Locator"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgVolume/Property"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgVolume/RayTracedTechnique"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgVolume/Version"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgVolume/Volume"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgVolume/VolumeTechnique"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/osgVolume/VolumeTile"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "libopenscenegraph-dev")

