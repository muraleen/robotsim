# Install script for directory: /home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/OpenThreads/pthreads

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr/local")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Release")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "libopenthreads")
  FOREACH(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libOpenThreads.so.3.2.1"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libOpenThreads.so.20"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libOpenThreads.so"
      )
    IF(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      FILE(RPATH_CHECK
           FILE "${file}"
           RPATH "")
    ENDIF()
  ENDFOREACH()
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib64" TYPE SHARED_LIBRARY FILES
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/lib/libOpenThreads.so.3.2.1"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/lib/libOpenThreads.so.20"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/lib/libOpenThreads.so"
    )
  FOREACH(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libOpenThreads.so.3.2.1"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libOpenThreads.so.20"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libOpenThreads.so"
      )
    IF(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      IF(CMAKE_INSTALL_DO_STRIP)
        EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "${file}")
      ENDIF(CMAKE_INSTALL_DO_STRIP)
    ENDIF()
  ENDFOREACH()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "libopenthreads")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "libopenthreads-dev")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/OpenThreads" TYPE FILE FILES
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/OpenThreads/Atomic"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/OpenThreads/Barrier"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/OpenThreads/Block"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/OpenThreads/Condition"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/OpenThreads/Exports"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/OpenThreads/Mutex"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/OpenThreads/ReadWriteMutex"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/OpenThreads/ReentrantMutex"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/OpenThreads/ScopedLock"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/OpenThreads/Thread"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/OpenThreads/Version"
    "/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/include/OpenThreads/Config"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "libopenthreads-dev")

