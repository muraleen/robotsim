# Install script for directory: /home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr/local")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Release")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  INCLUDE("/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/OpenThreads/cmake_install.cmake")
  INCLUDE("/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osg/cmake_install.cmake")
  INCLUDE("/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgDB/cmake_install.cmake")
  INCLUDE("/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgUtil/cmake_install.cmake")
  INCLUDE("/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgGA/cmake_install.cmake")
  INCLUDE("/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgText/cmake_install.cmake")
  INCLUDE("/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgViewer/cmake_install.cmake")
  INCLUDE("/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgAnimation/cmake_install.cmake")
  INCLUDE("/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgFX/cmake_install.cmake")
  INCLUDE("/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgManipulator/cmake_install.cmake")
  INCLUDE("/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgParticle/cmake_install.cmake")
  INCLUDE("/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgPresentation/cmake_install.cmake")
  INCLUDE("/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgShadow/cmake_install.cmake")
  INCLUDE("/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgSim/cmake_install.cmake")
  INCLUDE("/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgTerrain/cmake_install.cmake")
  INCLUDE("/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgWidget/cmake_install.cmake")
  INCLUDE("/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgVolume/cmake_install.cmake")
  INCLUDE("/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgWrappers/serializers/cmake_install.cmake")
  INCLUDE("/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgWrappers/deprecated-dotosg/cmake_install.cmake")
  INCLUDE("/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgPlugins/cmake_install.cmake")
  INCLUDE("/home/narendran/Desktop/Research/C++/Simulation/lib/osg-3.2.2-rc2/src/osgQt/cmake_install.cmake")

ENDIF(NOT CMAKE_INSTALL_LOCAL_ONLY)

