# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "Fortran"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_Fortran
  "/home/narendran/Desktop/Research/Simulation/lib/lapack-3.6.0/BLAS/TESTING/zblat3.f" "/home/narendran/Desktop/Research/Simulation/lib/lapack-3.6.0/BLAS/TESTING/CMakeFiles/xblat3z.dir/zblat3.f.o"
  )
set(CMAKE_Fortran_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/narendran/Desktop/Research/Simulation/lib/lapack-3.6.0/BLAS/SRC/CMakeFiles/blas.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
