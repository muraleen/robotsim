# CMake generated Testfile for 
# Source directory: /home/narendran/Desktop/Research/Simulation/lib/lapack-3.6.0
# Build directory: /home/narendran/Desktop/Research/Simulation/lib/lapack-3.6.0
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(BLAS)
subdirs(SRC)
subdirs(TESTING)
