# Install script for directory: /home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/src/osgWidget

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "libopenscenegraph")
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgWidget.so.3.4.0"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgWidget.so.130"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgWidget.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHECK
           FILE "${file}"
           RPATH "")
    endif()
  endforeach()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib64" TYPE SHARED_LIBRARY FILES
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/lib/libosgWidget.so.3.4.0"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/lib/libosgWidget.so.130"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/lib/libosgWidget.so"
    )
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgWidget.so.3.4.0"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgWidget.so.130"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgWidget.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_REMOVE
           FILE "${file}")
      if(CMAKE_INSTALL_DO_STRIP)
        execute_process(COMMAND "/usr/bin/strip" "${file}")
      endif()
    endif()
  endforeach()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "libopenscenegraph-dev")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/osgWidget" TYPE FILE FILES
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgWidget/Export"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgWidget/Box"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgWidget/Browser"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgWidget/PdfReader"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgWidget/VncClient"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgWidget/Canvas"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgWidget/EventInterface"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgWidget/Frame"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgWidget/Input"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgWidget/Label"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgWidget/Lua"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgWidget/Python"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgWidget/ScriptEngine"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgWidget/StyleInterface"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgWidget/StyleManager"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgWidget/Table"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgWidget/Types"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgWidget/UIObjectParent"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgWidget/Util"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgWidget/Version"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgWidget/ViewerEventHandlers"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgWidget/Widget"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgWidget/Window"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgWidget/WindowManager"
    )
endif()

