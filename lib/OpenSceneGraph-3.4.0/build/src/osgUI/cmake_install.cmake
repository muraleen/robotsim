# Install script for directory: /home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/src/osgUI

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "libopenscenegraph")
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgUI.so.3.4.0"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgUI.so.130"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgUI.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHECK
           FILE "${file}"
           RPATH "")
    endif()
  endforeach()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib64" TYPE SHARED_LIBRARY FILES
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/lib/libosgUI.so.3.4.0"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/lib/libosgUI.so.130"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/lib/libosgUI.so"
    )
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgUI.so.3.4.0"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgUI.so.130"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgUI.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_REMOVE
           FILE "${file}")
      if(CMAKE_INSTALL_DO_STRIP)
        execute_process(COMMAND "/usr/bin/strip" "${file}")
      endif()
    endif()
  endforeach()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "libopenscenegraph-dev")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/osgUI" TYPE FILE FILES
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgUI/Export"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgUI/ColorPalette"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgUI/Widget"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgUI/Label"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgUI/LineEdit"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgUI/Dialog"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgUI/Popup"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgUI/PushButton"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgUI/ComboBox"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgUI/Callbacks"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgUI/Style"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgUI/AlignmentSettings"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgUI/FrameSettings"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgUI/TabWidget"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgUI/TextSettings"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgUI/Validator"
    )
endif()

