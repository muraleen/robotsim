# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/src/osgWrappers/deprecated-dotosg/osgShadow/LibraryWrapper.cpp" "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/src/osgWrappers/deprecated-dotosg/osgShadow/CMakeFiles/osgdb_deprecated_osgshadow.dir/LibraryWrapper.o"
  "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/src/osgWrappers/deprecated-dotosg/osgShadow/ShadowMap.cpp" "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/src/osgWrappers/deprecated-dotosg/osgShadow/CMakeFiles/osgdb_deprecated_osgshadow.dir/ShadowMap.o"
  "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/src/osgWrappers/deprecated-dotosg/osgShadow/ShadowTechnique.cpp" "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/src/osgWrappers/deprecated-dotosg/osgShadow/CMakeFiles/osgdb_deprecated_osgshadow.dir/ShadowTechnique.o"
  "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/src/osgWrappers/deprecated-dotosg/osgShadow/ShadowTexture.cpp" "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/src/osgWrappers/deprecated-dotosg/osgShadow/CMakeFiles/osgdb_deprecated_osgshadow.dir/ShadowTexture.o"
  "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/src/osgWrappers/deprecated-dotosg/osgShadow/ShadowVolume.cpp" "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/src/osgWrappers/deprecated-dotosg/osgShadow/CMakeFiles/osgdb_deprecated_osgshadow.dir/ShadowVolume.o"
  "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/src/osgWrappers/deprecated-dotosg/osgShadow/ShadowedScene.cpp" "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/src/osgWrappers/deprecated-dotosg/osgShadow/CMakeFiles/osgdb_deprecated_osgshadow.dir/ShadowedScene.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/src/osgShadow/CMakeFiles/osgShadow.dir/DependInfo.cmake"
  "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/src/osgDB/CMakeFiles/osgDB.dir/DependInfo.cmake"
  "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/src/osgUtil/CMakeFiles/osgUtil.dir/DependInfo.cmake"
  "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/src/osg/CMakeFiles/osg.dir/DependInfo.cmake"
  "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/src/OpenThreads/pthreads/CMakeFiles/OpenThreads.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../include"
  "include"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
