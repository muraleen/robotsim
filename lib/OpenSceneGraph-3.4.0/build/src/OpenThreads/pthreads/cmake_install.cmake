# Install script for directory: /home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/src/OpenThreads/pthreads

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "libopenthreads")
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libOpenThreads.so.3.3.0"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libOpenThreads.so.20"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libOpenThreads.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHECK
           FILE "${file}"
           RPATH "")
    endif()
  endforeach()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib64" TYPE SHARED_LIBRARY FILES
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/lib/libOpenThreads.so.3.3.0"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/lib/libOpenThreads.so.20"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/lib/libOpenThreads.so"
    )
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libOpenThreads.so.3.3.0"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libOpenThreads.so.20"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libOpenThreads.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      if(CMAKE_INSTALL_DO_STRIP)
        execute_process(COMMAND "/usr/bin/strip" "${file}")
      endif()
    endif()
  endforeach()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "libopenthreads-dev")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/OpenThreads" TYPE FILE FILES
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/OpenThreads/Atomic"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/OpenThreads/Barrier"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/OpenThreads/Block"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/OpenThreads/Condition"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/OpenThreads/Exports"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/OpenThreads/Mutex"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/OpenThreads/ReadWriteMutex"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/OpenThreads/ReentrantMutex"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/OpenThreads/ScopedLock"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/OpenThreads/Thread"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/include/OpenThreads/Version"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/include/OpenThreads/Config"
    )
endif()

