# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/src/osgPlugins/pnm/ReaderWriterPNM.cpp" "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/src/osgPlugins/pnm/CMakeFiles/osgdb_pnm.dir/ReaderWriterPNM.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/src/osgDB/CMakeFiles/osgDB.dir/DependInfo.cmake"
  "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/src/osgUtil/CMakeFiles/osgUtil.dir/DependInfo.cmake"
  "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/src/osg/CMakeFiles/osg.dir/DependInfo.cmake"
  "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/src/OpenThreads/pthreads/CMakeFiles/OpenThreads.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../include"
  "include"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
