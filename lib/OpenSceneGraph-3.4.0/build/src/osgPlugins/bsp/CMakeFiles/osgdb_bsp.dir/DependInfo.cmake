# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/src/osgPlugins/bsp/BITSET.cpp" "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/src/osgPlugins/bsp/CMakeFiles/osgdb_bsp.dir/BITSET.o"
  "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/src/osgPlugins/bsp/Q3BSPLoad.cpp" "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/src/osgPlugins/bsp/CMakeFiles/osgdb_bsp.dir/Q3BSPLoad.o"
  "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/src/osgPlugins/bsp/Q3BSPReader.cpp" "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/src/osgPlugins/bsp/CMakeFiles/osgdb_bsp.dir/Q3BSPReader.o"
  "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/src/osgPlugins/bsp/ReaderWriterBSP.cpp" "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/src/osgPlugins/bsp/CMakeFiles/osgdb_bsp.dir/ReaderWriterBSP.o"
  "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/src/osgPlugins/bsp/VBSPData.cpp" "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/src/osgPlugins/bsp/CMakeFiles/osgdb_bsp.dir/VBSPData.o"
  "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/src/osgPlugins/bsp/VBSPEntity.cpp" "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/src/osgPlugins/bsp/CMakeFiles/osgdb_bsp.dir/VBSPEntity.o"
  "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/src/osgPlugins/bsp/VBSPGeometry.cpp" "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/src/osgPlugins/bsp/CMakeFiles/osgdb_bsp.dir/VBSPGeometry.o"
  "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/src/osgPlugins/bsp/VBSPReader.cpp" "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/src/osgPlugins/bsp/CMakeFiles/osgdb_bsp.dir/VBSPReader.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/src/osgDB/CMakeFiles/osgDB.dir/DependInfo.cmake"
  "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/src/osgUtil/CMakeFiles/osgUtil.dir/DependInfo.cmake"
  "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/src/osg/CMakeFiles/osg.dir/DependInfo.cmake"
  "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/src/OpenThreads/pthreads/CMakeFiles/OpenThreads.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../include"
  "include"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
