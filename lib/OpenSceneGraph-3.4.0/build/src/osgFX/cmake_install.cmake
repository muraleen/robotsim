# Install script for directory: /home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/src/osgFX

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "libopenscenegraph")
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgFX.so.3.4.0"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgFX.so.130"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgFX.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHECK
           FILE "${file}"
           RPATH "")
    endif()
  endforeach()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib64" TYPE SHARED_LIBRARY FILES
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/lib/libosgFX.so.3.4.0"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/lib/libosgFX.so.130"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/build/lib/libosgFX.so"
    )
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgFX.so.3.4.0"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgFX.so.130"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib64/libosgFX.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_REMOVE
           FILE "${file}")
      if(CMAKE_INSTALL_DO_STRIP)
        execute_process(COMMAND "/usr/bin/strip" "${file}")
      endif()
    endif()
  endforeach()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "libopenscenegraph-dev")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/osgFX" TYPE FILE FILES
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgFX/AnisotropicLighting"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgFX/BumpMapping"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgFX/Cartoon"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgFX/Effect"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgFX/Export"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgFX/MultiTextureControl"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgFX/Outline"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgFX/Registry"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgFX/Scribe"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgFX/SpecularHighlights"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgFX/Technique"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgFX/Validator"
    "/home/narendran/Desktop/Research/Simulation/lib/OpenSceneGraph-3.4.0/include/osgFX/Version"
    )
endif()

