clc; clear all;

p3d = csvread('log_p3d.csv');
sr6 = csvread('log_sr6.csv');

% P3D Variables: q1 ... q7, th1 ... th6
p3d_map = [8,9,10,11,12,13,14,15,16,17,18,19,20];

% SR6 Variables: x, y, z, phi, theta, psi, th1 ... th6
sr6_map = [11,12,13,2,10,3,4,5,6,7,8,9];

N = 30;

labels = {'x (m)', 'y (m)', 'z (m)', '\phi (rad)', '\theta (rad)', '\psi (rad)', '\theta_1 (rad)', '\theta_2 (rad)', '\theta_3 (rad)', '\theta_4 (rad)', '\theta_5 (rad)', '\theta_6 (rad)'};

p3d_data = [];
sr6_data = [];

error = [];

q1 = 0;
q2 = 0;
q3 = 0;
q4 = 0;
q5 = 0;
q6 = 0;

r = [ (2*cos(q1))/25 - (9*sin(q1))/50 + (101*cos(q1)*cos(q2))/250 - (87*cos(q3)*sin(q1))/500 + (2*cos(q1)*cos(q2)*sin(q4))/5 - (69*cos(q3)*cos(q5)*sin(q1))/500 + (87*cos(q1)*sin(q2)*sin(q3))/500 + (2*cos(q4)*sin(q1)*sin(q3))/5 + (2*cos(q1)*cos(q3)*cos(q4)*sin(q2))/5 - (69*cos(q1)*cos(q2)*cos(q4)*sin(q5))/500 + (107*cos(q1)*cos(q2)*cos(q6)*sin(q4))/500 + (69*cos(q1)*cos(q5)*sin(q2)*sin(q3))/500 + (107*cos(q4)*cos(q6)*sin(q1)*sin(q3))/500 - (107*cos(q3)*sin(q1)*sin(q5)*sin(q6))/500 + (69*sin(q1)*sin(q3)*sin(q4)*sin(q5))/500 + (107*cos(q1)*cos(q3)*cos(q4)*cos(q6)*sin(q2))/500 + (107*cos(q1)*cos(q2)*cos(q4)*cos(q5)*sin(q6))/500 + (69*cos(q1)*cos(q3)*sin(q2)*sin(q4)*sin(q5))/500 + (107*cos(q1)*sin(q2)*sin(q3)*sin(q5)*sin(q6))/500 - (107*cos(q5)*sin(q1)*sin(q3)*sin(q4)*sin(q6))/500 - (107*cos(q1)*cos(q3)*cos(q5)*sin(q2)*sin(q4)*sin(q6))/500;...

	  (9*cos(q1))/50 + (2*sin(q1))/25 + (87*cos(q1)*cos(q3))/500 + (101*cos(q2)*sin(q1))/250 + (87*sin(q1)*sin(q2)*sin(q3))/500 + (69*cos(q1)*cos(q3)*cos(q5))/500 - (2*cos(q1)*cos(q4)*sin(q3))/5 + (2*cos(q2)*sin(q1)*sin(q4))/5 - (107*cos(q1)*cos(q4)*cos(q6)*sin(q3))/500 + (2*cos(q3)*cos(q4)*sin(q1)*sin(q2))/5 - (69*cos(q2)*cos(q4)*sin(q1)*sin(q5))/500 + (107*cos(q2)*cos(q6)*sin(q1)*sin(q4))/500 + (107*cos(q1)*cos(q3)*sin(q5)*sin(q6))/500 + (69*cos(q5)*sin(q1)*sin(q2)*sin(q3))/500 - (69*cos(q1)*sin(q3)*sin(q4)*sin(q5))/500 + (107*cos(q3)*cos(q4)*cos(q6)*sin(q1)*sin(q2))/500 + (107*cos(q2)*cos(q4)*cos(q5)*sin(q1)*sin(q6))/500 + (107*cos(q1)*cos(q5)*sin(q3)*sin(q4)*sin(q6))/500 + (69*cos(q3)*sin(q1)*sin(q2)*sin(q4)*sin(q5))/500 + (107*sin(q1)*sin(q2)*sin(q3)*sin(q5)*sin(q6))/500 - (107*cos(q3)*cos(q5)*sin(q1)*sin(q2)*sin(q4)*sin(q6))/500;...
      			                                                                                                                                                                                                                                                                                                                                                                                                    (87*cos(q2)*sin(q3))/500 - (101*sin(q2))/250 - (2*sin(q2)*sin(q4))/5 + (2*cos(q2)*cos(q3)*cos(q4))/5 + (69*cos(q2)*cos(q5)*sin(q3))/500 + (69*cos(q4)*sin(q2)*sin(q5))/500 - (107*cos(q6)*sin(q2)*sin(q4))/500 + (107*cos(q2)*cos(q3)*cos(q4)*cos(q6))/500 + (69*cos(q2)*cos(q3)*sin(q4)*sin(q5))/500 - (107*cos(q4)*cos(q5)*sin(q2)*sin(q6))/500 + (107*cos(q2)*sin(q3)*sin(q5)*sin(q6))/500 - (107*cos(q2)*cos(q3)*cos(q5)*sin(q4)*sin(q6))/500 + 79/250];

x0 = r(1);
y0 = r(2);
z0 = r(3);

for k = 1:N

	% Compute forward kinematics for sawyer on p3d to get position and orientation of SR6 base
	q1 = p3d(k,p3d_map(1));
	q2 = p3d(k,p3d_map(2));
	q3 = p3d(k,p3d_map(3));
	q4 = p3d(k,p3d_map(4));
	q5 = p3d(k,p3d_map(5));
	q6 = p3d(k,p3d_map(6));
	q7 = p3d(k,p3d_map(7));
	
	R = [ sin(q7)*(sin(q5)*(sin(q4)*(sin(q1)*sin(q3) + cos(q1)*cos(q3)*sin(q2)) - cos(q1)*cos(q2)*cos(q4)) - cos(q5)*(cos(q3)*sin(q1) - cos(q1)*sin(q2)*sin(q3))) - cos(q7)*(sin(q6)*(cos(q4)*(sin(q1)*sin(q3) + cos(q1)*cos(q3)*sin(q2)) + cos(q1)*cos(q2)*sin(q4)) + cos(q6)*(cos(q5)*(sin(q4)*(sin(q1)*sin(q3) + cos(q1)*cos(q3)*sin(q2)) - cos(q1)*cos(q2)*cos(q4)) + sin(q5)*(cos(q3)*sin(q1) - cos(q1)*sin(q2)*sin(q3)))),   sin(q7)*(sin(q6)*(cos(q4)*(sin(q1)*sin(q3) + cos(q1)*cos(q3)*sin(q2)) + cos(q1)*cos(q2)*sin(q4)) + cos(q6)*(cos(q5)*(sin(q4)*(sin(q1)*sin(q3) + cos(q1)*cos(q3)*sin(q2)) - cos(q1)*cos(q2)*cos(q4)) + sin(q5)*(cos(q3)*sin(q1) - cos(q1)*sin(q2)*sin(q3)))) + cos(q7)*(sin(q5)*(sin(q4)*(sin(q1)*sin(q3) + cos(q1)*cos(q3)*sin(q2)) - cos(q1)*cos(q2)*cos(q4)) - cos(q5)*(cos(q3)*sin(q1) - cos(q1)*sin(q2)*sin(q3))), cos(q6)*(cos(q4)*(sin(q1)*sin(q3) + cos(q1)*cos(q3)*sin(q2)) + cos(q1)*cos(q2)*sin(q4)) - sin(q6)*(cos(q5)*(sin(q4)*(sin(q1)*sin(q3) + cos(q1)*cos(q3)*sin(q2)) - cos(q1)*cos(q2)*cos(q4)) + sin(q5)*(cos(q3)*sin(q1) - cos(q1)*sin(q2)*sin(q3)));...
		  cos(q7)*(sin(q6)*(cos(q4)*(cos(q1)*sin(q3) - cos(q3)*sin(q1)*sin(q2)) - cos(q2)*sin(q1)*sin(q4)) + cos(q6)*(cos(q5)*(sin(q4)*(cos(q1)*sin(q3) - cos(q3)*sin(q1)*sin(q2)) + cos(q2)*cos(q4)*sin(q1)) + sin(q5)*(cos(q1)*cos(q3) + sin(q1)*sin(q2)*sin(q3)))) - sin(q7)*(sin(q5)*(sin(q4)*(cos(q1)*sin(q3) - cos(q3)*sin(q1)*sin(q2)) + cos(q2)*cos(q4)*sin(q1)) - cos(q5)*(cos(q1)*cos(q3) + sin(q1)*sin(q2)*sin(q3))), - sin(q7)*(sin(q6)*(cos(q4)*(cos(q1)*sin(q3) - cos(q3)*sin(q1)*sin(q2)) - cos(q2)*sin(q1)*sin(q4)) + cos(q6)*(cos(q5)*(sin(q4)*(cos(q1)*sin(q3) - cos(q3)*sin(q1)*sin(q2)) + cos(q2)*cos(q4)*sin(q1)) + sin(q5)*(cos(q1)*cos(q3) + sin(q1)*sin(q2)*sin(q3)))) - cos(q7)*(sin(q5)*(sin(q4)*(cos(q1)*sin(q3) - cos(q3)*sin(q1)*sin(q2)) + cos(q2)*cos(q4)*sin(q1)) - cos(q5)*(cos(q1)*cos(q3) + sin(q1)*sin(q2)*sin(q3))), sin(q6)*(cos(q5)*(sin(q4)*(cos(q1)*sin(q3) - cos(q3)*sin(q1)*sin(q2)) + cos(q2)*cos(q4)*sin(q1)) + sin(q5)*(cos(q1)*cos(q3) + sin(q1)*sin(q2)*sin(q3))) - cos(q6)*(cos(q4)*(cos(q1)*sin(q3) - cos(q3)*sin(q1)*sin(q2)) - cos(q2)*sin(q1)*sin(q4));...
		                                                                                                                                                                      sin(q7)*(sin(q5)*(cos(q4)*sin(q2) + cos(q2)*cos(q3)*sin(q4)) + cos(q2)*cos(q5)*sin(q3)) - cos(q7)*(cos(q6)*(cos(q5)*(cos(q4)*sin(q2) + cos(q2)*cos(q3)*sin(q4)) - cos(q2)*sin(q3)*sin(q5)) - sin(q6)*(sin(q2)*sin(q4) - cos(q2)*cos(q3)*cos(q4))),                                                                                                                                                                       cos(q7)*(sin(q5)*(cos(q4)*sin(q2) + cos(q2)*cos(q3)*sin(q4)) + cos(q2)*cos(q5)*sin(q3)) + sin(q7)*(cos(q6)*(cos(q5)*(cos(q4)*sin(q2) + cos(q2)*cos(q3)*sin(q4)) - cos(q2)*sin(q3)*sin(q5)) - sin(q6)*(sin(q2)*sin(q4) - cos(q2)*cos(q3)*cos(q4))),                                                                                                   - sin(q6)*(cos(q5)*(cos(q4)*sin(q2) + cos(q2)*cos(q3)*sin(q4)) - cos(q2)*sin(q3)*sin(q5)) - cos(q6)*(sin(q2)*sin(q4) - cos(q2)*cos(q3)*cos(q4))];

	r = [ (2*cos(q1))/25 - (9*sin(q1))/50 + (101*cos(q1)*cos(q2))/250 - (87*cos(q3)*sin(q1))/500 + (2*cos(q1)*cos(q2)*sin(q4))/5 - (69*cos(q3)*cos(q5)*sin(q1))/500 + (87*cos(q1)*sin(q2)*sin(q3))/500 + (2*cos(q4)*sin(q1)*sin(q3))/5 + (2*cos(q1)*cos(q3)*cos(q4)*sin(q2))/5 - (69*cos(q1)*cos(q2)*cos(q4)*sin(q5))/500 + (107*cos(q1)*cos(q2)*cos(q6)*sin(q4))/500 + (69*cos(q1)*cos(q5)*sin(q2)*sin(q3))/500 + (107*cos(q4)*cos(q6)*sin(q1)*sin(q3))/500 - (107*cos(q3)*sin(q1)*sin(q5)*sin(q6))/500 + (69*sin(q1)*sin(q3)*sin(q4)*sin(q5))/500 + (107*cos(q1)*cos(q3)*cos(q4)*cos(q6)*sin(q2))/500 + (107*cos(q1)*cos(q2)*cos(q4)*cos(q5)*sin(q6))/500 + (69*cos(q1)*cos(q3)*sin(q2)*sin(q4)*sin(q5))/500 + (107*cos(q1)*sin(q2)*sin(q3)*sin(q5)*sin(q6))/500 - (107*cos(q5)*sin(q1)*sin(q3)*sin(q4)*sin(q6))/500 - (107*cos(q1)*cos(q3)*cos(q5)*sin(q2)*sin(q4)*sin(q6))/500;...
 		  (9*cos(q1))/50 + (2*sin(q1))/25 + (87*cos(q1)*cos(q3))/500 + (101*cos(q2)*sin(q1))/250 + (87*sin(q1)*sin(q2)*sin(q3))/500 + (69*cos(q1)*cos(q3)*cos(q5))/500 - (2*cos(q1)*cos(q4)*sin(q3))/5 + (2*cos(q2)*sin(q1)*sin(q4))/5 - (107*cos(q1)*cos(q4)*cos(q6)*sin(q3))/500 + (2*cos(q3)*cos(q4)*sin(q1)*sin(q2))/5 - (69*cos(q2)*cos(q4)*sin(q1)*sin(q5))/500 + (107*cos(q2)*cos(q6)*sin(q1)*sin(q4))/500 + (107*cos(q1)*cos(q3)*sin(q5)*sin(q6))/500 + (69*cos(q5)*sin(q1)*sin(q2)*sin(q3))/500 - (69*cos(q1)*sin(q3)*sin(q4)*sin(q5))/500 + (107*cos(q3)*cos(q4)*cos(q6)*sin(q1)*sin(q2))/500 + (107*cos(q2)*cos(q4)*cos(q5)*sin(q1)*sin(q6))/500 + (107*cos(q1)*cos(q5)*sin(q3)*sin(q4)*sin(q6))/500 + (69*cos(q3)*sin(q1)*sin(q2)*sin(q4)*sin(q5))/500 + (107*sin(q1)*sin(q2)*sin(q3)*sin(q5)*sin(q6))/500 - (107*cos(q3)*cos(q5)*sin(q1)*sin(q2)*sin(q4)*sin(q6))/500;...
          			                                                                                                                                                                                                                                                                                                                                                                                                    (87*cos(q2)*sin(q3))/500 - (101*sin(q2))/250 - (2*sin(q2)*sin(q4))/5 + (2*cos(q2)*cos(q3)*cos(q4))/5 + (69*cos(q2)*cos(q5)*sin(q3))/500 + (69*cos(q4)*sin(q2)*sin(q5))/500 - (107*cos(q6)*sin(q2)*sin(q4))/500 + (107*cos(q2)*cos(q3)*cos(q4)*cos(q6))/500 + (69*cos(q2)*cos(q3)*sin(q4)*sin(q5))/500 - (107*cos(q4)*cos(q5)*sin(q2)*sin(q6))/500 + (107*cos(q2)*sin(q3)*sin(q5)*sin(q6))/500 - (107*cos(q2)*cos(q3)*cos(q5)*sin(q4)*sin(q6))/500 + 79/250];
	
	x = r(1);
	y = r(2);
	z = r(3);
	phi = atan2(-R(2,3),R(3,3));
	theta = atan2(R(1,3),sqrt(R(2,3)^2 + R(3,3)^2));
	psi = atan2(-R(1,2),R(1,1));
	
	p3d_data = [p3d_data; p3d(k,1) x-x0 y-y0 z-z0 phi theta psi p3d(k,p3d_map(1,8:13))];
	sr6_data = [sr6_data; sr6(k,1) sr6(k,sr6_map)];
	
end

% PLOT
figure;

for n = 1:12
	
	subplot(6,2,n);
	
	plot(sr6_data(:,1), sr6_data(:,n+1), 'b-'); hold on; grid on;
	plot(p3d_data(:,1), p3d_data(:,n+1), 'r-');
	
	ylabel(labels{n});
	
end

subplot(6,2,2);
legend('Free-floating Trajectory', 'Force-feedback Control');

subplot(6,2,11);
xlabel('t (s)');

subplot(6,2,12);
xlabel('t (s)');

error = sr6_data - p3d_data;

error_rms = [];

for n = 1:12
	
	error_rms = [error_rms rms(error(:,n+1))];
	
end

dr_rms = norm(error_rms(1:3))

da_rms = norm(error_rms(4:6))

dt_rms = norm(error_rms(7:12))
