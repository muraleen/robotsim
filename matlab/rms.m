function [ y ] = rms ( x )

	xbar = mean(x);
	y = 0;
	
	for k = 1:length(x)
		y = y + (x(k) - xbar)^2;
	end
	
	y = sqrt(y/length(x));

end
